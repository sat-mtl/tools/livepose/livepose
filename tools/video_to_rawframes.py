import os, sys, argparse
from tqdm import tqdm
import rosbag
import cv2
import numpy as np

# How to use video_to_rawframes.py :
#
# In a terminal : python3 video_to_rawframes.py /workspace_directory
#
# In this workspace_directory you should have one videos directory (/workspace_directory/videos) containing the .bag files 
# from which you want to extract the frames.
#
# The output directory will be /workspace_directory/rawframes where you can enjoy color and depth frames (.jpg)

FPS = 30

global args

def get_directory():
    # get dataset name
    dataset_name = args.dataset
    rawframes_directory = os.path.join("./", dataset_name, "rawframes")
    video_directory = os.path.join("./", dataset_name, "videos")
    return rawframes_directory, video_directory, dataset_name


def make_directory(directory_name):
    if not os.path.exists(directory_name):
        os.mkdir(directory_name)
    else:
        pass

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("dataset", type=str, help="dataset name")
    parser.add_argument('--rosbag-stream', default="Color", choices=["Color","Depth","Infrared"], type=str, help="rosbag stream")
    parser.add_argument('--color-format', default="jpg", choices=["jpg","png"], type=str, help="choose jpg or png output color frame format")
    args = parser.parse_args()

    rawframes_directory, video_directory, dataset_name = get_directory()
    make_directory(rawframes_directory)


    if os.path.isdir(os.path.join(video_directory)):
        make_directory(os.path.join(rawframes_directory))
        for videos in tqdm(
            os.listdir(os.path.join(video_directory)),
            desc="converting videos to frames",
        ):
            video_name, extension = os.path.splitext(videos)
            make_directory(os.path.join(rawframes_directory, video_name))
            video_path = os.path.join(video_directory, videos)
            image_path = os.path.join(rawframes_directory, video_name)
            if extension.endswith("bag"):
                # https://github.com/mlaiacker/rosbag2video/blob/master/rosbag2video.py
                counter = 1
                counter_d = 1
                for topic, msg, t in rosbag.Bag(video_path).read_messages():                    
                    if topic.endswith('image/data') and topic.find("Color") != -1:
                        pix_fmt=None
                        if msg.encoding.find('YUYV')!=-1:
                            pix_fmt=cv2.COLOR_YUV2BGR_YUYV    
                        else:
                            print(f"unsupported format {msg.encoding}")
                            sys.exit(1)
                        np_arr = np.frombuffer(msg.data, np.uint8)
                        mat = np.reshape(np_arr, (msg.height,msg.width,2))
                        cv_image = cv2.cvtColor(mat, pix_fmt)
                        image_filename = '%s/img_%05d.%s' % (image_path,counter,args.color_format)
                        cv2.imwrite(image_filename,cv_image)
                        counter += 1
                        


                    if topic.endswith('image/data') and topic.find("Depth") != -1:
                        pix_fmt=None
                        if msg.encoding.find('mono16')!=-1:
                            pix_fmt=cv2.COLOR_GRAY2BGR
                        else:
                            print(f"unsupported format {msg.encoding}")
                            sys.exit(1)
                        np_arr_depth = np.frombuffer(msg.data, np.uint16)
                        mat_depth = np.reshape(np_arr_depth,(msg.height,msg.width))
                        # cv_image = cv2.cvtColor(mat_depth, pix_fmt)
                        depth_image_normalized = cv2.normalize(mat_depth, None, 0, 65535, cv2.NORM_MINMAX, cv2.CV_16U)


                        image_filename = '%s/img_%05d_depth.png' % (image_path,counter_d) 
                        cv2.imwrite(image_filename,depth_image_normalized)
                        counter_d += 1

            else:
                command = "ffmpeg -i {0} -vf fps={1} {2}/img_%05d.jpg -hide_banner -loglevel error".format(
                    video_path, FPS, image_path
                )
                print(command)
                os.system(command)
    else:
        print(
            "Video directory does not match the template. Check the readme for instructions"
        )
        sys.exit()
