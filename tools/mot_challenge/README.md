# MOT Challenge
Tools for evaluating LivePose on the
[Multiple Object Tracking Benchmark (MOT)](https://motchallenge.net/).


## Data
This repo assumes you have already downloaded the MOT 20 Training and Testing
data and have placed it in a folder `~/MOT20/` in your home directory.

If you have not done so already, please download it
[here](https://motchallenge.net/data/MOT20/).
Select `Get all data (5.0 GB)` and unzip the file in your home directory.

The `MOT20/` directory should contain subdirectories `train/` and `test/`.


## Usage
To generate the LivePose predictions for the MOT20 training dataset, run the
python script included in this directory:

```
./tools/mot_challenge/run_mot20_challenge.py
```

This will write predictions for the dataset to a folder
`~/livepose_mot20_results/data/` in your home directory.
If this folder does not already exist, the script will create it for you.

You can then use the official
[MOT Challenge Evaluation Toolkit](https://github.com/JonathonLuiten/TrackEval)
repo to evaluate the results with the following commands.

1. Clone the Evaluation Toolkit repo
```
git clone https://github.com/JonathonLuiten/TrackEval
```

2. Copy LivePose predictions to the TrackEval data directory

```
cp -r ~/livepose_mot20_results/{backend_name} ~/TrackEval/data/livepose
```

For example, using the OpenPose Wrapper backend:
```
cp -r ~/livepose_mot20_results/openpose_wrapper ~/TrackEval/data/livepose
```

3. Run evaluation script for the MOT20 challenge
```
cd ~/TrackEval
python3 scripts/run_mot_challenge.py --BENCHMARK MOT20 --TRACKERS_TO_EVAL livepose --METRICS HOTA
```

This will print out all evaluation results for livepose.
Please see the
[TrackEval docs](https://github.com/JonathonLuiten/TrackEval/blob/master/Readme.md)
for references for in depth explanations of the evaluation metrics.
