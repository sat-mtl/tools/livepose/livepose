# LivePose MOT20 Challenege Results
This doc summarizes performance of LivePose on the MOT20 Challenge using the
HOTA metrics.

These experiments can be repeated using the instructions found in
[tools/mot_challenge/README](tools/mot_challenge/README.md).

These results were created using the official
[TrackEval](https://github.com/JonathonLuiten/TrackEval) repo.

More info on HOTA can be found in the TrackEval documentation and in
[this blog post](https://jonathonluiten.medium.com/how-to-evaluate-tracking-with-the-hota-metrics-754036d183e1).

**Note**: Each evaluation takes roughly 20-30 minutes to run on the System76 laptops.
This has only been tested for the OpenPose wrapper so far.

## Best Results
This table represents the best results measured so far. They were recorded
using the OpenPose Wrapper with the following parameters for SORT:
```
max_age: 8
min_hits: 1
iou_threshold: 0.1
```

| HOTA     | DetA     | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| -------- | -------- | ------- | -------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 1.2857   | 4.2629   | 0.45314 | 4.7043   | 22.088 | 0.47958 | 22.736 | 60.971 | 1.357   | 3.737   | 30.045  | 1.1228      |

| Dets   | GT_Dets | IDs   | GT_IDs |
| ------ | ------- | ----- | ------ |
| 241649 | 1134614 | 48708 | 2215   |


## Shuffling Order Test
We shuffle the order of the results for each sequence to make sure the order
doesn't affect the evaluation scores. This is just a control test to make
sure we do not have to record the results in any particular order.
The results are still separated by sequence (each sequence has its own file),
but within a given sequence we shuffle the order of the line-by-line results,
where each line gives a prediction for a single detected person in a single
detected frame. All metrics for the control and the shuffled results should be
exactly the same. This ensures we can save predictions in any order for all
future experiments and do not need to order them by detection ID or frame
index.

### Control
Using defaults (`max_age=10`, `min_hits=1`, `iou_threshold=0.1`)

| HOTA     | DetA     | AssA    | DetRe   | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| -------- | -------- | ------- | ------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 1.2985   | 4.223    | 0.46639 | 4.6597  | 21.979 | 0.49762 | 21.47  | 60.873 | 1.3705  | 3.7689  | 29.965  | 1.1293      |

| Dets   | GT_Dets | IDs   | GT_IDs |
| ------ | ------- | ----- | ------ |
| 240548 | 1134614 | 44908 | 2215   |

### With sequence results shuffled
We call `random.shuffle(seq_results)` before writing the results to file.

| HOTA     | DetA     | AssA    | DetRe   | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| -------- | -------- | ------- | ------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 1.2985   | 4.223    | 0.46639 | 4.6597  | 21.979 | 0.49762 | 21.47  | 60.873 | 1.3705  | 3.7689  | 29.965  | 1.1293      |

| Dets   | GT_Dets | IDs   | GT_IDs |
| ------ | ------- | ----- | ------ |
| 240548 | 1134614 | 44908 | 2215   |


# Parameter Search
The following sections contain tests on the 3 main parameters for the SORT
object tracker.

## Max Age Parameter Test
In this experiment we vary the `max_age` parameter for the `SORT` object tracker.
This parameter determines how many consecutive frames a detection can be missing
from before it is deleted.
For example if `max_age == 1` then a detection ID will be discarded immediately
once it is missing from a single frame. If that same person is re-detected in
a later frame, they will be given a new ID.

### Max Age 1 (Default)
| HOTA     | DetA     | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| -------- | -------- | ------- | -------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 0.11551  | 0.07032  | 0.2404  | 0.070393 | 28.413 | 0.24092 | 71.585 | 62.74  | 0.11558 | 0.26441 | 34.195  | 0.090416    |

| Dets | GT_Dets | IDs  | GT_IDs |
| ---- | ------- | ---- | ------ |
| 2811 | 1134614 | 2016 | 2215   |

### Max Age 5
| HOTA     | DetA     | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| -------- | -------- | ------- | -------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 0.1611   | 0.11182  | 0.29885 | 0.11201  | 28.437 | 0.30045 | 74.101 | 62.646 | 0.16126 | 0.36372 | 34.495  | 0.12546     |

| Dets | GT_Dets | IDs  | GT_IDs |
| ---- | ------- | ---- | ------ |
| 4469 | 1134614 | 3018 | 2215   |

### Max Age 7
| HOTA     | DetA     | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| -------- | -------- | ------- | -------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 0.16497  | 0.11913  | 0.29953 | 0.11935  | 28.592 | 0.3011  | 68.953 | 62.781 | 0.16514 | 0.36336 | 34.791  | 0.12642     |

| Dets | GT_Dets | IDs  | GT_IDs |
| ---- | ------- | ---- | ------ |
| 4736 | 1134614 | 3077 | 2215   |

### Max Age 8
| HOTA     | DetA     | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| -------- | -------- | ------- | -------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 0.17292  | 0.12082  | 0.33142 | 0.12104  | 28.34  | 0.33371 | 69.548 | 62.676 | 0.17311 | 0.39196 | 34.566  | 0.13548     |

| Dets | GT_Dets | IDs  | GT_IDs |
| ---- | ------- | ---- | ------ |
| 4846 | 1134614 | 3159 | 2215   |

### Max Age 9
| HOTA     | DetA     | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| -------- | -------- | ------- | -------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 0.16905  | 0.12063  | 0.29735 | 0.12084  | 28.259 | 0.29963 | 68.668 | 62.895 | 0.16923 | 0.39079 | 34.264  | 0.1339      |

| Dets | GT_Dets | IDs  | GT_IDs |
| ---- | ------- | ---- | ------ |
| 4852 | 1134614 | 3131 | 2215   |

### Max Age 10
| HOTA     | DetA     | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| -------- | -------- | ------- | -------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 0.15445  | 0.11705  | 0.26809 | 0.11726  | 27.729 | 0.26911 | 71.636 | 62.518 | 0.15461 | 0.3626  | 33.69   | 0.12216     |

| Dets | GT_Dets | IDs  | GT_IDs |
| ---- | ------- | ---- | ------ |
| 4798 | 1134614 | 3133 | 2215   |

## Min Hits Parameter Test
For all of these tests we set `max_age=8`, since it performed best in the
previous experiment. The original default value for `min_hits` in SORT is `3`.

### Min Hits 1
| HOTA     | DetA     | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| -------- | -------- | ------- | -------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 0.92758  | 2.9438   | 0.36827 | 3.1354   | 22.834 | 0.37744 | 47.156 | 60.962 | 0.96143 | 2.4665  | 30.29   | 0.7471      |

| Dets   | GT_Dets | IDs   | GT_IDs |
| ------ | ------- | ----- | ------ |
| 155797 | 1134614 | 68681 | 2215   |

## IoU Threshold Parameter Test
All of these tests use `max_age=8` and `min_hits=1`.

### IoU Treshold 0.1
| HOTA     | DetA     | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| -------- | -------- | ------- | -------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 1.2857   | 4.2629   | 0.45314 | 4.7043   | 22.088 | 0.47958 | 22.736 | 60.971 | 1.357   | 3.737   | 30.045  | 1.1228      |

| Dets   | GT_Dets | IDs   | GT_IDs |
| ------ | ------- | ----- | ------ |
| 241649 | 1134614 | 48708 | 2215   |

# Deprecated Results
These tests were done with an older version of LivePose before a bug was
removed in the SORT object tracker code (see
[MR 211](https://gitlab.com/sat-mtl/tools/livepose/-/merge_requests/211)).
The results are no longer valid with the newest version of LivePose but they are
kept here for reference.

## Sort Tracker Reversed Order Test
In this experiment we change the enumeration in line 256 in `sort.py` to
enumerate the trackers in reversed order, that way when the dead trackers are
popped it does not affect the indices of the other unprocessed trackers.
I hypothesize this should *improve* results, since before we were essentially
discarding random trackers due to indices being chnaged unpredictably by calling
`pop()` on the list of trackers while enumerating through it, thus changing the
indices of the trackers to be popped. Reversing the list should solve this
issue and increase performance by removing the correct trackers instead of just
removing random incorrectly indexed trackers.

### SORT Defaults
In the first experiment we use the default parameters for SORT:
* `max_age=1`
* `min_hits=3`
* `iou_threshold=0.3`

#### Control
| HOTA     | DetA     | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA | HOTA(0) | LocA(0) | HOTALocA(0) |
| -------- | -------- | ------- | -------- | ------ | ------- | ------ | ------ | ----- | ------- | ------- | ----------- |
| 0.097957 | 0.052054 | 0.22009 | 0.052093 | 28.692 | 0.22044 | 72.444 | 63.248 | 0.098 | 0.21884 | 34.846  | 0.076257    |

| Dets | GT_Dets | IDs  | GT_IDs |
| ---- | ------- | ---- | ------ |
| 2060 | 1134614 | 1601 | 2215   |


#### With Trackers Reversed
| HOTA     | DetA     | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| -------- | -------- | ------- | -------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 0.11551  | 0.07032  | 0.2404  | 0.070393 | 28.413 | 0.24092 | 71.585 | 62.74  | 0.11558 | 0.26441 | 34.195  | 0.090416    |

| Dets | GT_Dets | IDs  | GT_IDs |
| ---- | ------- | ---- | ------ |
| 2811 | 1134614 | 2016 | 2215   |

### OpenPose Wrapper
**Backend**: OpenPose (using the official OpenPose wrapper)
**SORT Params**: default


**HOTA Results**
| HOTA     | DetA     | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA | HOTA(0) | LocA(0) | HOTALocA(0) |
| -------- | -------- | ------- | -------- | ------ | ------- | ------ | ------ | ----- | ------- | ------- | ----------- |
| 0.097957 | 0.052054 | 0.22009 | 0.052093 | 28.692 | 0.22044 | 72.444 | 63.248 | 0.098 | 0.21884 | 34.846  | 0.076257    |

**Detection Count Results**
| Dets | GT_Dets | IDs  | GT_IDs |
| ---- | ------- | ---- | ------ |
| 2060 | 1134614 | 1601 | 2215   |


## Max Age Parameter Test
In this experiment we vary the `max_age` parameter for the `SORT` object tracker.
This parameter determines how many consecutive frames a detection can be missing
from before it is deleted.
For example if `max_age == 1` then a detection ID will be discarded immediately
once it is missing from a single frame. If that same person is re-detected in
a later frame, they will be given a new ID.

### Max Age 1
| HOTA     | DetA     | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA | HOTA(0) | LocA(0) | HOTALocA(0) |
| -------- | -------- | ------- | -------- | ------ | ------- | ------ | ------ | ----- | ------- | ------- | ----------- |
| 0.097957 | 0.052054 | 0.22009 | 0.052093 | 28.692 | 0.22044 | 72.444 | 63.248 | 0.098 | 0.21884 | 34.846  | 0.076257    |

| Dets | GT_Dets | IDs  | GT_IDs |
| ---- | ------- | ---- | ------ |
| 2060 | 1134614 | 1601 | 2215   |

### Max Age 5
| HOTA    | DetA    | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| ------- | ------- | ------- | -------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 0.14449 | 0.09624 | 0.29843 | 0.096379 | 28.366 | 0.30026 | 70.875 | 62.804 | 0.14461 | 0.31884 | 34.589  | 0.11028     |

| Dets | GT_Dets | IDs  | GT_IDs |
| ---- | ------- | ---- | ------ |
| 3855 | 1134614 | 2710 | 2215   |

### Max Age 10
| HOTA    | DetA    | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| ------- | ------- | ------- | -------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 0.15647 | 0.10848 | 0.29543 | 0.10867  | 27.521 | 0.29718 | 73.31  | 62.456 | 0.15663 | 0.35992 | 33.7    | 0.12129     |

| Dets | GT_Dets | IDs  | GT_IDs |
| ---- | ------- | ---- | ------ |
| 4480 | 1134614 | 2956 | 2215   |

### Max Age 15
| HOTA    | DetA    | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| ------- | ------- | ------- | -------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 0.15197 | 0.10708 | 0.31301 | 0.10725  | 27.751 | 0.316   | 69.493 | 62.496 | 0.15212 | 0.34064 | 33.916  | 0.11553     |

| Dets | GT_Dets | IDs  | GT_IDs |
| ---- | ------- | ---- | ------ |
| 4385 | 1134614 | 2895 | 2215   |

### Max Age 20
| HOTA    | DetA    | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| ------- | ------- | ------- | -------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 0.15108 | 0.10467 | 0.29277 | 0.10484  | 27.296 | 0.29507 | 67.567 | 62.332 | 0.15123 | 0.34652 | 33.506  | 0.11611     |

| Dets | GT_Dets | IDs  | GT_IDs |
| ---- | ------- | ---- | ------ |
| 4358 | 1134614 | 2784 | 2215   |


## Min Hits Parameter Test
In this experiment we vary the `min_hits` parameter for the `SORT` object tracker.

All trials here use `max_age=10`, since that value gave the best performance
in the standalone max age experiments.

### Min Hits 1
| HOTA    | DetA    | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| ------- | ------- | ------- | -------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 0.92347 | 2.9825  | 0.36147 | 3.1825   | 22.609 | 0.37094 | 44.975 | 60.892 | 0.95811 | 2.478   | 30.1    | 0.74589     |

| Dets   | GT_Dets | IDs   | GT_IDs |
| ------ | ------- | ----- | ------ |
| 159710 | 1134614 | 67548 | 2215   |


## IoU Threshold Test
In this experiment we vary the `iou_threshold` parameter for the `SORT` object tracker.

All results here use `max_age=10` and `min_hits=1`, since those values gave the
best performance in previous experiments.

### IoU Threshold 0.1
| HOTA    | DetA    | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| ------- | ------- | ------- | -------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 1.2985  | 4.223   | 0.46639 | 4.6597   | 21.979 | 0.49762 | 21.47  | 60.873 | 1.3705  | 3.7689  | 29.965  | 1.1293      |

| Dets   | GT_Dets | IDs   | GT_IDs |
| ------ | ------- | ----- | ------ |
| 240548 | 1134614 | 44908 | 2215   |

### IoU Threshold 0.3 (default)
| HOTA    | DetA    | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| ------- | ------- | ------- | -------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 0.92347 | 2.9825  | 0.36147 | 3.1825   | 22.609 | 0.37094 | 44.975 | 60.892 | 0.95811 | 2.478   | 30.1    | 0.74589     |

| Dets   | GT_Dets | IDs   | GT_IDs |
| ------ | ------- | ----- | ------ |
| 159710 | 1134614 | 67548 | 2215   |

### IoU Threshold 0.5

| HOTA    | DetA    | AssA    | DetRe    | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| ------- | ------- | ------- | -------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 0.53567 | 1.5856  | 0.2438  | 1.6379   | 23.254 | 0.24464 | 72.869 | 61.113 | 0.54592 | 1.344   | 30.463  | 0.4094      |

| Dets   | GT_Dets | IDs   | GT_IDs |
| ------ | ------- | ----- | ------ |
| 79918  | 1134614 | 55450 | 2215   |

### IoU Threshold 0.9
| HOTA     | DetA     | AssA    | DetRe   | DetPr  | AssRe   | AssPr  | LocA   | RHOTA   | HOTA(0) | LocA(0) | HOTALocA(0) |
| -------- | -------- | ------- | ------- | ------ | ------- | ------ | ------ | ------- | ------- | ------- | ----------- |
| 0.085919 | 0.061861 | 0.15612 | 0.06195 | 21.952 | 0.15612 | 89.301 | 61.817 | 0.08599 | 0.20639 | 30.461  | 0.062868    |

| Dets   | GT_Dets | IDs   | GT_IDs |
| ------ | ------- | ----- | ------ |
| 3202   | 1134614 | 3111  | 2215   |
