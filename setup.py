import os
import sys
from setuptools import setup
import re
import subprocess

from distutils.extension import Extension
from Cython.Build import cythonize
from Cython.Distutils import build_ext
from typing import Dict, List

sys.path.append("/usr/local/python")

try:
    import lsb_release
    has_lsb_release = True
except (ImportError, ModuleNotFoundError) as e:
    has_lsb_release = False

# Core dependencies
install_requires = [
    "dataclasses",
    "getch",
    "filterpy",
    "lap",
    "numpy<2",
    "opencv-python",
    "rosbags",
    "typing-extensions>=4.5"
]

# Extra dependencies: extra name as key to list of dependencies
extras_require: Dict[str, List[str]] = {
    "libmapper": ["libmapper"],
    "mediapipe": ["mediapipe>=0.10.14"],
    "mmpose": ["numpy <2", "torch", "torchvision", "mmcv-full>=1.6.1", "mmdet>=2.25.1", "mmpose==0.28.1", "xtcocotools==1.10"],
    "mmaction2": ["mmaction2==0.24.1"],
    "osc": ["pyliblo"],
    "ndi": ["cyndilib==0.0.4"],
    "posenet": [],
    "pyrealsense2": ["pyrealsense2>=2.50.0"],
    "trt": ["pycocotools", "torch", "torchvision", "tensorrt", "torch2trt@git+https://gitlab.com/sat-mtl/tools/forks/torch2trt.git", "trt_pose@git+https://gitlab.com/sat-mtl/tools/forks/trt_pose.git"],
    "websockets": ["websockets"],
}

# Paths to git submodules required by extras
extras_submodule_paths = {
    "mmpose": "external/mmpose",
    "mmaction2": "external/mmaction2"
}

# Entry points for plugins: with list of required extras at end of string
entry_points = {
    "livepose.cameras": [
        "opencv = livepose.cameras.opencv []",
        "pyrealsense2 = livepose.cameras.pyrealsense2 [pyrealsense2]",
    ],
    "livepose.pose_backends": [
        "mediapipe = livepose.pose_backends.mediapipe [mediapipe]",
        "mmpose = livepose.pose_backends.mmpose [mmpose]",
        "posenet = livepose.pose_backends.posenet [posenet]",
        "trt = livepose.pose_backends.trt [trt]",
        "void = livepose.pose_backends.void []",
    ],
    "livepose.filters": [
        "ArmUpFilter = livepose.filters.armup_filter []",
        "HubsFilter = livepose.filters.armup_filter []",
        "MMAction2Filter = livepose.filters.mmaction2 [mmaction2]",
        "NumberIDFilter = livepose.filters.number_id []",
        "OrientationFilter = livepose.filters.orientation_filter []",
        "PositionFilter = livepose.filters.position_filter []",
        "Position2DFilter = livepose.filters.position_2d_filter []",
        "SkeletonsFilter = livepose.filters.skeletons_filter []",
        "TriangulateFilter = livepose.filters.triangulate_filter []",
        "PoseActionDetection = livepose.filters.pose_action_detection_filter [mmaction2]",
        "FrameActionDetection = livepose.filters.frame_action_detection_filter [mmaction2]",
        "PointingFilter = livepose.filters.pointing_filter []",
        "PyRealsenseHeadFollower = livepose.filters.pyrealsense_head_follower [pyrealsense2]",
        "SVMPoseEstimator = livepose.filters.svm_pose_estimator []",
    ],
    "livepose.outputs": [
        "json = livepose.outputs.json []",
        "libmapper = livepose.outputs.libmapper [libmapper]",
        "osc = livepose.outputs.osc [osc]",
        "websocket = livepose.outputs.websocket [websockets]",
    ],
    "livepose.dimmaps": [
        "pose2d_to_floor = livepose.dimmaps.pose2d_to_floor []",
        "realsense_2d_to_3d = livepose.dimmaps.realsense_2d_to_3d []",
    ],
}

# Generate extras_require
# Remove direct URL dependencies (git repositories) when packages are available (for ubuntu 20.04 and 22.04)


def generate_extras_require() -> Dict[str, List[str]]:
    if has_lsb_release:
        lsb = lsb_release.get_os_release()
        if lsb['RELEASE'] == '20.04' or lsb['RELEASE'] == '22.04':
            for extra in extras_require:
                for i in range(len(extras_require[extra])):
                    extras_require[extra][i] = extras_require[extra][i].split("@")[0]
                    print(extras_require[extra][i])
    return extras_require

# Generate required extras based on environment variable LIVEPOSE_EXTRAS
# Defaults to all extras if LIVEPOSE_EXTRAS is not defined


def generate_required_extras() -> List[str]:
    required_extras: List[str] = []
    LIVEPOSE_EXTRAS = os.environ.get("LIVEPOSE_EXTRAS", None)
    if LIVEPOSE_EXTRAS is not None and LIVEPOSE_EXTRAS != "all":
        extras = LIVEPOSE_EXTRAS.split(";")
        for extra in extras:
            if extra in generate_extras_require().keys():
                required_extras.append(extra)
            else:
                print(f"Wrong extra {extra} specified in LIVEPOSE_EXTRAS={LIVEPOSE_EXTRAS}")
                sys.exit(1)
    if len(required_extras) == 0 or LIVEPOSE_EXTRAS == "all":
        required_extras = list(generate_extras_require().keys())
    return required_extras

# Generate install requires based on core dependencies and required extras


def generate_install_requires() -> List[str]:
    required_extras: List[str] = generate_required_extras()
    required_extra_requires: List[str] = []
    for extra in required_extras:
        for require in generate_extras_require()[extra]:
            required_extra_requires.append(require)
        # Update git submodules required by extras
        if extra in extras_submodule_paths and os.path.exists(".gitmodules"):
            path = extras_submodule_paths[extra]
            try:
                subprocess.check_output(["git", "submodule", "update", "--init",
                                         "--recursive", path], stderr=subprocess.STDOUT)
            except FileNotFoundError:
                print(f"Could not run git, please install git")
                sys.exit(1)
            except subprocess.CalledProcessError as e:
                output = e.output.decode()
                print(f"Could not update git submodule at path {path} required for extra {extra}: {output}")
                sys.exit(1)
    return install_requires+required_extra_requires

# Generate entry points based on core dependencies and required extras


def generate_entry_points() -> Dict[str, List[str]]:
    required_extras = generate_required_extras()
    required_entry_points: Dict[str, List[str]] = {}
    for group in entry_points.keys():
        required_entry_points[group] = []
        for entry_point in entry_points[group]:
            DEPS_REGEX = r"\[(.*)\]"
            found_deps = re.search(DEPS_REGEX, entry_point)
            deps = []
            if found_deps is not None:
                deps_str = found_deps.group().split('[')[1].split(']')[0]
                if deps_str != '':
                    deps = deps_str.split(";")
            deps_ok = True
            for dep in deps:
                if dep not in required_extras:
                    deps_ok = False
            if deps_ok:
                required_entry_points[group].append(entry_point)
    return required_entry_points

# Utility function to read the README.md


def read(fname: str) -> str:
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="LivePose",
    version="1.12.0",
    author="Gabriel Downs, Christian Frisson, Marie-Ève Dumas, Emmanuel Durand",
    author_email="edurand@sat.qc.ca",
    description="Command line tool to track people skeleton and send them through various protocols",
    license="GPL",
    keywords="skeleton posenet osc websocket",
    url="https://gitlab.com/sat-mtl/tools/livepose",
    packages=[
        "livepose",
        "livepose.cameras",
        "livepose.configs",
        "livepose.dimmaps",
        "livepose.pose_backends",
        "livepose.extlib",
        "livepose.filters",
        "livepose.keypoints",
        "livepose.outputs"
    ],
    package_data={
        "livepose": [
            "configs/*.json",
            "models/posenet/LICENSE.md",
            "models/posenet/mobilenet_float_100_v1_stride8_frozen_model.pb",
            "models/posenet/mobilenet_float_100_v1_stride16_frozen_model.pb",
            "models/posenet/resnet50_float_v1_stride16_frozen_model.pb",
            "models/posenet/resnet50_float_v1_stride32_frozen_model.pb",
            "external/mmpose/configs/*",
            "external/mmpose/demo/mm*_cfg"
        ]
    },
    include_package_data=True,
    install_requires=generate_install_requires(),
    extras_require=generate_extras_require(),
    entry_points=generate_entry_points(),
    scripts=[
        "bin/livepose"
    ],
    long_description=read("README.md"),
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Topic :: Multimedia :: Graphics :: Capture",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)"
    ],
    cmdclass={'build_ext': build_ext},
    ext_modules=cythonize([
        Extension("livepose/cameras/*", ["livepose/cameras/*.py"], language='c++'),
        Extension("livepose/dimmaps/*", ["livepose/dimmaps/*.py"], language='c++'),
        Extension("livepose/extlib/*", ["livepose/extlib/*.py"], language='c++'),
        Extension("livepose/filters/*", ["livepose/filters/*.py"], language='c++'),
        Extension("livepose/inputs/*", ["livepose/inputs/*.py"], language='c++'),
        # Extension("livepose/outputs/*", ["livepose/outputs/*.py"], language='c++'),  # deactivated because of issues with RosbagOutput
        # Extension("livepose/pose_backends/*", ["livepose/pose_backends/*.py"], language='c++'),  # deactivated because of issues with some backends
        Extension("livepose/*.py", ["livepose/*.py"], language='c++')
    ],
        compiler_directives={
            'always_allow_keywords': True,
            'binding': True,
            'language_level': 3
    })
)
