LivePose
=====

LivePose is a command line tool which tracks people skeletons from a RGB or
grayscale video feed (live or not), applies various filters on them (for
detection, selection, improving the data, etc) and sends the results through
the network (OSC and Websocket are currently supported).

LivePose is able to do all of this in real-time, processing *live* video
streams and sending out results for each frame at 20-30 FPS.

As some example of what the software can do, there are filters to detect
specific actions (e.g. raising arms), detecting skeletons position onto a
plane, controlling the avatar in [Mozilla Hubs](https://hubs.mozilla.org), etc.
There are some ongoing work to add support for 3D skeleton detection using
multiple cameras, and for more robust action detection using a neural network.

This software has been tested on Ubuntu 20.04 and 22.04.

## Contents
[[_TOC_]]

## Goals

This software is meant to give access to the impressive work around skeleton
detection using neural networks in the recent years. These new methods are
often hard to use and deploy, and as a lot of code and libraries created
throughout research projects they are not always well maintained throughout
time. Also in some cases the licenses prevent using them easily in commercial
work, which can includes artistic pieces. On top of that, deep neural network
frameworks have their own issues regarding deployment and maintainability.

LivePose is focused on implementing algorithms around hardened libraries.
For example the PoseNet implementation uses the OpenCV deep neural network
module (DNN). OpenCV has proven to be a stable and well-maintained library
which will hopefully allow for this implementation to work for many years.
And OpenCV supports a lot of acceleration backends for DNN.


## Installation

### Recommended - Installation from sources

To install `LivePose` from the sources, see instructions in [Installation.md](doc/Installation.md).

### Alternative Installation Instructions

The alternative installations each have their own page:
- [Installing Without GPU Support](doc/Installation_no_gpu.md)
- [Compiling Dependencies From Source](doc/Installation_compile_from_source.md)
- [Installing on a Jetson board](doc/Installation_jetson.md)

### Docker Image

We're currently in the process of rebasing LivePose's docker image. Please  follow [these legacy instructions](https://hub.docker.com/r/satmtl/livepose) to use LivePose from the docker Image:

```bash
# On Ubuntu:
# From the NVIDIA documentation
distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
      && curl -s -L https://nvidia.github.io/libnvidia-container/gpgkey | sudo apt-key add - \
      && curl -s -L https://nvidia.github.io/libnvidia-container/$distribution/libnvidia-container.list | sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list

sudo apt update
sudo apt install docker.io nvidia-container-toolkit

sudo systemctl restart docker

docker pull satmtl/livepose:latest
```



## Running LivePose

### Using the Docker image

Even though it is possible to run the image directly using `docker` and specifying the parameters, it is strongly advised to use the scripts available alongside the Dockerfile in the [image repository](https://gitlab.com/sat-mtl/distribution/docker-images/), and follow the [instructions from the README.md file](https://gitlab.com/sat-mtl/distribution/docker-images/-/tree/master/livepose#running-the-image).

### From the code repository
From the root directory of `LivePose`, run:
```bash
./bin/livepose
```

The above command requires installation. To try out `LivePose` without installing,
it can be run locally from the root directory of `LivePose` with:
```bash
./livepose.sh
```

To verify that the capture is working, we can use `oscdump` (with its default
settings, `LivePose` sends OSC messages locally on the port 9000)
```bash
oscdump 9000
```

## Configuration

LivePose configuration is done through a JSON file. By default the file located
at `./config/default.json` will be used. The configuration allows for
configuring the following elements:

* backend
* camera(s)
* filter(s)
* output(s)

The configuration file can be specified from the command line with the `-c` option. For example:
```bash
./bin/livepose -c  /path/to/your/config.json
```

A full specification of the config options is given in [Config.md](doc/Config.md)

## Models
Several different models are included in the model directory, `livepose/livepose/models/`.
There are models available for each backend in the respective sub-directories (`posenet/` and `trt/`).

For PoseNet, we currently support models in the `.onnx` and `.pb` formats.
All of the included `.pb` models were converted directly from the original TensorflowJS models, which can be found in
`livepose/livepose/models/posenet/tensorflowjs/`.
To convert the models yourself, you can use the [tfjs-to-tf](https://github.com/patlevin/tfjs-to-tf) command:

```
  tfjs_graph_converter path/to/tfjs/model.json path/to/tf/output/model.pb
```

For example, to re-create the float stride16 mobilenet model included here,
assuming you are in the top-level livepose directory, you would run:
```
  tfjs_graph_converter \
    livepose/models/posenet/tensorflowjs/mobilenet_float_100_v1/model-stride16.json \
    livepose/models/posenet/mobilenet_float_100_v1_stride16_frozen_model.pb
```

## WebSocket
### SSL
If the application receiving WebSocket messages does not run on the same
machine running LivePose, it might be necessary to use SSL. For example, modern
browsers will require the WebSocket to be authenticated with a SSL certificate.
This line can be used to create a self-signed SSL certificate and its
corresponding private key:

```bash
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365 -nodes -sha256
```
The paths to the SSL key and the SSL certificate must be added to the LivePose
config. The config
[example_websocket_output.json](https://gitlab.com/sat-mtl/tools/livepose/-/blob/develop/config/example_websocket_output.json)
shows an example.

The paths can be either absolute or relative to the working folder. For
example, if the relative path to the certificate is `ssl/cert.pem` when
LivePose is run with `./livepose.sh`, then it will become
`livepose/ssl/cert.pem` when running livepose with `./livepose/livepose.sh`.

On the client side, the address to the WebSocket must include `wss`. For example,
if the non-SSL address was `ws://10.10.10.2:8765`, the address with SSL will
become `wss://10.10.10.2:8765`.

Modern browsers require the user to authorize self-signed SSL certificates
manually. For this, type the WebSocket address into the address bar, but replacing `wss` by `https`. For example, if the WebSocket address is `wss://10.10.10.2:8765`, type `https://10.10.10.2:8765` into the address bar. Then accept the certificate.

## Filters

### Action detection filter from RGB frames

This filter detects action from webcam feed. To test the filter use the [frame_liveaction.json](https://gitlab.com/sat-metalab/livepose/-/blob/main/livepose/configs/frame_liveaction.json) configuration file
and allow a few seconds to receive results. The latency is because of temporal action detection (vs frame-based) to yield more
accurate results. 

The aforementioned config includes [I3D](https://github.com/open-mmlab/mmaction2/blob/master/configs/recognition/i3d/README.md) model pre-trained on [Kinetics](https://github.com/open-mmlab/mmaction2/blob/master/tools/data/kinetics/README.md) dataset from [mmaction2](https://github.com/open-mmlab/mmaction2) repo; meaning, the set of actions to expect
as output are the classes of this dataset. To gain a good understanding of the model performance we output the top three
most confident prediction of action detection model.

### Demo

For the specific combination of model and dataset, the following actions are detected more easily: *drinking and eating classes
(including testing beer, testing food), head-related actions (fixing hair, massaging head, filling eyebrows, applying cream), and stretching arms.*

### Models

Below are a list of some other models that output more accurate predictions with webcam feed data. You can change the 
model in the config file:
* [tpn 1](https://github.com/open-mmlab/mmaction2/blob/master/configs/recognition/tpn/tpn_slowonly_r50_8x8x1_150e_kinetics_rgb.py)
* [tpn 2](https://github.com/open-mmlab/mmaction2/blob/master/configs/recognition/tpn/tpn_imagenet_pretrained_slowonly_r50_8x8x1_150e_kinetics_rgb.py)
* [x3d](https://github.com/open-mmlab/mmaction2/blob/master/configs/recognition/x3d/x3d_s_13x6x1_facebook_kinetics400_rgb.py)
* [tanet](https://github.com/open-mmlab/mmaction2/blob/master/configs/recognition/tanet/tanet_r50_dense_1x1x8_100e_kinetics400_rgb.py)




## License
LivePose is free software released under the GNU General Public License, version 3.0.
For more details see the included license file [License.md](License.md)
or read more on the [GNU website](https://www.gnu.org/licenses/gpl-3.0.html).

## Citation

Please cite this paper in your publications if LivePose is related to your research: 

> Christian Frisson, Gabriel N. Downs, Marie-Ève Dumas, Farzaneh Askari, and Emmanuel Durand. 2022. LivePose: Democratizing Pose Detection for Multimedia Arts and Telepresence Applications on Open Edge Devices. In 28th ACM Symposium on Virtual Reality Software and Technology (VRST '22). Association for Computing Machinery, New York, NY, USA, Article 66, 1–2. https://doi.org/10.1145/3562939.3565660

```
@inproceedings{10.1145/3562939.3565660,
author = {Frisson, Christian and Downs, Gabriel N. and Dumas, Marie-\`{E}ve and Askari, Farzaneh and Durand, Emmanuel},
title = {LivePose: Democratizing Pose Detection for Multimedia Arts and Telepresence Applications on Open Edge Devices},
year = {2022},
isbn = {9781450398893},
publisher = {Association for Computing Machinery},
address = {New York, NY, USA},
url = {https://doi.org/10.1145/3562939.3565660},
doi = {10.1145/3562939.3565660},
abstract = {We present LivePose: an open-source (GPL license) tool that democratizes pose detection for multimedia arts and telepresence applications, optimized for and distributed on open edge devices. We designed the architecture of LivePose with a 5-stage pipeline (frame capture, pose estimation, dimension mapping, filtering, output) sharing streams of data flow, distributable on networked nodes. We distribute LivePose and dependencies packages and filesystem images optimized for edge devices (NVIDIA Jetson). We showcase multimedia arts and telepresence applications enabled by LivePose.},
booktitle = {28th ACM Symposium on Virtual Reality Software and Technology},
articleno = {66},
numpages = {2},
keywords = {multimedia arts, telepresence, edge computing, pose detection},
location = {Tsukuba, Japan},
series = {VRST '22}
}
```
