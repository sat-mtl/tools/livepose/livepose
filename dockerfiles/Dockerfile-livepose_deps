FROM ubuntu:22.04

ENV DEBIAN_FRONTEND noninteractive

RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN apt update -qq \
  && apt install -y --no-install-recommends \
    git build-essential ca-certificates \
    coreutils gnupg2 wget software-properties-common sed \
  && export OS="ubuntu`lsb_release -rs | sed 's/\.//'`"\
  && wget https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/cuda-${OS}.pin \
  && mv cuda-${OS}.pin /etc/apt/preferences.d/cuda-repository-pin-600 \
  && apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/3bf863cc.pub \
  && add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/ /" \
  && wget -qO - https://sat-mtl.gitlab.io/distribution/mpa-$(lsb_release -cs)-amd64-nvidia/sat-metalab-mpa-keyring.gpg | gpg --dearmor | dd of=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg \
  && echo "deb [ arch=amd64, signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-$(lsb_release -cs)-amd64-nvidia/debs/ sat-metalab main" | tee /etc/apt/sources.list.d/sat-metalab-mpa.list \
  && wget -qO - https://sat-mtl.gitlab.io/distribution/mpa-datasets/sat-metalab-mpa-keyring.gpg | gpg --dearmor | dd of=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg \
  && echo 'deb [ signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-datasets/debs/ sat-metalab main' | tee /etc/apt/sources.list.d/sat-metalab-mpa-datasets.list \
  && apt update --allow-insecure-repositories -qq \
  && apt install -y --no-install-recommends --allow-unauthenticated \
    libpython3-dev python3 python3-pip python3-setuptools python3-venv python3-wheel \
    python3-cycler python3-dateutil python3-dev python3-filterpy python3-getch python3-imageio python3-lap \
    python3-matplotlib python3-networkx python3-numpy python3-scipy python3-six python3-skimage python3-tk \
    v4l-utils cython3 \
    python3-mmcv python3-mmdetection python3-xtcocotools python3-mmpose \
    python3-opencv \
    python3-liblo \
    python3-libmapper \
    python3-librealsense2 \
    python3-torch python3-torchvision python3-torch2trt python3-trt-pose \
    python3-websockets \
  && python3 -m venv --system-site-packages --symlinks livepose-venv \
  && source livepose-venv/bin/activate \
  && git clone https://gitlab.com/sat-mtl/tools/livepose/livepose.git \
  && cd livepose \
  && pip3 install -e .
