PYTHON=python3

.PHONY: all clean LIVEPOSE

all: LIVEPOSE

LIVEPOSE:
	$(PYTHON) setup.py build_ext --inplace

clean:
	find . -name '*cpython*.so' -delete
