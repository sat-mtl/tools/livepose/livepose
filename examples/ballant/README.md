# Ballant - a tiny movement game with LivePose

Previously mentionned on the [Metalab blog](https://sat-mtl.gitlab.io/metalab/blog/en/posts/livepose-ballant.html), updated here:

* [English version of the Ballant blogpost](https://sat-mtl.gitlab.io/documentation/livepose/en/livepose-ballant.html)
* [version en français du billet de blogue Ballant](https://sat-mtl.gitlab.io/documentation/livepose/fr/livepose-ballant.html)

## Run the game

To run this game, first install the game:

    cd examples/ballant
    npm install
    
and then launch the game:

    ./ballant.sh
    
The game will open in a web browser window.

LivePose will be running in an independent window.

## Exit the game

To exit the game, click on any of the open terminal window and press 'Ctrl' + 'C'. Do this for all open Terminal window. 
