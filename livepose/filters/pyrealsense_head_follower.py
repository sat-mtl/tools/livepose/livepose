import numpy as np
import sys

from typing import Any, Dict, List, Optional, Tuple, Union

from livepose.cameras.pyrealsense2 import PyRealSense2Camera
from livepose.dataflow import Channel, Flow, Stream
from livepose.filter import register_filter, Filter
from livepose.pose_backend import PosesForCamera, Pose, Keypoint

import logging

logger = logging.getLogger(__name__)


try:
    import pyrealsense2 as rs
    has_pyrealsense2 = True
except (ImportError, ModuleNotFoundError):
    has_pyrealsense2 = False


@register_filter("pyrealsense_head_follower")
class PyRealsenseHeadFollower(Filter):
    """
    """

    def __init__(self, *args: Any, **kwargs: Any):
        super(PyRealsenseHeadFollower, self).__init__("pyrealsense_head_follower", **kwargs)

    def add_parameters(self) -> None:
        self._parser.add_argument("--depth-clip", type=float, default=0.0, help="Depth clip")

    def init(self) -> None:
        if not has_pyrealsense2:
            logger.error("pyrealsense2 library not found, please install it")
            sys.exit(1)

    def compute_position(self, cam_id: int, color_frames: List[Channel], depth_frames: List[Channel], pt: np.array) -> Tuple[float, float, float]:
        """
        Compute the depth from screen points.
        :param frames: List[Dict[str, Any]] - List of all input video frames
        """
        color_frame = color_frames[cam_id].metadata[PyRealSense2Camera.COLOR_METADATA]
        depth_frame = depth_frames[cam_id].metadata[PyRealSense2Camera.DEPTH_METADATA]

        # Apply flipping and rotation to the input points, as the PyRealSense2 frames have
        # not been transformed (they are still the raw channel values)
        if color_frames[cam_id].metadata["flip"]:
            pt[0] = color_frames[cam_id].metadata["resolution"][0] - pt[0]

        if color_frames[cam_id].metadata["rotate"] != 0:
            angle = color_frames[cam_id].metadata["rotate"]
            res_x = color_frames[cam_id].metadata["resolution"][0]
            res_y = color_frames[cam_id].metadata["resolution"][1]

            if angle == 90:
                pt = np.array([
                    res_x - (pt[1] - res_y / 2),
                    (pt[0] - res_x / 2) + res_y
                ])
            elif angle == 180:
                pt = np.array([
                    res_x - (pt[0] - res_x / 2),
                    res_y - (pt[1] - res_y / 2)
                ])
            elif angle == 270:
                pt = np.array([
                    (pt[1] - res_y / 2) - res_x,
                    res_y - (pt[0] - res_x / 2)
                ])

        depth_scale = depth_frames[cam_id].metadata[PyRealSense2Camera.DEPTH_SCALE_METADATA]
        depth_min = 0.1
        depth_max = 2.0
        color_intrinsics = color_frame.profile.as_video_stream_profile().get_intrinsics()
        depth_intrinsics = depth_frame.profile.as_video_stream_profile().get_intrinsics()
        depth_to_color_extrinsics = depth_frame.profile.as_video_stream_profile().get_extrinsics_to(color_frame.profile)
        color_to_depth_extrinsics = color_frame.profile.as_video_stream_profile().get_extrinsics_to(depth_frame.profile)

        depth_ptr = rs.rs2_project_color_pixel_to_depth_pixel(
            depth_frame.get_data(),
            depth_scale,
            depth_min,
            depth_max,
            depth_intrinsics,
            color_intrinsics,
            depth_to_color_extrinsics,
            color_to_depth_extrinsics,
            [pt[0], pt[1]]
        )
        ptr = [int(round(depth_ptr[0])), int(round(depth_ptr[1]))]

        distance = depth_frame.as_depth_frame().get_distance(ptr[0], ptr[1])
        projected_pt = rs.rs2_deproject_pixel_to_point(depth_intrinsics, ptr, distance)
        return (projected_pt[0], projected_pt[1], projected_pt[2])

    def step(self, flow: Flow, now: float, dt: float) -> None:
        """
        Process the given image(s) with the DL Backend model
        :param flow: Flow - Data flow to read from and write to
        :param now: float - current time
        :param dt: float - time since last call
        :return: bool - success
        """
        super().step(flow=flow, now=now, dt=dt)

        result: Filter.Result = {}

        # Get input depth frames
        color_frames: List[Channel] = []
        depth_frames: List[Channel] = []
        input_streams = flow.get_streams_by_type(Stream.Type.INPUT)
        for stream in input_streams.values():
            if stream.has_channel_type(Channel.Type.COLOR):
                color_frames.append(stream.get_channels_by_type(Channel.Type.COLOR)[0])
            if stream.has_channel_type(Channel.Type.DEPTH):
                depth_frames.append(stream.get_channels_by_type(Channel.Type.DEPTH)[0])

        pose_streams = flow.get_streams_by_type(Stream.Type.POSE_BACKEND)

        for stream in pose_streams.values():
            assert(stream is not None)
            for channel in stream.get_channels_by_type(Channel.Type.POSE_2D):
                poses_for_cameras: List[PosesForCamera] = channel.data

                for cam_id, poses_for_camera in enumerate(poses_for_cameras):
                    result[cam_id] = {}
                    for pose_index, pose in enumerate(poses_for_camera.poses):
                        keypoint_dict = pose.keypoints

                        if keypoint_dict is None:
                            continue

                        selected_keypoints = [
                            keypoint_dict.get("RIGHT_EYE"),
                            keypoint_dict.get("LEFT_EYE"),
                            keypoint_dict.get("RIGHT_EAR"),
                            keypoint_dict.get("LEFT_EAR"),
                            keypoint_dict.get("NOSE")
                        ]

                        valid_keypoints = [point for point in selected_keypoints if point is not None]

                        if len(valid_keypoints) == 0:
                            continue

                        barycenter = np.array([0.0, 0.0])
                        for keypoint in valid_keypoints:
                            barycenter = barycenter + keypoint.position
                        barycenter = barycenter / len(valid_keypoints)

                        barycenter_3d = self.compute_position(cam_id, color_frames, depth_frames, barycenter)

                        if self._args.depth_clip > 0.0 and barycenter_3d[2] > self._args.depth_clip:
                            continue

                        result[pose_index] = [
                            barycenter_3d[0],
                            barycenter_3d[1],
                            barycenter_3d[2]
                        ]

        self._result = result

        if not flow.has_stream(self._filter_name):
            flow.add_stream(name=self._filter_name, type=Stream.Type.FILTER)

        flow.set_stream_frame(
            name=self._filter_name,
            frame=[Channel(
                type=Channel.Type.OUTPUT,
                data=self._result,
                name=self._filter_name,
                metadata={}
            )]
        )
