import cv2
import numpy as np

from typing import Any, Dict, List, Union

from livepose.dataflow import Channel, Flow, Stream
from livepose.filter import register_filter, Filter
from livepose.pose_backend import PosesForCamera, Pose, Keypoint


@register_filter("triangulate")
class TriangulateFilter(Filter):
    """
    Filter which outputs the 3D coordinates from 2 frames with different point of views.
    This implies that at least 2 calibrated cameras are required

    ### ISSUE AND LIMITATIONS:
        * how to match bodies from the 2 incoming images
        * only support one body for now
        * how to handle multiple cameras pairs
    The output `Dict` is as follow, for each person and body part:
    {base dict part}/triangulate/{Person ID}/{world X position} {world Y position} {world Z position}
    """

    def __init__(self, *args: Any, **kwargs: Any):
        super(TriangulateFilter, self).__init__("triangulate", **kwargs)

    def add_parameters(self) -> None:
        self._parser.add_argument("--Kmats", type=Dict[str, List[float]], default={}, help="Kmats")  # type: ignore
        self._parser.add_argument("--Rtmats", type=Dict[str, List[float]], default={}, help="Rtmats")  # type: ignore
        self._parser.add_argument("--dist-coeffs", type=Dict[str, List[float]],  # type: ignore
                                  default={}, help="Distortion coefficients")
        self._parser.add_argument("--res", type=int, nargs=2, default=(1920, 1080), help="Camera resolution")

    def init(self) -> None:
        self._Kmatrices: Dict[str, np.array] = {}
        self._Rtmatrices: Dict[str, np.array] = {}  # [R|t]
        self._dist_coeffs: Dict[str, np.array] = {}
        self._proj_matrices: Dict[str, np.array] = {}

        # reshape the intrinsics parameters to 3x3 matrices
        for kmat in self._args.Kmats:
            self._Kmatrices[kmat] = np.array([self._args.Kmats[kmat]]).reshape((3, 3))

        # if distortion coefficients, update the camera matrices
        # alpha=0: the rectified images are zoomed and shifted. Only valid pixels are visible
        # alpha=1: no source image pixels are lost
        alpha = 0.0
        if self._args.res is not None:
            for dist in self._args.dist_coeffs:
                self._dist_coeffs[dist] = np.array([self._args.dist_coeffs[dist]]).reshape((-1, 1))
                newcameramatrix, _ = cv2.getOptimalNewCameraMatrix(
                    self._Kmatrices[dist],
                    self._dist_coeffs[dist],
                    (self._args.res[0], self._args.res[1]),
                    alpha,
                    (self._args.res[0], self._args.res[1])
                )
                self._Kmatrices[dist] = newcameramatrix

        # reshape the extrinsics parameters to 3x4 matrices
        # and compute projection matrices
        for name in self._args.Rtmats:
            self._Rtmatrices[name] = np.array([self._args.Rtmats[name]]).reshape((3, 4))
            self._proj_matrices[name] = np.dot(self._Kmatrices[name], self._Rtmatrices[name])

    def triangulate_points(self, skeleton1: np.array, skeleton2: np.array) -> np.array:
        """
        Triangulate 2D points from 2 point of views
        """
        pose_left = np.array(skeleton1, np.float32)[:, 0:2].transpose()
        pose_right = np.array(skeleton2, np.float32)[:, 0:2].transpose()

        pose3D = np.ndarray((4, pose_left.shape[0]), dtype=np.float32)

        pose3D = cv2.triangulatePoints(list(self._proj_matrices.values())[0],
                                       list(self._proj_matrices.values())[1],
                                       pose_left,
                                       pose_right,
                                       pose3D)

        pose3D = pose3D[0:3, :] / pose3D[3, :]
        return pose3D.reshape((-1, 3))

    def step(self, flow: Flow, now: float, dt: float) -> None:
        """
        Process the given image(s) with the DL Backend model
        :param flow: Flow - Data flow to read from and write to
        :param now: float - current time
        :param dt: float - time since last call
        :return: bool - success
        """
        super().step(flow=flow, now=now, dt=dt)

        result: Filter.Result = {}

        # Get all pose streams
        pose_streams = flow.get_streams_by_type(Stream.Type.POSE_BACKEND)

        # All 2D poses from all streams are sent
        for stream in pose_streams.values():
            assert(stream is not None)
            for channel in stream.get_channels_by_type(Channel.Type.POSE_2D):
                poses_for_cameras: List[PosesForCamera] = channel.data

                # support only 2 devices
                if not poses_for_cameras or not len(poses_for_cameras) == 2:
                    return

                # We assume that both frames have the same number of bodies in the same order
                for pose_index, pose in enumerate(poses_for_cameras[0].poses):
                    result[pose_index] = {}

                    keypoints_dict_0 = pose.keypoints
                    keypoints_dict_1 = poses_for_cameras[1].poses[pose_index].keypoints
                    if not keypoints_dict_0 or not keypoints_dict_1:
                        continue

                    # only keep the keypoints detected on skeleton by both cameras
                    intersection = keypoints_dict_0.keys() & keypoints_dict_1.keys()

                    if not intersection:
                        return
                    pose_0 = np.array(
                        [keypoints_dict_0[key].position for key in intersection]
                    ).reshape((-1, 2))
                    pose_1 = np.array(
                        [keypoints_dict_1[key].position for key in intersection]
                    ).reshape((-1, 2))

                    pose3D = self.triangulate_points(pose_0, pose_1)

                    for index, keypt in enumerate(intersection):
                        result[pose_index][keypt] = [
                            float(pose3D[index][0]),
                            float(pose3D[index][1]),
                            float(pose3D[index][2])
                        ]

        self._result = result

        if not flow.has_stream(self._filter_name):
            flow.add_stream(name=self._filter_name, type=Stream.Type.FILTER)

        flow.set_stream_frame(
            name=self._filter_name,
            frame=[Channel(
                type=Channel.Type.OUTPUT,
                data=self._result,
                name=self._filter_name,
                metadata={}
            )]
        )
