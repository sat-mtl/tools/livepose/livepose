from typing import Any, List

from livepose.dataflow import Channel, Flow, Stream
from livepose.filter import register_filter, Filter
from livepose.pose_backend import PosesForCamera, Pose, Keypoint


@register_filter("mmaction2")
class MMAction2Filter(Filter):
    """
    Filter which formats skeleton output as required for mmaction2.

    The output is as follows:
        [
            {
                "frameNum": 0,
                "p1": [x1,y1,c1,x2,y2,c2,...,xn,yn,cn],
                "p2": [x1,y1,c1,x2,y2,c2,...,xn,yn,cn],
                ...
                "pm": [x1,y1,c1,x2,y2,c2,...,xn,yn,cn]
            },
            {
                "frameNum": 1,
                "p1": [x1,y1,c1,x2,y2,c2,...,xn,yn,cn],
                "p2": [x1,y1,c1,x2,y2,c2,...,xn,yn,cn],
                ...
                "pm": [x1,y1,c1,x2,y2,c2,...,xn,yn,cn]
            },
            ...
            {
                "frameNum": t,
                "p1": [x1,y1,c1,x2,y2,c2,...,xn,yn,cn],
                "p2": [x1,y1,c1,x2,y2,c2,...,xn,yn,cn],
                ...
                "pm": [x1,y1,c1,x2,y2,c2,...,xn,yn,cn]
            }
        ]
    """

    def __init__(self, *args: Any, **kwargs: Any):
        super(MMAction2Filter, self).__init__("mmaction2", **kwargs)

        self._frameNum: int = 0

    def add_parameters(self) -> None:
        pass

    def init(self) -> None:
        pass

    def step(self, flow: Flow, now: float, dt: float) -> None:
        """
        Update the filter
        :param flow: Flow - Data flow to read from and write to
        :param now: Current time
        :param dt: Time since last call
        """
        super().step(flow=flow, now=now, dt=dt)

        # Get all pose streams
        pose_streams = flow.get_streams_by_type(Stream.Type.POSE_BACKEND)

        result: Filter.Result = {}
        result["frameNum"] = self._frameNum

        # All 2D poses from all streams are sent
        for stream in pose_streams.values():
            assert(stream is not None)
            for channel in stream.get_channels_by_type(Channel.Type.POSE_2D):
                poses_for_cameras: List[PosesForCamera] = channel.data

                for cam_id, poses_for_camera in enumerate(poses_for_cameras):
                    for pose_index, pose in enumerate(poses_for_camera.poses):

                        p_key = "p" + str(pose_index + 1)

                        # Iter over each keypoint.
                        p_coords = []
                        for name, keypoint in pose.keypoints.items():
                            if keypoint is not None:
                                for c in keypoint.position:
                                    p_coords.append(float(c))
                                p_coords.append(float(keypoint.confidence))

                        result[p_key] = p_coords

        self._frameNum += 1
        self._result = result

        if not flow.has_stream(self._filter_name):
            flow.add_stream(name=self._filter_name, type=Stream.Type.FILTER)

        flow.set_stream_frame(
            name=self._filter_name,
            frame=[Channel(
                type=Channel.Type.OUTPUT,
                data=self._result,
                name=self._filter_name,
                metadata={}
            )]
        )
