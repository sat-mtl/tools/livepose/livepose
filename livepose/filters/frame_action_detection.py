import logging
import os
import sys
import time
from typing import Deque
from collections import defaultdict
from operator import itemgetter
from typing import Any, Dict, List
from collections import deque

from livepose.dataflow import Channel, Flow, Stream
from livepose.filter import register_filter, Filter

import numpy as np
import torch
from mmaction.apis import inference_recognizer, init_recognizer
from mmaction.models import build_recognizer
from mmaction.datasets.pipelines import Compose
from mmaction.core import OutputHook
import mmcv
from mmcv.parallel import collate, scatter
from mmcv.runner import load_checkpoint

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

ROOT_DIR = os.path.realpath(os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    "../.."
))

EXCLUED_STEPS = [
    'OpenCVInit', 'OpenCVDecode', 'DecordInit', 'DecordDecode', 'PyAVInit',
    'PyAVDecode', 'RawFrameDecode'
]


@register_filter("frame_action_detection")
class FrameActionDetection(Filter):
    """
        This filter detects actions from a sequence of RGB raw frames.
        For more information:
        https://github.com/open-mmlab/mmaction2
        https://github.com/open-mmlab/mmaction2/blob/v0.24.1/demo/webcam_demo.py
        https://github.com/open-mmlab/mmaction2/blob/v0.24.1/demo/demo_skeleton.py
    """

    def __init__(self, *args: Any, **kwargs: Any):
        super(FrameActionDetection, self).__init__(
            "frame_action_detection", **kwargs)

    def add_parameters(self) -> None:
        self._parser.add_argument('--mmaction-root', type=str, default=ROOT_DIR,
                                  help="Root path to which model weights and description files paths are relative to.")
        self._parser.add_argument("--action-detection-config-path", type=str, default="",
                                  help="action detection configuration file path")
        self._parser.add_argument(
            "--label-map-path", type=str, default="", help="label map file path")
        self._parser.add_argument(
            "--window-length", type=int, default=5, help="window length")
        self._parser.add_argument(
            "--window-interval", type=int, default=1, help="window interval")
        self._parser.add_argument("--camera-resolution", type=int, nargs=2,
                                  default=(848, 480), help="camera resolution")
        self._parser.add_argument(
            "--device", type=str, default="cuda:0", help="CUDA device path")
        self._parser.add_argument(
            "--pose-backend", type=str, default="mmpose", help="pose backend")
        self._parser.add_argument(
            "--average-size", type=int, default=3, help="average size")
        self._parser.add_argument(
            "--threshold", type=float, default=0.2, help="threshold")
        self._parser.add_argument(
            "--inference-fps", type=int, default=0, help="inference fps")
        self._parser.add_argument("--checkpoint-url", type=str,
                                  default="https://download.openmmlab.com/mmaction/recognition/i3d/i3d_r50_32x2x1_100e_kinetics400_rgb/i3d_r50_32x2x1_100e_kinetics400_rgb_20200614-c25ef9a4.pth", help="checkpoint url")

    def init(self) -> None:

        if self._args.mmaction_root != ROOT_DIR:
            self._args.action_detection_config_path = os.path.realpath(os.path.join(
                self._args.mmaction_root, self._args.action_detection_config_path))
            self._args.label_map_path = os.path.realpath(os.path.join(
                self._args.mmaction_root, self._args.label_map_path))
            self._args.checkpoint_url = os.path.realpath(os.path.join(
                self._args.mmaction_root, self._args.checkpoint_url))
        self._frame_shape = self._args.camera_resolution
        self._frame_interval = self._args.window_interval
        self._num_frames = self._args.window_length
        self._score_cache: Deque[float] = deque()
        self._scores_sum = 0
        self._model = init_recognizer(
            self._args.action_detection_config_path, self._args.checkpoint_url, self._args.device)
        self._label_map = [x.strip() for x in open(
            str(self._args.label_map_path)).readlines()]
        self._data = dict(img_shape=None, modality='RGB', label=-1)
        self._sample_length, self._test_pipeline = self.build_test_pipeine()

        # For each frame input, we queue 'window_length' frames
        self._frame_buffer: Dict[str, List[np.array]] = {}

    def build_test_pipeine(self):

        cfg = self._model.cfg
        sample_length = 0
        pipeline = cfg.data.test.pipeline
        pipeline_ = pipeline.copy()

        for step in pipeline:
            if 'SampleFrames' in step['type']:
                sample_length = step['clip_len'] * step['num_clips']
                self._data['num_clips'] = step['num_clips']
                self._data['clip_len'] = step['clip_len']
                pipeline_.remove(step)
            if step['type'] in EXCLUED_STEPS:
                # remove step to decode frames
                pipeline_.remove(step)
        test_pipeline = Compose(pipeline_)

        assert sample_length > 0

        return sample_length, test_pipeline

    def step(self, flow: Flow, now: float, dt: float) -> None:
        """
        Update the filter
        :param flow: Flow - Data flow to read from and write to
        :param now: Current time
        :param dt: Time since last call
        """
        super().step(flow=flow, now=now, dt=dt)

        result: Filter.Result = {}

        input_streams = flow.get_streams_by_type(Stream.Type.INPUT)
        for stream_name, stream in input_streams.items():
            assert(stream is not None)
            for channel in stream.get_channels_by_type(Channel.Type.COLOR):
                frame: np.array = channel.data
                if stream_name not in self._frame_buffer:
                    self._frame_buffer[stream_name] = []

                self._frame_buffer[stream_name].append(frame)

        for source_name, frames in self._frame_buffer.items():
            if len(frames) < self._sample_length:
                result[source_name] = ["waiting for action"]
                continue
            elif len(frames) == self._sample_length:
                cur_windows = np.array(frames)
                if self._data['img_shape'] is None:
                    self._data['img_shape'] = self._frame_shape
                inference_results = []
                try:
                    inference_results = list(
                        inference_recognizer(self._model, video=cur_windows))
                except RuntimeError as e:
                    logger.error(f"inference error: {e}")
                if len(inference_results) > 0:
                    result[stream_name] = {}
                    for i, inference_result in enumerate(inference_results):
                        if inference_result[0] < len(self._label_map):
                            result[stream_name][i] = {}
                            result[stream_name][i]["label"] = self._label_map[inference_result[0]]
                            result[stream_name][i]["score"] = float(
                                inference_result[1])
                        else:
                            logger.error(
                                f"label index mistmatch, check the value of num_classes in your action training and detection configs versus the amount of lines in your label map")
                            sys.exit(1)
                    if len(result[stream_name]) > 0 and result[stream_name][0]["score"] >= self._args.threshold:
                        logger.info(
                            f"class {result[stream_name][0]['label']} score {result[stream_name][0]['score']}")
                        result[source_name] = result[stream_name][0]['label']
                else:
                    result[source_name] = ["waiting for action"]
                frames.clear()

        self._result = result

        if not flow.has_stream(self._filter_name):
            flow.add_stream(name=self._filter_name, type=Stream.Type.FILTER)
        #
        flow.set_stream_frame(
            name=self._filter_name,
            frame=[Channel(
                type=Channel.Type.OUTPUT,
                data=self._result,
                name=self._filter_name,
                metadata={}
            )]
        )
