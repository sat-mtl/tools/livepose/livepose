import numpy as np  # type: ignore

from typing import Any, Dict, List, Optional

from livepose.dataflow import Channel, Flow, Stream
from livepose.filter import register_filter, Filter
from livepose.pose_backend import PosesForCamera, Keypoint


@register_filter("armup")
class ArmUpFilter(Filter):
    """
    Filter which outputs whether a person has one or both arms up. It is based
    on the pose detection. An arm is up (left or right) if either of the
    elbow or wrist keypoints are higher than the nose or neck.
    For more information about the bones:
    https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/output.md#keypoint-ordering

    OSC message is as follows, for frame:
    {base OSC path}/armup/{camera_id}/{index_of_body}{left/right} {True or False}
    """

    def __init__(self, *args: Any, **kwargs: Any):
        super(ArmUpFilter, self).__init__("armup", **kwargs)

    def add_parameters(self) -> None:
        pass

    def init(self) -> None:
        pass

    def step(self, flow: Flow, now: float, dt: float) -> None:
        """
        Process the given image(s) with the DL Backend model
        :param flow: Flow - Data flow to read from and write to
        :param now: float - current time
        :param dt: float - time since last call
        :return: bool - success
        """
        super().step(flow=flow, now=now, dt=dt)

        result: Filter.Result = {}

        # Get all pose streams
        pose_streams = flow.get_streams_by_type(Stream.Type.POSE_BACKEND)

        # All 2D poses from all streams are sent
        for stream in pose_streams.values():
            assert(stream is not None)
            for channel in stream.get_channels_by_type(Channel.Type.POSE_2D):
                poses_for_cameras: List[PosesForCamera] = channel.data

                threshold = 0.5
                for cam_id, poses_for_camera in enumerate(poses_for_cameras):
                    result[cam_id] = {}

                    for pose_index, pose in enumerate(poses_for_camera.poses):
                        result[cam_id][pose_index] = {}

                        keypoint_dict = pose.keypoints
                        if not keypoint_dict:  # or not keypoint_defs:
                            continue

                        left_armup = False
                        right_armup = False

                        # An arm is up if it is higher than the head given that the
                        # confidence > threshold
                        # The upper left corner has coordinates (0, 0) -> the arm is up
                        # if its y coordinate is smaller than the head / neck / shoulders
                        left_arm_keypoints: List[Optional[Keypoint]] = [
                            keypoint_dict.get("LEFT_ELBOW"),
                            keypoint_dict.get("LEFT_WRIST")
                        ]
                        valid_left_arm_keypoints = [k.position for k in left_arm_keypoints
                                                    if k is not None and k.confidence > threshold]

                        right_arm_keypoints: List[Optional[Keypoint]] = [
                            keypoint_dict.get("RIGHT_ELBOW"),
                            keypoint_dict.get("RIGHT_WRIST")
                        ]
                        valid_right_arm_keypoints = [k.position for k in right_arm_keypoints
                                                     if k is not None and k.confidence > threshold]

                        # Nose and Neck coordinates.
                        head_keypoints: List[Optional[Keypoint]] = [
                            keypoint_dict.get("NOSE"),
                            keypoint_dict.get("NECK")
                        ]
                        valid_head_keypoints = [k.position for k in head_keypoints
                                                if k is not None and k.confidence > threshold]

                        # Check if each elbow or wrist is higher than nose or neck.
                        for left_arm_keypoint in valid_left_arm_keypoints:
                            for head_keypoint in valid_head_keypoints:
                                # Image coordinates go from top to bottom.
                                if left_arm_keypoint[1] < head_keypoint[1]:
                                    left_armup = True
                                    break
                            if left_armup:
                                break

                        for right_arm_keypoint in valid_right_arm_keypoints:
                            for head_keypoint in valid_head_keypoints:
                                if right_arm_keypoint[1] < head_keypoint[1]:
                                    right_armup = True
                                    break
                            if right_armup:
                                break

                        result[cam_id][pose_index]["left"] = left_armup
                        result[cam_id][pose_index]["right"] = right_armup

        self._result = result

        if not flow.has_stream(self._filter_name):
            flow.add_stream(name=self._filter_name, type=Stream.Type.FILTER)

        flow.set_stream_frame(
            name=self._filter_name,
            frame=[Channel(
                type=Channel.Type.OUTPUT,
                data=self._result,
                name=self._filter_name,
                metadata={}
            )]
        )
