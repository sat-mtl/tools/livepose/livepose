import logging
import sys
from collections import deque
from collections import defaultdict
from operator import itemgetter
from typing import Any, Deque, Dict, List, Union
import time

from livepose.dataflow import Channel, Flow, Stream
from livepose.filter import register_filter, Filter
from livepose.pose_backend import PosesForCamera, Pose, Keypoint

import numpy as np
from mmaction.apis import inference_recognizer, init_recognizer
from mmaction.models import build_recognizer
from mmaction.datasets.pipelines import Compose
from mmaction.core import OutputHook
import mmcv
from mmcv.parallel import collate, scatter
from mmcv.runner import load_checkpoint
import torch

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


@register_filter("pose_action_detection")
class PoseActionDetection(Filter):
    """
        This filter detects actions from a sequence of RGB raw frames.
        For more information:
        https://github.com/open-mmlab/mmaction2
        https://github.com/open-mmlab/mmaction2/blob/v0.24.1/demo/demo_skeleton.py
    """

    def __init__(self, *args: Any, **kwargs: Any):
        super(PoseActionDetection, self).__init__(
            "pose_action_detection", **kwargs)

    def add_parameters(self) -> None:
        self._parser.add_argument("--action-detection-config-path", type=str,
                                  default="external/mmaction2/configs/skeleton/posec3d/slowonly_r50_u48_240e_ntu120_xsub_keypoint.py", help="action detection configuration file path")
        self._parser.add_argument("--label-map-path", type=str,
                                  default="external/mmaction2/tools/data/skeleton/label_map_ntu120.txt", help="label map file path")
        self._parser.add_argument(
            "--window-length", type=int, default=5, help="window length")
        self._parser.add_argument(
            "--window-interval", type=int, default=1, help="window interval")
        self._parser.add_argument("--camera-resolution", type=int, nargs=2,
                                  default=(640, 480), help="camera resolution")
        self._parser.add_argument(
            "--device", type=str, default="cuda:0", help="CUDA device path")
        self._parser.add_argument(
            "--pose-backend", type=str, default="mmpose", help="pose backend")
        self._parser.add_argument(
            "--average-size", type=int, default=3, help="average size")
        self._parser.add_argument(
            "--threshold", type=float, default=0.2, help="threshold")
        self._parser.add_argument(
            "--inference-fps", type=int, default=0, help="inference fps")
        self._parser.add_argument("--checkpoint-url", type=str,
                                  default="https://download.openmmlab.com/mmaction/skeleton/posec3d/slowonly_r50_u48_240e_ntu120_xsub_keypoint/slowonly_r50_u48_240e_ntu120_xsub_keypoint-6736b03f.pth", help="checkpoint url")

    def init(self) -> None:

        self._frame_shape = self._args.camera_resolution
        self._num_coords = 2
        self._num_score = 1
        self._num_keypoints = 0
        self._score_cache: Deque[float] = deque()
        self._scores_sum = 0

        h, w = self._frame_shape
        # Get clip_len, frame_interval and calculate center index of each clip
        config = mmcv.Config.fromfile(self._args.action_detection_config_path)
        try:
            self._num_keypoints = config['model']['backbone']['in_channels']
        except KeyError as e:
            logger.error(
                f"could not determine keypoint amount from config file {self._args.action_detection_config_path}: missing key {e}")
            sys.exit(1)
        # config.merge_from_dict(args.cfg_options)
        for component in config.data.test.pipeline:
            if component['type'] == 'PoseNormalize':
                component['mean'] = (w // 2, h // 2, .5)
                component['max_value'] = (w, h, 1.)
        self._model = init_recognizer(
            config, self._args.checkpoint_url, self._args.device)

        self._label_map = [x.strip() for x in open(
            str(self._args.label_map_path)).readlines()]
        self._pad = np.zeros((self._num_keypoints, self._num_coords))
        self._score_pad = np.zeros(self._num_keypoints)
        self._modality = "Pose"

        # For each pose input, we queue 'window_length' frames
        self._pose_buffer: Dict[str, List[List[PosesForCamera]]] = {}

    def convert_pose(self, poses):
        """
        convert pose to mmaction2 pose input format
        :param poses: List[PosesForCamera]
        :return: converted poses : Dict[str, Any]
        """

        # get max number of persons in this window
        max_num_persons = max([len(item.poses) for item in poses])

        converted_pose = np.zeros(
            (max_num_persons, self._args.window_length,
             self._num_keypoints, self._num_coords),
            dtype=np.float16
        )
        converted_pose_score = np.zeros(
            (max_num_persons, self._args.window_length, self._num_keypoints),
            dtype=np.float16
        )

        for frame_idx, frame_val in enumerate(poses):
            # frame without pose
            if not frame_val:
                converted_pose[:, frame_idx, :, :] = np.repeat(
                    self._pad[np.newaxis, :, :], max_num_persons, axis=0
                )
                converted_pose_score[:, frame_idx, :] = np.repeat(
                    self._score_pad[np.newaxis, :, :], max_num_persons, axis=0
                )
                pass

            if len(frame_val.poses) == 1:

                if len(list(frame_val.poses[0].keypoints.values())) == self._num_keypoints:
                    keypoints_col = np.array(
                        list(frame_val.poses[0].keypoints.values()))
                    keypoint = np.array(
                        [keypoint.position for keypoint in keypoints_col])
                    keypoint_score = np.array(
                        [keypoint.confidence for keypoint in keypoints_col])
                    converted_pose[0, frame_idx] = keypoint
                    converted_pose_score[0, frame_idx] = keypoint_score
                else:
                    for joint, joint_crd in frame_val.poses[0].keypoints.items():
                        idx = frame_val.poses[0].keypoints_definitions[joint]
                        keypoint, keypoint_score = joint_crd.position, joint_crd.confidence
                        converted_pose[0, frame_idx] = keypoint
                        converted_pose_score[0, frame_idx] = keypoint_score

            elif len(frame_val.poses) > 1:

                for person_idx, person_val in enumerate(frame_val.poses, ):
                    if len(list(person_val.keypoints.values())) == self._num_keypoints:
                        keypoints_col = np.array(
                            list(person_val.keypoints.values()))
                        keypoint = np.array(
                            [keypoint.position for keypoint in keypoints_col])
                        keypoint_score = np.array(
                            [keypoint.confidence for keypoint in keypoints_col])
                        converted_pose[person_idx, frame_idx] = keypoint
                        converted_pose_score[person_idx,
                                             frame_idx] = keypoint_score
                    else:
                        for joint, joint_crd in person_val.keypoints.items():
                            idx = person_val.keypoints_definitions[joint]
                            keypoint, keypoint_score = joint_crd.position, joint_crd.confidence
                            converted_pose[person_idx, frame_idx] = keypoint
                            converted_pose_score[person_idx,
                                                 frame_idx] = keypoint_score

        return converted_pose, converted_pose_score

    def create_fake_anno(self, converted_keypoints, converted_keypoints_score):

        fake_anno = dict()

        fake_anno["keypoint"] = converted_keypoints
        fake_anno["keypoint_score"] = converted_keypoints_score
        fake_anno["frame_dir"] = ""
        fake_anno["label"] = -1
        fake_anno["start_index"] = 0
        fake_anno["total_frames"] = self._args.window_length
        fake_anno["original_shape"] = self._frame_shape
        fake_anno["img_shape"] = self._frame_shape
        fake_anno["modality"] = self._modality

        return fake_anno

    def step(self, flow: Flow, now: float, dt: float) -> None:
        """
        Update the filter
        :param flow: Flow - Data flow to read from and write to
        :param now: Current time
        :param dt: Time since last call
        """
        super().step(flow=flow, now=now, dt=dt)

        result: Filter.Result = {}

        # This filter works with detected poses
        pose_streams = flow.get_streams_by_type(Stream.Type.POSE_BACKEND)

        for stream_name, stream in pose_streams.items():
            assert(stream is not None)
            for channel in stream.get_channels_by_type(Channel.Type.POSE_2D):
                poses_for_cameras: List[PosesForCamera] = channel.data
                if stream_name not in self._pose_buffer:
                    self._pose_buffer[stream_name] = []

                self._pose_buffer[stream_name].append(poses_for_cameras)

        for stream_name, poses in self._pose_buffer.items():
            if len(poses) < self._args.window_length:
                result[stream_name] = ["waiting for action"]
                continue
            elif len(poses) == self._args.window_length:

                flat_poses = [item for sublist in poses for item in sublist]
                converted_pose, converted_pose_score = self.convert_pose(
                    flat_poses)
                fake_anno = self.create_fake_anno(
                    converted_pose, converted_pose_score)
                torch.cuda.empty_cache()
                inference_results = []
                try:
                    inference_results = list(
                        inference_recognizer(self._model, fake_anno))
                except RuntimeError as e:
                    logger.error(f"inference error: {e}")
                if len(inference_results) > 0:
                    result[stream_name] = {}
                    for i, inference_result in enumerate(inference_results):
                        if inference_result[0] < len(self._label_map):
                            result[stream_name][i] = {}
                            result[stream_name][i]["label"] = self._label_map[inference_result[0]]
                            result[stream_name][i]["score"] = float(
                                inference_result[1])
                        else:
                            logger.error(
                                f"label index mistmatch, check the value of num_classes in your action training and detection configs versus the amount of lines in your label map")
                            sys.exit(1)
                else:
                    result[stream_name] = ["waiting for action"]
                poses.clear()
                print(result)

        self._result = result

        if not flow.has_stream(self._filter_name):
            flow.add_stream(name=self._filter_name, type=Stream.Type.FILTER)

        flow.set_stream_frame(
            name=self._filter_name,
            frame=[Channel(
                type=Channel.Type.OUTPUT,
                data=self._result,
                name=self._filter_name,
                metadata={}
            )]
        )
