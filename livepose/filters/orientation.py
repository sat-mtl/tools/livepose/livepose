import math
import numpy as np

from typing import Any, Dict, List

from livepose.dataflow import Channel, Flow, Stream
from livepose.filter import register_filter, Filter
from livepose.pose_backend import PosesForCamera, Pose, Keypoint


@register_filter("orientation")
class OrientationFilter(Filter):
    """
    Filter which outputs the orientation of detected bodies. It is based on the pose detection
    The filter outputs an angle between 0 and 90 degrees.
    * 0 degree is perfectly facing the camera or being perfectly turned back to the camera
    * 90 degree is when you are perpendicular to the camera

    Current limitations:
    * Requires hard coded values of bones length in pixel
    * Only works for one body at a time due to the calibration of the bones

    For more information about the bones:
    https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/output.md#keypoint-ordering

    OSC message is as follows, for frame:
    {base OSC path}/orientation/{camera_id}/{orientation of the ith body}
    """

    def __init__(self, *args: Any, **kwargs: Any):
        super(OrientationFilter, self).__init__("orientation", **kwargs)

    def add_parameters(self) -> None:
        pass

    def init(self) -> None:
        pass

    def step(self, flow: Flow, now: float, dt: float) -> None:
        """
        Process the given image(s) with the DL Backend model
        :param flow: Flow - Data flow to read from and write to
        :param now: float - current time
        :param dt: float - time since last call
        :return: bool - success
        """
        super().step(flow=flow, now=now, dt=dt)

        result: Filter.Result = {}

        threshold = 0.6

        # Get all pose streams
        pose_streams = flow.get_streams_by_type(Stream.Type.POSE_BACKEND)

        # All 2D poses from all streams are sent
        for stream in pose_streams.values():
            assert(stream is not None)
            for channel in stream.get_channels_by_type(Channel.Type.POSE_2D):
                poses_for_cameras: List[PosesForCamera] = channel.data

                for cam_id, poses_for_camera in enumerate(poses_for_cameras):
                    result[cam_id] = {}
                    for pose_index, pose in enumerate(poses_for_camera.poses):
                        keypoint_dict = pose.keypoints
                        if not keypoint_dict:
                            continue

                        # these values are person and distance specific
                        # They should be updated accordingly
                        # reference deltas for shoulders, torso, arms, thighs resp.
                        ref_deltas = [80., 160., 90., 130.]

                        # Make sure shoulders are valid
                        left_shoulder = keypoint_dict.get("RIGHT_SHOULDER")
                        right_shoulder = keypoint_dict.get("LEFT_SHOULDER")

                        if (left_shoulder is None or right_shoulder is None or left_shoulder.confidence <= threshold or right_shoulder.confidence <= threshold):
                            return

                        # Distance between shoulders.
                        length_shoulder = np.linalg.norm(
                            np.array(left_shoulder.position) - np.array(right_shoulder.position)
                        )

                        # Calculate lengths of other limbs.

                        # Length of torso, right arm, left arm, right thigh, left thigh.
                        limb_lengths = [0., 0., 0., 0., 0.]

                        # Pairs of joints making up the limbs listed above.
                        limb_joints = [
                            [keypoint_dict.get("NECK"),
                             keypoint_dict.get("MID_HIP")],
                            [keypoint_dict.get("RIGHT_SHOULDER"),
                             keypoint_dict.get("RIGHT_ELBOW")],
                            [keypoint_dict.get("LEFT_SHOULDER"),
                             keypoint_dict.get("LEFT_ELBOW")],
                            [keypoint_dict.get("RIGHT_HIP"),
                             keypoint_dict.get("RIGHT_KNEE")],
                            [keypoint_dict.get("LEFT_HIP"),
                             keypoint_dict.get("LEFT_KNEE")]
                        ]

                        for i in range(len(limb_lengths)):
                            joint_0, joint_1 = limb_joints[i]
                            if (joint_0 is not None and joint_1 is not None and joint_0.confidence > threshold and joint_1.confidence > threshold):
                                limb_lengths[i] = np.linalg.norm(
                                    np.array(joint_0.position) - np.array(joint_1.position)
                                )

                        # Scale the length of each limb by the reference lengths.
                        # Select the max of all ratios provide robustness to
                        # body movements.
                        ratio = max(
                            [length / delta
                             for length, delta in zip(limb_lengths, ref_deltas)]
                        )

                        if ratio == 0:
                            return

                        # Scale shoulders by the maximum ratio.
                        length_shoulder /= ratio

                        orientation = math.degrees(math.acos(min(
                            length_shoulder / ref_deltas[0],
                            1.0
                        )))

                        # only the first value is useful, the others are for the
                        # calibration step.
                        result[cam_id][pose_index] = [orientation, length_shoulder] + limb_lengths

        self._result = result

        if not flow.has_stream(self._filter_name):
            flow.add_stream(name=self._filter_name, type=Stream.Type.FILTER)

        flow.set_stream_frame(
            name=self._filter_name,
            frame=[Channel(
                type=Channel.Type.OUTPUT,
                data=self._result,
                name=self._filter_name,
                metadata={}
            )]
        )
