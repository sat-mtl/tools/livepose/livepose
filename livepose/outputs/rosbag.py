import json
import logging
import numpy as np
import time
import threading

from dataclasses import asdict, dataclass
from queue import Queue
from rosbags import rosbag1, rosbag2
from rosbags.serde import serialize_cdr, serialize_ros1
from rosbags.typesys.types import std_msgs__msg__String as String
from rosbags.typesys.types import sensor_msgs__msg__Image as Image
from rosbags.typesys.types import std_msgs__msg__Header as Header
from rosbags.typesys.types import builtin_interfaces__msg__Time as Time

from typing import Any, Dict, List, Optional, Union

from livepose.dataflow import Channel, Flow
from livepose.output import register_output, Output
from livepose.pose_backend import PosesForCamera

logger = logging.getLogger(__name__)

DEFAULT_RECORD_PATH = "/tmp/livepose_rosbag_record"


@register_output("rosbag")
class RosbagOutput(Output):
    """
    Class which records all generated streams, or only the specified ones,
    into a single rosbag file.
    """

    # These are all the types handled by this output
    # Anything not mentionned here will be skipped
    TypeConversion: Dict[Channel.Type, Any] = {
        Channel.Type.COLOR: Image,
        Channel.Type.DEPTH: Image,
        Channel.Type.OUTPUT: String,
        Channel.Type.POSE_2D: String,
        Channel.Type.POSE_3D: String,
    }

    class Writer:
        def __init__(self, version: int, path: str, compress: bool = False):
            assert(version in [1, 2])
            self._version = version
            self._writer: Union[rosbag1.Writer, rosbag2.Writer] = rosbag1.Writer(
                path) if version == 1 else rosbag2.Writer(path)

            if compress:
                if version == 1:
                    self._writer.set_compression(rosbag1.Writer.CompressionFormat.BZ2)  # type: ignore
                else:
                    self._writer.set_compression(
                        rosbag2.Writer.CompressionMode.MESSAGE,  # type: ignore
                        rosbag2.Writer.CompressionFormat.ZSTD  # type: ignore
                    )

        def open(self) -> None:
            self._writer.open()

        def close(self) -> None:
            self._writer.close()

        def get_connection(self, topic: str, msgtype: str) -> Union[rosbag1.writer.Connection, rosbag2.writer.Connection]:
            connections: List[Union[rosbag1.writer.Connection, rosbag2.writer.Connection]] = [
                connection for connection in self._writer.connections if connection.topic == topic]
            connection: Optional[Union[rosbag1.writer.Connection, rosbag2.writer.Connection]] = None

            if not connections:
                if self._version == 1:
                    connection = self._writer.add_connection(topic, msgtype)
                else:
                    connection = self._writer.add_connection(topic, msgtype, 'cdr', '') # type: ignore
            else:
                connection = connections[0]

            return connection

        def write(self,
                  connection: Union[rosbag1.writer.Connection, rosbag2.writer.Connection],
                  timestamp: int,
                  data: Any,
                  msgtype: str):
            if self._version == 1:
                self._writer.write(connection, timestamp, serialize_ros1(data, msgtype))
            else:
                self._writer.write(connection, timestamp, serialize_cdr(data, msgtype))

    @dataclass
    class Frame:
        topic: str
        timestamp: int
        data: Any
        msgtype: str

    def __init__(self, **kwargs: Any):
        super(RosbagOutput, self).__init__("record", **kwargs)

    def add_parameters(self) -> None:
        self._parser.add_argument("--channels", type=str, default="OUTPUT")
        self._parser.add_argument("--compress", type=bool, default=False)
        self._parser.add_argument("--path", type=str, default=DEFAULT_RECORD_PATH)
        self._parser.add_argument("--rosbag_version", type=int, default=1)
        self._parser.add_argument("--chunk_duration", type=int, default=0)

    def init(self) -> None:
        self._channels = self._args.channels.replace(' ', '').split(',')
        self._rosbag_version = self._args.rosbag_version if self._args.rosbag_version in [1, 2] else 1
        self._chunk_duration = self._args.chunk_duration
        self._chunk_id = 0
        self._start_time = time.strftime("%Y-%m-%d_%H:%M:%S")
        self._chunk_start_time = time.time()

        self._path = self._get_write_path()

        self._continue_write = True
        self._new_frame_condition = threading.Condition()
        self._frame_queue: Queue[RosbagOutput.Frame] = Queue()

        self._writer_thread = threading.Thread(
            target=self.writer_thread_func
        )
        self._writer_thread.start()

    def writer_thread_func(self) -> None:
        """
        Thread function for handling write operations
        """
        self._writer = RosbagOutput.Writer(self._rosbag_version, self._path, self._args.compress)
        self._writer.open()

        while self._continue_write:
            with self._new_frame_condition:
                self._new_frame_condition.wait()

            while self._frame_queue.qsize() > 0:
                if self._chunk_duration:
                    new_time = time.time()
                    if new_time - self._chunk_start_time > self._chunk_duration:
                        self._end_chunk()
                        self._chunk_start_time = new_time

                frame = self._frame_queue.get()
                connection = self._writer.get_connection(frame.topic, frame.msgtype)
                self._writer.write(connection, frame.timestamp, frame.data, frame.msgtype)
                self._frame_queue.task_done()

        self._writer.close()

    def send_flow(self, flow: Flow, now: float, dt: float) -> None:
        """
        Send the results for the given filter
        :param flow: Flow which outputs will be sent
        """
        streams = flow.get_all_streams()

        for stream in streams.values():
            channels = []
            for channel_type in self._channels:
                channels += stream.get_channels_by_type(Channel.Type[channel_type])

            for channel in channels:
                data: Any = channel.data

                topic = f"/{stream.name}/{channel.name}"

                if channel.type not in RosbagOutput.TypeConversion:
                    continue

                # Get the message type
                msgtype: str = RosbagOutput.TypeConversion[channel.type].__msgtype__

                # Each channel type has to be serialized in its own way
                if channel.type is Channel.Type.COLOR:
                    color_as_array: np.array = data
                    width = channel.metadata["resolution"][0]
                    height = channel.metadata["resolution"][1]

                    color_image: Image = Image(
                        Header(
                            stamp=Time(sec=int(now), nanosec=int((now - int(now)) * 10**9)),
                            frame_id='color'
                        ),
                        width=width,
                        height=height,
                        encoding="bgr8",
                        is_bigendian=False,
                        step=width * 3,
                        data=color_as_array.reshape((width * height * 3))
                    )
                    self._frame_queue.put(RosbagOutput.Frame(
                        topic=topic,
                        timestamp=int(now * 10**9),
                        data=color_image,
                        msgtype=msgtype
                    ))

                elif channel.type is Channel.Type.DEPTH:
                    depth_as_array: np.array = data
                    width = channel.metadata["resolution"][0]
                    height = channel.metadata["resolution"][1]

                    depth_image: Image = Image(
                        Header(
                            stamp=Time(sec=int(now), nanosec=int((now - int(now)) * 10**9)),
                            frame_id='color'
                        ),
                        width=width,
                        height=height,
                        encoding=f"mono{8*depth_as_array.itemsize}",
                        is_bigendian=False,
                        step=width,
                        data=depth_as_array.reshape((width * height)).view(dtype=np.uint8)
                    )
                    self._frame_queue.put(RosbagOutput.Frame(
                        topic=topic,
                        timestamp=int(now * 10**9),
                        data=depth_image,
                        msgtype=msgtype
                    ))

                elif channel.type in [Channel.Type.POSE_2D, Channel.Type.POSE_3D]:
                    poses: List[PosesForCamera] = data
                    poses_as_string: str = str([asdict(pose) for pose in poses])
                    self._frame_queue.put(RosbagOutput.Frame(
                        topic=topic,
                        timestamp=int(now * 10**9),
                        data=String(poses_as_string),
                        msgtype=msgtype
                    ))

                elif channel.type is Channel.Type.POSE_FLOOR:
                    poses_as_string = str(data)
                    self._frame_queue.put(RosbagOutput.Frame(
                        topic=topic,
                        timestamp=int(now * 10**9),
                        data=String(poses_as_string),
                        msgtype=msgtype
                    ))

                elif channel.type is Channel.Type.OUTPUT:
                    data_as_string = json.dumps(data, indent=0)
                    self._frame_queue.put(RosbagOutput.Frame(
                        topic=topic,
                        timestamp=int(now * 10**9),
                        data=String(data_as_string),
                        msgtype=msgtype
                    ))

                with self._new_frame_condition:
                    self._new_frame_condition.notifyAll()

    def _get_write_path(self) -> str:
        """
        Set the path for the current rosbag file
        """
        path = ""
        if self._args.path == DEFAULT_RECORD_PATH:
            path = f"{DEFAULT_RECORD_PATH}_{self._start_time}"
            if self._rosbag_version == 1:
                if self._chunk_duration:
                    path = f"{path}_{str(self._chunk_id).zfill(3)}.bag"
                else:
                    path = f"{path}.bag"
        else:
            if self._chunk_duration:
                split = self._args.path.split(".bag")[0]
                path = f"{split}_{str(self._chunk_id).zfill(3)}.bag"
            else:
                path = self._args.path
        return path

    def _end_chunk(self) -> None:
        """
        Close the rosbag file cleanly and open a new one
        """
        self._writer.close()
        self._chunk_id += 1
        self._writer = RosbagOutput.Writer(self._rosbag_version, self._get_write_path(), self._args.compress)

        self._writer.open()

    def stop(self) -> None:
        """
        Close the rosbag file cleanly
        """
        self._continue_write = False
        with self._new_frame_condition:
            self._new_frame_condition.notifyAll()
        self._writer_thread.join()
