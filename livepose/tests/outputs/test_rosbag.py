"""Unit tests for livepose/outputs/rosbag.py"""

import numpy as np

from time import time
from unittest import TestCase

from livepose.dataflow import Channel, Flow, Stream
from livepose.outputs.rosbag import RosbagOutput


class TestRosbag1Recording(TestCase):
    def setUp(self):
        self.output = RosbagOutput(channels="COLOR, DEPTH, OUTPUT", rosbag_version=1)
        self.output.init()

    def tearDown(self):
        self.output.stop()

    def test_output_recording(self):
        input_name = "input_camera"
        input_color = "input_color"
        input_depth = "input_depth"
        filter_name = "filter_output"
        filter_channel = "filter_channel"

        flow = Flow()
        flow.add_stream(name=filter_name, type=Stream.Type.FILTER)
        flow.set_stream_frame(
            name=filter_name,
            frame=[Channel(
                name=filter_channel,
                data={},
                type=Channel.Type.OUTPUT,
                metadata={}
            )]
        )

        flow.add_stream(name=input_name, type=Stream.Type.INPUT)
        flow.set_stream_frame(
            name=input_name,
            frame=[Channel(
                name=input_color,
                data=np.zeros((4, 4, 3), dtype=np.uint8),
                type=Channel.Type.COLOR,
                metadata={
                    "resolution": [4, 4]
                }
            ), Channel(
                name=input_depth,
                data=np.zeros((4, 4), dtype=np.uint16),
                type=Channel.Type.DEPTH,
                metadata={
                    "resolution": [4, 4]
                }
            )]
        )

        self.output.send_flow(flow, time(), 0.0)


class TestRosbag2Recording(TestCase):
    def setUp(self):
        self.output = RosbagOutput(channels="COLOR, DEPTH, OUTPUT", rosbag_version=2)
        self.output.init()

    def tearDown(self):
        self.output.stop()

    def test_output_recording(self):
        input_name = "input_camera"
        input_color = "input_color"
        input_depth = "input_depth"
        filter_name = "filter_output"
        filter_channel = "filter_channel"

        flow = Flow()
        flow.add_stream(name=filter_name, type=Stream.Type.FILTER)
        flow.set_stream_frame(
            name=filter_name,
            frame=[Channel(
                name=filter_channel,
                data={},
                type=Channel.Type.OUTPUT,
                metadata={}
            )]
        )

        flow.add_stream(name=input_name, type=Stream.Type.INPUT)
        flow.set_stream_frame(
            name=input_name,
            frame=[Channel(
                name=input_color,
                data=np.zeros((4, 4, 3), dtype=np.uint8),
                type=Channel.Type.COLOR,
                metadata={
                    "resolution": [4, 4]
                }
            ), Channel(
                name=input_depth,
                data=np.zeros((4, 4), dtype=np.uint16),
                type=Channel.Type.DEPTH,
                metadata={
                    "resolution": [4, 4]
                }
            )]
        )

        self.output.send_flow(flow, time(), 0.0)
