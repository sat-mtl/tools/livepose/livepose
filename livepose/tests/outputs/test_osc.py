"""Unit tests for livepose/outputs/osc.py"""

from time import time
from threading import Thread
from unittest import TestCase

import liblo

from livepose.dataflow import Channel, Flow, Stream
from livepose.pose_backend import PoseBackend
from livepose.filter import Filter
from livepose.outputs.osc import BASE_OSC_PATH, OSCOutput

from typing import Any, List

# OSC address for testing.
ADDRESS = "127.0.0.1"
PORT = 9000

# Class to implement PoseBackend abstract methods


class PoseBackendForTesting(PoseBackend):
    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

    def add_parameters(self) -> None:
        super().add_parameters()

    def init(self) -> None:
        return

    def get_output(self) -> List[Channel]:
        return []

    def step_pose_detection(self, flow: Flow, now: float, dt: float) -> bool:
        return True

    def is_gpu_acceleration_active(self) -> bool:
        """Check if GPU acceleration is active."""
        return False

    def is_gpu_acceleration_needed(self) -> bool:
        """Check if GPU acceleration is needed."""
        return False


class TestSendOscResults(TestCase):
    """Test function for sending out osc results."""

    def start_frame(self, path, argv, types, src, data):
        self.FRAME_STARTED = True

    def end_frame(self, path, argv, types, src, data):
        self.FRAME_ENDED = True

    def start_filter(self, path, argv, types, src, data):
        self.FILTER_STARTED = True

    def end_filter(self, path, argv, types, src, data):
        self.FILTER_ENDED = True

    def setUp(self):
        self.pose_backend = PoseBackendForTesting()
        self.output = OSCOutput(destinations={ADDRESS: PORT}, send_filter_boundary_messages=True, send_frame_messages=True)
        self.output.init()

        # Status variables changed by osc messages.
        self.FRAME_STARTED = False
        self.FRAME_ENDED = False
        self.FILTER_STARTED = False
        self.FILTER_ENDED = False

        # Start OSC server to receive LivePose messages.
        self.osc_server = liblo.Server(PORT)
        self.osc_server.add_method(
            f"{BASE_OSC_PATH}/frame",
            None,
            self.start_frame
        )
        self.osc_server.add_method(
            f"{BASE_OSC_PATH}/end_frame",
            None,
            self.end_frame
        )

    def tearDown(self):
        self.osc_server.free()

    def test_no_filters(self):
        """Test only `frame` and `end_frame` messages are sent if there are no
        filters attached to the deep learning backend.
        """
        # Run LivePose method in thread.
        livepose_thread = Thread(
            target=OSCOutput.send_flow,
            args=(self.output, Flow(), 0.0, 0.0),
            daemon=True
        )

        livepose_thread.start()

        # Test frame started message received.
        self.assertFalse(self.FRAME_STARTED)
        self.osc_server.recv()
        self.assertTrue(self.FRAME_STARTED)

        # Test frame ended message receieved.
        self.assertFalse(self.FRAME_ENDED)
        self.osc_server.recv()
        self.assertTrue(self.FRAME_ENDED)

        livepose_thread.join()

    def test_no_results(self):
        """Test only `start_filter` and `end_filter` messages are sent if there
        is a filter with no results.
        """
        filter_name = "filter_output"
        channel_name = "dummy_channel"

        self.osc_server.add_method(
            f"{BASE_OSC_PATH}/{filter_name}/start_filter",
            None,
            self.start_filter
        )
        self.osc_server.add_method(
            f"{BASE_OSC_PATH}/{filter_name}/end_filter",
            None,
            self.end_filter
        )

        flow = Flow()
        flow.add_stream(name=filter_name, type=Stream.Type.FILTER)
        flow.set_stream_frame(
            name=filter_name,
            frame=[Channel(
                name=channel_name,
                data={},
                type=Channel.Type.OUTPUT,
                metadata={}
            )]
        )

        livepose_thread = Thread(
            target=OSCOutput.send_flow,
            args=(self.output, flow, time(), 0.0),
            daemon=True
        )

        livepose_thread.start()

        self.assertFalse(self.FRAME_STARTED)
        self.osc_server.recv()
        self.assertTrue(self.FRAME_STARTED)

        self.assertFalse(self.FILTER_STARTED)
        self.osc_server.recv()
        self.assertTrue(self.FILTER_STARTED)

        self.assertFalse(self.FILTER_ENDED)
        self.osc_server.recv()
        self.assertTrue(self.FILTER_ENDED)

        self.assertFalse(self.FRAME_ENDED)
        self.osc_server.recv()
        self.assertTrue(self.FRAME_ENDED)

        livepose_thread.join()
