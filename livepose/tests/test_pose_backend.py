"""Unit tests for PoseBackend base class."""

from unittest import TestCase

import numpy as np

from livepose.dataflow import Channel, Flow
from livepose.pose_backend import PosesForCamera, Pose, Keypoint, PoseBackend

from typing import Any, List

# Class to implement PoseBackend abstract methods


class PoseBackendForTesting(PoseBackend):
    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

    def add_parameters(self) -> None:
        super().add_parameters()

    def init(self) -> None:
        return

    def get_output(self) -> List[Channel]:
        return []

    def step_pose_detection(self, flow: Flow, now: float, dt: float) -> bool:
        return True

    def is_gpu_acceleration_active(self) -> bool:
        """Check if GPU acceleration is active."""
        return False

    def is_gpu_acceleration_needed(self) -> bool:
        """Check if GPU acceleration is needed."""
        return False


class TestPoseBackend(TestCase):
    def setUp(self) -> None:
        self.pose_backend = PoseBackendForTesting()

    def test_model_path(self) -> None:
        model_path: str = "/some/path"

        self.pose_backend.model_path = model_path
        self.assertEqual(self.pose_backend.model_path, model_path)


class TestGetBoundingBoxes(TestCase):
    """Unit tests for helper method PoseNetBackend.get_bounding_boxes."""

    def setUp(self) -> None:
        self.backend = PoseBackendForTesting()

    def test_no_keypoints(self) -> None:
        """Test empty numpy array is returned for each camera if no poses or
        keypoints are given.
        """
        self.backend._poses_for_cameras = [
            PosesForCamera(poses=[
                Pose(confidence=1.0, id=0, keypoints={}, keypoints_definitions={})
            ])
        ]
        result = np.empty((0, 5))
        bounding_boxes = np.array(self.backend.get_bounding_boxes())
        self.assertTrue((bounding_boxes == result).all())

        self.backend._poses_for_cameras = [
            PosesForCamera(poses=[
                Pose(confidence=1.0, id=0, keypoints={}, keypoints_definitions={}),
                Pose(confidence=1.0, id=0, keypoints={}, keypoints_definitions={})
            ])
        ]
        bounding_boxes = self.backend.get_bounding_boxes()
        self.assertTrue((bounding_boxes == result).all())

    def test_one_pose(self) -> None:
        """Test correct bounding box is returned for a single pose."""
        self.backend._poses_for_cameras = [
            PosesForCamera(poses=[
                Pose(
                    confidence=1.0,
                    id=0,
                    keypoints={
                        "NOSE": Keypoint(confidence=0.95, part="NOSE", position=[1, 1]),
                        "NECK": Keypoint(confidence=0.95, part="NECK", position=[1, 3]),
                        "LEFT_EYE": Keypoint(confidence=0.95, part="LEFT_EYE", position=[5, 0]),
                        "RIGHT_EYE": Keypoint(confidence=0.95, part="RIGHT_EYE", position=[3, 0])
                    },
                    keypoints_definitions={}
                )
            ])
        ]

        result = np.array([[1, 0, 5, 3, 0.95]])
        bounding_boxes = self.backend.get_bounding_boxes()
        self.assertTrue((bounding_boxes == result).all())
