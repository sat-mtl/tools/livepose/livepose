"""Unit tests for livepose/pose_backends/posenet.py backend."""

from unittest import TestCase

import cv2
import numpy as np
import os

from livepose.dataflow import Channel, Flow, Stream
from livepose.pose_backends.posenet import PoseNetBackend

from typing import Dict

# Tests for data classes.


class TestPart(TestCase):
    """Unit tests for Part data class."""

    def test_less_than_operator(self):
        """Test less than operator '<' compares Part.id"""
        p1 = PoseNetBackend.Part(heatmap_x=0, heatmap_y=0, id=0)
        p2 = PoseNetBackend.Part(heatmap_x=0, heatmap_y=0, id=1)
        p3 = PoseNetBackend.Part(heatmap_x=0, heatmap_y=0, id=0)
        p4 = PoseNetBackend.Part(heatmap_x=0, heatmap_y=0, id=-1)

        self.assertTrue(p1 < p2)
        self.assertFalse(p2 < p1)

        self.assertFalse(p1 < p3)
        self.assertFalse(p3 < p1)

        self.assertFalse(p1 < p4)
        self.assertTrue(p4 < p1)

    def test_greater_than_operator(self):
        """Test greater than operator '>' compares Part.id"""
        p1 = PoseNetBackend.Part(heatmap_x=0, heatmap_y=0, id=0)
        p2 = PoseNetBackend.Part(heatmap_x=0, heatmap_y=0, id=1)
        p3 = PoseNetBackend.Part(heatmap_x=0, heatmap_y=0, id=0)
        p4 = PoseNetBackend.Part(heatmap_x=0, heatmap_y=0, id=-1)

        self.assertFalse(p1 > p2)
        self.assertTrue(p2 > p1)

        self.assertFalse(p1 > p3)
        self.assertFalse(p3 > p1)

        self.assertTrue(p1 > p4)
        self.assertFalse(p4 > p1)


class TestMultiPersonPoseNet(TestCase):
    """
    Unit tests for the multi-person implementation of PoseNet
    """

    def setUp(self):
        self.backend = PoseNetBackend()


class TestScoreIsMaximumInLocalWindow(TestCase):
    """Unit tests for helper method PoseNet.score_is_maximum_in_local_window."""

    def setUp(self):
        self.backend = PoseNetBackend()

    def test_is_maximum(self):
        """Test the method returns true if the score is the maximum in radius."""

        heatmap = np.array([
            [[0.5], [0.5], [0.5]],
            [[0.5], [1.0], [0.5]],
            [[0.5], [0.5], [0.5]],
        ])

        result = self.backend.score_is_maximum_in_local_window(
            heatmap_x=1,
            heatmap_y=1,
            keypoint_id=0,
            local_maximum_radius=1,
            scores=heatmap
        )

        self.assertTrue(result)

    def test_is_not_maximum(self):
        """Test the method returns false if the score is not the maximum in radius."""
        heatmap = np.array([
            [[1.0], [1.0], [1.0]],
            [[1.0], [0.5], [1.0]],
            [[1.0], [1.0], [1.0]],
        ])

        result = self.backend.score_is_maximum_in_local_window(
            heatmap_x=1,
            heatmap_y=1,
            keypoint_id=0,
            local_maximum_radius=1,
            scores=heatmap
        )

        self.assertFalse(result)


class TestBuildPartWithScoreQueue(TestCase):
    """Unit tests for helper method PoseNet.build_part_with_score_queue."""

    def setUp(self):
        self.backend = PoseNetBackend()

    def test_queue_is_ordered_by_value(self):
        """
        These heatmap values have been computed by running the DNN
        in a Jupyter notebook, then keeping only a small part of it.
        From a 9x9x17 tensor, a 4x4x1 part has been extracted.
        """
        heatmap = np.array(
            [[[5.3732965e-06],
              [6.5074448e-05],
              [2.7162108e-05],
              [3.0306341e-05]],
             [[2.9957318e-06],
              [1.7177117e-05],
              [9.0190861e-06],
              [8.9713476e-06]],
             [[5.0751050e-06],
              [9.9910476e-06],
              [1.1036305e-04],
              [1.3970293e-04]],
             [[3.1442094e-05],
              [6.4972487e-06],
              [1.7201510e-05],
              [3.5599649e-05]]],
            dtype=np.float32
        )

        queue = self.backend.build_part_with_score_queue(
            score_threshold=1.0e-7,
            local_maximum_radius=1,
            scores=heatmap
        )

        previous = queue[0]
        for part in queue:
            self.assertTrue(previous[0] <= part[0])
            previous = part

    def test_parts_with_same_score_ordered_by_id(self):
        """Test if two parts have the same confidence score, they are ordered
        in the queue by their Part.id
        """
        heatmap = np.array(
            [[[1.0, 2.0, 1.0, 2.0, 1.0]]],
            dtype=np.float32
        )

        queue = self.backend.build_part_with_score_queue(
            score_threshold=0.0,
            local_maximum_radius=1,
            scores=heatmap
        )

        self.assertEqual(queue[0][1].id, 0)
        self.assertEqual(queue[1][1].id, 2)
        self.assertEqual(queue[2][1].id, 4)
        self.assertEqual(queue[3][1].id, 1)
        self.assertEqual(queue[4][1].id, 3)

    def test_parts_with_same_score_and_id_ordred_by_coords(self):
        """Test if multiple parts have the same confidence score and Part.id,
        they are ordered by y, then x coordinate.
        """
        heatmap = np.array(
            [[[1.0],
              [1.0]],
             [[1.0],
              [1.0]]],
            dtype=np.float32
        )

        queue = self.backend.build_part_with_score_queue(
            score_threshold=0.0,
            local_maximum_radius=1,
            scores=heatmap
        )

        self.assertEqual(queue[0][1].heatmap_x, 0)
        self.assertEqual(queue[0][1].heatmap_y, 0)

        self.assertEqual(queue[1][1].heatmap_x, 0)
        self.assertEqual(queue[1][1].heatmap_y, 1)

        self.assertEqual(queue[2][1].heatmap_x, 1)
        self.assertEqual(queue[2][1].heatmap_y, 0)

        self.assertEqual(queue[3][1].heatmap_x, 1)
        self.assertEqual(queue[3][1].heatmap_y, 1)


class TestMobileNetSinglePoseAccuracy(TestCase):
    """Unit tests to assess the accuracy of the Mobilenet model."""

    def setUp(self) -> None:
        params = {
            "base_model": "mobilenet",
            "num_cams": 1,
            "min_keypoint_confidence": 0.0,
            "min_pose_confidence": 0.15,
            "input_resolution": [500, 500],
            "multi_pose_detection": False,
            "output_stride": 16
        }
        self.backend = PoseNetBackend(**params)
        img_path = os.path.realpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "../data/mosaico.png"))

        flow = Flow()
        flow.add_stream(name="input", type=Stream.Type.INPUT)
        flow.set_stream_frame(
            name="input",
            frame=[Channel(
                name="color",
                data=cv2.imread(img_path),
                type=Channel.Type.COLOR,
                metadata={}
            )]
        )

        self.backend.init()
        self.backend.start()
        self.backend.step(flow, 0, 0)

        # reference keypoints from the image
        # The pixel values were determined by human inspection of the image
        # They are used as the ground truth
        self.reference_keypoints: Dict[str, np.array] = {
            "LEFT_SHOULDER": np.array([1276.0, 592.0, 1.0]),
            "RIGHT_SHOULDER": np.array([1345.0, 585.0, 1.0]),
            "LEFT_ELBOW": np.array([1227.0, 589.0, 1.0]),
            "RIGHT_ELBOW": np.array([1302.0, 532.0, 1.0]),
            "LEFT_WRIST": np.array([1170.0, 561.0, 1.0]),
            "RIGHT_WRIST": np.array([1302.0, 502.0, 1.0]),
            "LEFT_HIP": np.array([1273.0, 724.0, 1.0]),
            "RIGHT_HIP": np.array([1336.0, 720.0, 1.0]),
            "LEFT_ANKLE": np.array([1249.0, 934.0, 1.0]),
            "RIGHT_ANKLE": np.array([1372.0, 928.0, 1.0]),
            "LEFT_KNEE": np.array([1264.0, 844.0, 1.0]),
            "RIGHT_KNEE": np.array([1342.0, 840.0, 1.0]),
            # Not visible; rough estimation
            "NOSE": np.array([1293.0, 568.0, 1.0]),
            "LEFT_EYE": np.array([1314.0, 544.0, 1.0]),
            "RIGHT_EYE": np.array([1294.0, 555.0, 1.0]),
            "LEFT_EAR": np.array([1332.0, 544.0, 1.0]),
            "RIGHT_EAR": np.array([1303.0, 355.0, 1.0])
        }

    def tearDown(self) -> None:
        self.backend.stop()

    def test_single_pose_current_accuracy(self) -> None:
        """Unit test that highlights the current accuracy of the MobileNet model."""
        # Keypoint pixel values determined by running the inference on
        # the reference image. These values are hardcoded and should be
        # modified if the accuracy improves
        runtime_reference: Dict[str, np.array] = {
            "RIGHT_SHOULDER": np.array([1311.95578, 645.362285, 0.597010434]),
            "LEFT_KNEE": np.array([1239.48375, 841.266400, 0.508680165]),
            "RIGHT_ANKLE": np.array([1234.91550, 918.486204, 0.774614513])
        }

        # verify that only one person is detected
        self.assertTrue(len(self.backend.poses_for_cameras[0].poses) == 1)

        # current model detects 3 keypoints
        self.assertTrue(len(self.backend.poses_for_cameras[0].poses[0].keypoints) == 3)

        for joint, value in self.backend.poses_for_cameras[0].poses[0].keypoints.items():
            try:
                reference_value = runtime_reference[joint]
            except:
                # if the joint is not in the runtime-reference, the model detects a new joint
                self.fail("New joint detected")

            self.assertTrue(np.allclose(reference_value, np.array([*value.position, value.confidence]), atol=5e-3))

    def test_single_pose_with_score(self) -> None:
        """Unit test that computes an error metric on the detected keypoints
        vs the ground truth."""
        summ = 0.0
        for joint, value in self.backend.poses_for_cameras[0].poses[0].keypoints.items():
            summ += np.linalg.norm(np.array(value.position) - self.reference_keypoints[joint][0:2])

        # value obtained by running the inference
        runtime_reference = 230.89757
        self.assertTrue(abs(summ - runtime_reference) < 0.05)


class TestResNet50SinglePoseAccuracy(TestCase):
    """Unit tests to assess the accuracy of the Resnet model."""

    def setUp(self) -> None:
        params = {
            "base_model": "resnet50",
            "num_cams": 1,
            "min_keypoint_confidence": 0.0,
            "min_pose_confidence": 0.15,
            "input_resolution": [500, 500],
            "multi_pose_detection": False,
            "output_stride": 16
        }
        self.backend = PoseNetBackend(**params)
        img_path = os.path.realpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "../data/mosaico.png"))

        flow = Flow()
        flow.add_stream(name="input", type=Stream.Type.INPUT)
        flow.set_stream_frame(
            name="input",
            frame=[Channel(
                name="color",
                data=cv2.imread(img_path),
                type=Channel.Type.COLOR,
                metadata={}
            )]
        )

        self.backend.init()
        self.backend.start()
        self.backend.step(flow, 0, 0)

        # reference keypoints from the image
        # The pixel values were determined by human inspection of the image
        # They are used as the ground truth
        self.reference_keypoints: Dict[str, np.array] = {
            "LEFT_SHOULDER": np.array([1276.0, 592.0, 1.0]),
            "RIGHT_SHOULDER": np.array([1345.0, 585.0, 1.0]),
            "LEFT_ELBOW": np.array([1227.0, 589.0, 1.0]),
            "RIGHT_ELBOW": np.array([1302.0, 532.0, 1.0]),
            "LEFT_WRIST": np.array([1170.0, 561.0, 1.0]),
            "RIGHT_WRIST": np.array([1302.0, 502.0, 1.0]),
            "LEFT_HIP": np.array([1273.0, 724.0, 1.0]),
            "RIGHT_HIP": np.array([1336.0, 720.0, 1.0]),
            "LEFT_ANKLE": np.array([1249.0, 934.0, 1.0]),
            "RIGHT_ANKLE": np.array([1372.0, 928.0, 1.0]),
            "LEFT_KNEE": np.array([1264.0, 844.0, 1.0]),
            "RIGHT_KNEE": np.array([1342.0, 840.0, 1.0]),
            # Not visible; rough estimation
            "NOSE": np.array([1293.0, 568.0, 1.0]),
            "LEFT_EYE": np.array([1314.0, 544.0, 1.0]),
            "RIGHT_EYE": np.array([1294.0, 555.0, 1.0]),
            "LEFT_EAR": np.array([1332.0, 544.0, 1.0]),
            "RIGHT_EAR": np.array([1303.0, 355.0, 1.0])
        }

    def tearDown(self) -> None:
        self.backend.stop()

    def test_single_pose_current_accuracy(self) -> None:
        """Unit test that highlights the current accuracy of the ResNet model."""
        # Keypoint pixel values determined by running the inference on
        # the reference image. These values are hardcoded and should be
        # modified if the accuracy improves
        runtime_reference: Dict[str, np.array] = {
            "LEFT_SHOULDER": np.array([1339.136390, 611.617530, 0.949844956]),
            "RIGHT_SHOULDER": np.array([1305.57116, 615.666232, 0.984520078]),
            "RIGHT_ELBOW": np.array([1248.26412, 585.636228, 0.890927672]),
            "LEFT_WRIST": np.array([1327.17502, 521.960629, 0.658919394]),
            "RIGHT_WRIST": np.array([1148.32253, 546.614060, 0.970456421]),
            "LEFT_HIP": np.array([1290.24697, 740.877646, 0.973540366]),
            "RIGHT_HIP": np.array([1248.79207, 749.760464, 0.976553500]),
            "LEFT_KNEE": np.array([1328.02251, 824.870780, 0.974290609]),
            "RIGHT_KNEE": np.array([1236.88820, 827.131079, 0.963019848]),
            "LEFT_ANKLE": np.array([1367.28047, 891.560122, 0.857080877]),
            "RIGHT_ANKLE": np.array([1258.55548, 911.612408, 0.581812561])
        }

        # verify that only one person is detected
        self.assertTrue(len(self.backend.poses_for_cameras[0].poses) == 1)

        # current model detects 11 keypoints
        self.assertTrue(len(self.backend.poses_for_cameras[0].poses[0].keypoints) == 11)

        for joint, value in self.backend.poses_for_cameras[0].poses[0].keypoints.items():
            try:
                reference_value = runtime_reference[joint]
            except:
                # if the joint is not in the runtime-reference, the model detects a new joint
                self.fail("New joint detected")

            self.assertTrue(np.allclose(reference_value, np.array([*value.position, value.confidence]), atol=5e-3))

    def test_single_pose_with_score(self) -> None:
        """Unit test that computes an error metric on the detected keypoints
        vs the ground truth."""
        summ = 0.0
        for joint, value in self.backend.poses_for_cameras[0].poses[0].keypoints.items():
            summ += np.linalg.norm(np.array(value.position) - self.reference_keypoints[joint][0:2])

        # value obtained by running the inference
        runtime_reference = 1043.24031
        self.assertTrue(abs(summ - runtime_reference) < 0.05)
