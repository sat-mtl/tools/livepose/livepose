"""Unit tests for livepose/filters/armup.py"""

from unittest import TestCase

from livepose.dataflow import Channel, Flow, Stream
from livepose.keypoints.body_25 import Body25Keypoints
from livepose.filters.armup import ArmUpFilter
from livepose.pose_backend import PosesForCamera, Pose, Keypoint


class TestStep(TestCase):
    def setUp(self):
        self.filter = ArmUpFilter()
        self.filter.init()

    def test_no_keypoints(self):
        """Test if no keypoints are provided, filter.result is empty"""
        self.assertEqual(self.filter.result, {})

        self.filter.step(
            flow=Flow(),
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result, {})

    def test_arms_low_confidence(self):
        """Test filter.result is False if all arm keypoints are below the
        confidence threshold (0.5 by default).
        """
        self.assertEqual(self.filter.result, {})

        pose = Pose(
            confidence=0.333,
            id=0,
            keypoints={
                "NOSE": Keypoint(confidence=1.0, part="NOSE", position=[1, 1]),
                "NECK": Keypoint(confidence=1.0, part="NECK", position=[1, 1]),
                "LEFT_WRIST": Keypoint(confidence=0.0, part="LEFT_WRIST", position=[2, 2]),
                "RIGHT_WRIST": Keypoint(confidence=0.0, part="RIGHT_WRIST", position=[2, 2]),
                "LEFT_ELBOW": Keypoint(confidence=0.0, part="LEFT_ELBOW", position=[2, 2]),
                "RIGHT_ELBOW": Keypoint(confidence=0.0, part="RIGHT_ELBOW", position=[2, 2]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[0][0]["left"], False)
        self.assertEqual(self.filter.result[0][0]["right"], False)

    def test_head_low_confidence(self):
        """Test filter.result is False if both the nose and neck keypoints are
        below the confidence threshold (0.5 by default).
        """
        self.assertEqual(self.filter.result, {})

        pose = Pose(
            confidence=0.667,
            id=0,
            keypoints={
                "NOSE": Keypoint(confidence=0.0, part="NOSE", position=[1, 1]),
                "NECK": Keypoint(confidence=0.0, part="NECK", position=[1, 1]),
                "LEFT_WRIST": Keypoint(confidence=1.0, part="LEFT_WRIST", position=[2, 2]),
                "RIGHT_WRIST": Keypoint(confidence=1.0, part="RIGHT_WRIST", position=[2, 2]),
                "LEFT_ELBOW": Keypoint(confidence=1.0, part="LEFT_ELBOW", position=[2, 2]),
                "RIGHT_ELBOW": Keypoint(confidence=1.0, part="RIGHT_ELBOW", position=[2, 2]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[0][0]["left"], False)
        self.assertEqual(self.filter.result[0][0]["right"], False)

    def test_left_arm_not_up(self):
        """Test filter.result is False for left arm if left arm keypoints are
        below nose and neck.
        """
        self.assertEqual(self.filter.result, {})

        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NOSE": Keypoint(confidence=1.0, part="NOSE", position=[1, 0]),
                "NECK": Keypoint(confidence=1.0, part="NECK", position=[1, 0]),
                "LEFT_WRIST": Keypoint(confidence=1.0, part="LEFT_WRIST", position=[0, 1]),
                "LEFT_ELBOW": Keypoint(confidence=1.0, part="LEFT_ELBOW", position=[0, 1]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[0][0]["left"], False)

    def test_right_arm_not_up(self):
        """Test filter.result is False for right arm if rght arm keypoints are
        below nose and neck.
        """
        self.assertEqual(self.filter.result, {})

        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NOSE": Keypoint(confidence=1.0, part="NOSE", position=[1, 0]),
                "NECK": Keypoint(confidence=1.0, part="NECK", position=[1, 0]),
                "RIGHT_WRIST": Keypoint(confidence=1.0, part="RIGHT_WRIST", position=[0, 1]),
                "RIGHT_ELBOW": Keypoint(confidence=1.0, part="RIGHT_ELBOW", position=[0, 1]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[0][0]["right"], False)

    def test_left_wrist_above_nose(self):
        """Test filter.result is True for left arm if left wrist is above nose."""
        self.assertEqual(self.filter.result, {})

        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NOSE": Keypoint(confidence=1.0, part="NOSE", position=[1, 2]),
                "LEFT_WRIST": Keypoint(confidence=1.0, part="LEFT_WRIST", position=[1, 1]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[0][0]["left"], True)

    def test_left_wrist_above_neck(self):
        """Test filter.result is True for left arm if left wrist is above neck."""
        self.assertEqual(self.filter.result, {})

        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NECK": Keypoint(confidence=1.0, part="NECK", position=[1, 2]),
                "LEFT_WRIST": Keypoint(confidence=1.0, part="LEFT_WRIST", position=[1, 1]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[0][0]["left"], True)

    def test_right_wrist_above_nose(self):
        """Test filter.result is True for right arm if right wrist is above nose."""
        self.assertEqual(self.filter.result, {})

        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NOSE": Keypoint(confidence=1.0, part="NOSE", position=[1, 2]),
                "RIGHT_WRIST": Keypoint(confidence=1.0, part="RIGHT_WRIST", position=[1, 1]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[0][0]["right"], True)

    def test_right_wrist_above_neck(self):
        """Test filter.result is True for right arm if right wrist is above nose."""
        self.assertEqual(self.filter.result, {})

        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NECK": Keypoint(confidence=1.0, part="NECK", position=[1, 2]),
                "RIGHT_WRIST": Keypoint(confidence=1.0, part="RIGHT_WRIST", position=[1, 1]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[0][0]["right"], True)

    def test_left_elbow_above_nose(self):
        """Test filter.result is True for left arm if left elbow is above nose."""
        self.assertEqual(self.filter.result, {})

        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NOSE": Keypoint(confidence=1.0, part="NOSE", position=[1, 2]),
                "LEFT_ELBOW": Keypoint(confidence=1.0, part="LEFT_ELBOW", position=[1, 1]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[0][0]["left"], True)

    def test_left_elbow_above_neck(self):
        """Test filter.result is True for left arm if left elbow is above neck."""
        self.assertEqual(self.filter.result, {})

        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NECK": Keypoint(confidence=1.0, part="NECK", position=[1, 2]),
                "LEFT_ELBOW": Keypoint(confidence=1.0, part="LEFT_ELBOW", position=[1, 1]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[0][0]["left"], True)

    def test_right_elbow_above_nose(self):
        """Test filter.result is True for right arm if right elbow is above nose."""
        self.assertEqual(self.filter.result, {})

        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NOSE": Keypoint(confidence=1.0, part="NOSE", position=[1, 2]),
                "RIGHT_ELBOW": Keypoint(confidence=1.0, part="RIGHT_ELBOW", position=[1, 1]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[0][0]["right"], True)

    def test_right_elbow_above_neck(self):
        """Test filter.result is True for right arm if right elbow is above neck."""
        self.assertEqual(self.filter.result, {})

        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NECK": Keypoint(confidence=1.0, part="NECK", position=[1, 2]),
                "RIGHT_ELBOW": Keypoint(confidence=1.0, part="RIGHT_ELBOW", position=[1, 1]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[0][0]["right"], True)

    def test_both_arms_up(self):
        """Test filter.result is True for both arms if they are both up."""
        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NECK": Keypoint(confidence=1.0, part="NECK", position=[1, 1]),
                "LEFT_ELBOW": Keypoint(confidence=1.0, part="LEFT_ELBOW", position=[1, 0]),
                "RIGHT_WRIST": Keypoint(confidence=1.0, part="RIGHT_WRIST", position=[1, 0]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[0][0]["right"], True)
        self.assertEqual(self.filter.result[0][0]["left"], True)

    def test_only_one_arm_up(self):
        """Test if one arm is up, it does not affect the result for the other
        arm.
        """
        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NECK": Keypoint(confidence=1.0, part="NECK", position=[1, 1]),
                "LEFT_ELBOW": Keypoint(confidence=1.0, part="LEFT_ELBOW", position=[1, 0]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[0][0]["left"], True)
        self.assertEqual(self.filter.result[0][0]["right"], False)

        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NECK": Keypoint(confidence=1.0, part="NECK", position=[1, 1]),
                "RIGHT_WRIST": Keypoint(confidence=1.0, part="RIGHT_WRIST", position=[1, 0]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[0][0]["left"], False)
        self.assertEqual(self.filter.result[0][0]["right"], True)

    def test_multiple_cameras(self):
        """Test filter.result contains the correct armup value for each
        separate camera.
        """
        pose_cam_1 = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NECK": Keypoint(confidence=1.0, part="NECK", position=[1, 1]),
                "RIGHT_ELBOW": Keypoint(confidence=1.0, part="RIGHT_ELBOW", position=[1, 0]),
            },
            keypoints_definitions=Body25Keypoints
        )

        pose_cam_2 = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NECK": Keypoint(confidence=1.0, part="NECK", position=[1, 1]),
                "LEFT_ELBOW": Keypoint(confidence=1.0, part="LEFT_ELBOW", position=[1, 2]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose_cam_1]), PosesForCamera(poses=[pose_cam_2])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[0][0]["right"], True)

        self.assertEqual(self.filter.result[0][0]["left"], False)

    def test_multiple_skeletons(self):
        """Test filter.result contains the correct armup value for each
        separate skeleton attached to a single camera.
        """
        pose_1 = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NECK": Keypoint(confidence=1.0, part="NECK", position=[1, 2]),
                "RIGHT_WRIST": Keypoint(confidence=1.0, part="RIGHT_WRIST", position=[1, 1]),
            },
            keypoints_definitions=Body25Keypoints
        )

        pose_2 = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "NECK": Keypoint(confidence=1.0, part="NECK", position=[1, 1]),
                "LEFT_WRIST": Keypoint(confidence=1.0, part="LEFT_WRIST", position=[1, 0]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose_1, pose_2])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[0][0]["right"], True)
        self.assertEqual(self.filter.result[0][0]["left"], False)

        self.assertEqual(self.filter.result[0][1]["right"], False)
        self.assertEqual(self.filter.result[0][1]["left"], True)
