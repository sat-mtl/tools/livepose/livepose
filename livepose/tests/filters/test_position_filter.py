"""Unit tests for livepose/filters/position.py"""

from unittest import TestCase

import numpy as np

from livepose.dataflow import Channel, Flow, Stream
from livepose.keypoints.body_25 import Body25Keypoints
from livepose.filters.position import PositionFilter
from livepose.pose_backend import PosesForCamera, Pose, Keypoint


class TestStep(TestCase):
    def setUp(self):
        self.filter = PositionFilter(
            Kmats={"/dev/video0": [
                532.6468964861642,
                0.0,
                322.8857310905135,
                0.0,
                497.14279954685696,
                243.38997065789061,
                0.0,
                0.0,
                1.0
            ]},
            Rtmats={"/dev/video0": [
                0.4513344543194968,
                -0.8861125765727516,
                -0.10536466193046157,
                -0.14965597754962875,
                -0.411626908562561,
                -0.10197357425932596,
                -0.9056294376288826,
                -0.38168482088186123,
                0.7917452232197868,
                0.45211269811008037,
                -0.41077196802638927,
                5.957351190828515
            ]}
        )
        self.filter.init()

    def test_no_keypoints(self):
        """Test if no keypoints are included, filter.result is empty"""
        self.assertEqual(self.filter.result, {})

        self.filter.step(
            flow=Flow(),
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result, {})

    def test_no_feet(self):
        """Test if no feet keypoints are included, filter.result is [0,0,0]"""
        pose = Pose(
            confidence=0.0,
            id=0,
            keypoints={
                "NOSE": Keypoint(confidence=0.0, part="NOSE", position=[0, 0]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[0][0], [0., 0., 0.])

    def test_feet_low_confidence(self):
        """Test if all feet keypoints have low confidence, filter.result is [0,0,0]"""
        pose = Pose(
            confidence=0.0,
            id=0,
            keypoints={
                "LEFT_HEEL": Keypoint(confidence=0.0, part="LEFT_HEEL", position=[0, 0]),
                "RIGHT_HEEL": Keypoint(confidence=0.0, part="RIGHT_HEEL", position=[0, 0]),
                "LEFT_BIG_TOE": Keypoint(confidence=0.0, part="LEFT_BIG_TOE", position=[0, 0]),
                "RIGHT_BIG_TOE": Keypoint(confidence=0.0, part="RIGHT_BIG_TOE", position=[0, 0]),
                "LEFT_SMALL_TOE": Keypoint(confidence=0.0, part="LEFT_SMALL_TOE", position=[0, 0]),
                "RIGHT_SMALL_TOE": Keypoint(confidence=0.0, part="RIGHT_SMALL_TOE", position=[0, 0]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result[0][0], [0., 0., 0.])

    def test_heels(self):
        """Test if the left and right heels are present, filter.result is set
        by filtr.compute_position
        """
        left_heel = np.array([1., 1., 1.])
        right_heel = np.array([1., 1., 1.])
        mid_feet = (left_heel[0:2] + right_heel[0:2]) / 2.0

        result = self.filter.compute_position(0, {}, mid_feet)
        # Last coordinate of result is always set as zero.
        result = [float(result[0]), float(result[1]), float(result[2])]

        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "LEFT_HEEL": Keypoint(confidence=left_heel[2], part="LEFT_HEEL", position=left_heel[0:2]),
                "RIGHT_HEEL": Keypoint(confidence=right_heel[2], part="RIGHT_HEEL", position=right_heel[0:2]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertTrue(self.filter.result[0][0] == result)

    def test_left_big_toe(self):
        """Test if heels are not present but left big toe is, it is used to
        compute position.
        """
        left_big_toe = np.array([1., 1., 1.])

        result = self.filter.compute_position(0, {}, left_big_toe[0:2])
        result = [float(result[0]), float(result[1]), float(result[2])]

        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "LEFT_BIG_TOE": Keypoint(confidence=left_big_toe[2], part="LEFT_BIG_TOE", position=left_big_toe[0:2]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertTrue(self.filter.result[0][0] == result)

    def test_right_big_toe(self):
        """Test if heels are not present but right big toe is, it is used to
        compute position.
        """
        right_big_toe = np.array([1., 1., 1.])

        result = self.filter.compute_position(0, {}, right_big_toe[0:2])
        result = [float(result[0]), float(result[1]), float(result[2])]

        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "RIGHT_BIG_TOE": Keypoint(confidence=right_big_toe[2], part="RIGHT_BIG_TOE", position=right_big_toe[0:2]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertTrue(self.filter.result[0][0] == result)

    def test_left_small_toe(self):
        """Test if heels are not present but left small toe is, it is used to
        compute position.
        """
        toe = np.array([1., 1., 1.])

        result = self.filter.compute_position(0, {}, toe[0:2])
        result = [float(result[0]), float(result[1]), float(result[2])]

        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "LEFT_SMALL_TOE": Keypoint(confidence=toe[2], part="LEFT_SMALL_TOE", position=toe[0:2]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertTrue(self.filter.result[0][0] == result)

    def test_right_small_toe(self):
        """Test if heels are not present but right small toe is, it is used to
        compute position.
        """
        toe = np.array([1., 1., 1.])

        result = self.filter.compute_position(0, {}, toe[0:2])
        result = [float(result[0]), float(result[1]), float(result[2])]

        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "RIGHT_SMALL_TOE": Keypoint(confidence=toe[2], part="RIGHT_SMALL_TOE", position=toe[0:2]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertTrue(self.filter.result[0][0] == result)

    def test_right_ankle(self):
        """Test if heels are not present but right ankle is, it is used to
        compute position.
        """
        ankle = np.array([1., 1., 1.])

        result = self.filter.compute_position(0, {}, ankle[0:2])
        result = [float(result[0]), float(result[1]), float(result[2])]

        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "RIGHT_ANKLE": Keypoint(confidence=ankle[2], part="RIGHT_ANKLE", position=ankle[0:2]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertTrue(self.filter.result[0][0] == result)

    def test_left_ankle(self):
        """Test if heels are not present but left ankle is, it is used to
        compute position.
        """
        ankle = np.array([1., 1., 1.])

        result = self.filter.compute_position(0, {}, ankle[0:2])
        result = [float(result[0]), float(result[1]), float(result[2])]

        pose = Pose(
            confidence=1.0,
            id=0,
            keypoints={
                "LEFT_ANKLE": Keypoint(confidence=ankle[2], part="LEFT_ANKLE", position=ankle[0:2]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertTrue(self.filter.result[0][0] == result)
