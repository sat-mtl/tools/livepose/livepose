"""
Unit tests for livepose/filters/triangulate.py
See blender file in livepose/livepose/tests/data/triangulate_filter.blend
for the full setup
"""
import cv2
import numpy as np
from unittest import TestCase

from livepose.dataflow import Channel, Flow, Stream
from livepose.keypoints.body_25 import Body25Keypoints
from livepose.filters.triangulate import TriangulateFilter
from livepose.pose_backend import PosesForCamera, Pose, Keypoint


class TestStep(TestCase):
    def setUp(self):
        # In Blender, both cameras have these settings:
        # horizontal fov 39.6 degrees,
        # focal length 50.0 mm
        # sensor width is 36.0 mm
        # resolution 1920x1080
        # with these values, we can infer the camera matrix:
        camera_matrix = np.array([
            [2666.6666, 0.0, 960.0],
            [0.0, 2666.6666, 540.0],
            [0.0, 0.0, 1.0]
        ])

        # Calibrate first camera
        cam1_keypoints_3d = [
            [-1.2627, 3.7373, 0.71429],
            [-1.0102, 3.9898, 0],
            [-1.2627, 3.7373, -0.71429],
            [-0.75761, 4.2424, 0.71429],
            [-0.50508, 4.4949, 0.35714],
            [-0.75761, 4.2424, -0.71429]
        ]

        cam1_keypoints_2d = [
            [60.0, 0.0],
            [285.0, 508.0],
            [60.0, 1014.0],
            [483.0, 60.0],
            [659.0, 289.0],
            [483.0, 954.0]
        ]

        (success, Rvec, tvec) = cv2.solvePnP(
            objectPoints=np.array([cam1_keypoints_3d]),
            imagePoints=np.array([cam1_keypoints_2d]),
            cameraMatrix=camera_matrix,
            distCoeffs=None,  # we have a perfect pinhole camera
            flags=cv2.SOLVEPNP_ITERATIVE
        )
        (rotation_mat, jacobian) = cv2.Rodrigues(Rvec)
        cam1_extrinsics = np.c_[rotation_mat, tvec]

        # To verify the computations, cam_1 is at the origin and
        # has a 90 degree rotation angle wrt x
        camera_position = (-np.array(rotation_mat).T @ tvec).reshape((3,))
        assert(np.allclose(camera_position, np.array([0.0, 0.0, 0.0]), atol=1e-01))
        camera_orientation = cv2.decomposeProjectionMatrix(cam1_extrinsics)[-1].reshape((3,))
        assert(np.allclose(camera_orientation, np.array([90.0, 0.0, 0.0]), atol=2))

        # Calibrate second camera
        cam2_keypoints_3d = [
            [-1.5152, 3.4848, 1.0714],
            [2.0, 5.0, 1.0],
            [0.0, 5.0, 0.0],
            [2.1429, 4.2424, -0.75761],
            [-0.71429, 3.9898, -1.0102],
            [-1.5152, 3.4848, -0.35714]
        ]

        cam2_keypoints_2d = [
            [108.0, 81.0],
            [1833.0, 20.0],
            [1020.0, 539.0],
            [1740.0, 995.0],
            [501.0, 981.0],
            [108.0, 693.0]
        ]

        (success, Rvec, tvec) = cv2.solvePnP(
            objectPoints=np.array([cam2_keypoints_3d]),
            imagePoints=np.array([cam2_keypoints_2d]),
            cameraMatrix=camera_matrix,
            distCoeffs=None,  # we have a perfect pinhole camera
            flags=cv2.SOLVEPNP_ITERATIVE
        )
        (rotation_mat, jacobian) = cv2.Rodrigues(Rvec)
        cam2_extrinsics = np.c_[rotation_mat, tvec]

        # To verify the computations, cam_2 is at [4.0, 0.0, 0.0]
        camera_position = (-np.array(rotation_mat).T @ tvec).reshape((3,))
        assert(np.allclose(camera_position, np.array([4.0, 0.0, 0.0]), atol=1e-01))

        Kmats = {
            "first_camera": camera_matrix,
            "second_camera": camera_matrix
        }
        Rtmats = {
            "first_camera": cam1_extrinsics,
            "second_camera": cam2_extrinsics
        }
        self.filter = TriangulateFilter(Kmats=Kmats, Rtmats=Rtmats)
        self.filter.init()

    def test_triangulate_points(self):
        """
        Test the triangulate_points function
        2d points were chosen from the Blender setup with the
        corresponding 3d point.
        """

        cam1_2d = np.array([[63.0, 542.0, 1.0]])
        cam2_2d = np.array([[264.0, 540.0, 1.0]])
        point_3d = self.filter.triangulate_points(cam1_2d, cam2_2d)

        self.assertTrue(np.allclose(
            point_3d,
            np.array([-1.2627, 3.7373, 0.0]).reshape((1, 3)),
            atol=5e-01)
        )

    def test_no_keypoints(self):
        """Test if no keypoints are included, filter.result is empty"""
        self.assertEqual(self.filter.result, {})

        self.filter.step(
            flow=Flow(),
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result, {})

    def test_one_camera(self):
        """Test if only one camera is provided, filter.result is empty"""
        keypoints = [[{"keypoints": {"NOSE": np.zeros(3)}}]]

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=keypoints,
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result, {})

    def test_two_cameras_different_keypoints(self):
        """
        Test if we have two cameras with keypoints, but no common keypoints,
        filter.result is empty
        """
        pose_1 = Pose(
            confidence=0.0,
            id=0,
            keypoints={
                "LEFT_HEEL": Keypoint(confidence=0.0, part="LEFT_HEEL", position=[0, 0]),
            },
            keypoints_definitions=Body25Keypoints
        )

        pose_2 = Pose(
            confidence=0.0,
            id=0,
            keypoints={
                "RIGHT_HEEL": Keypoint(confidence=0.0, part="RIGHT_HEEL", position=[0, 0]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose_1]), PosesForCamera(poses=[pose_2])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertEqual(self.filter.result, {})

    def test_two_cameras_shared_keypoints(self):
        """Test if keypoint is present in both camera, filter.result is set
        by filter.triangulate_points
        """
        cam1_left_heel = np.array([63.0, 542.0, 1.])
        cam2_left_heel = np.array([264.0, 540.0, 1.])

        result = self.filter.triangulate_points(
            cam1_left_heel.reshape((-1, 3)),
            cam2_left_heel.reshape((-1, 3))
        )

        pose_1 = Pose(
            confidence=0.0,
            id=0,
            keypoints={
                "LEFT_HEEL": Keypoint(confidence=1.0, part="LEFT_HEEL", position=cam1_left_heel[0:2]),
            },
            keypoints_definitions=Body25Keypoints
        )

        pose_2 = Pose(
            confidence=0.0,
            id=0,
            keypoints={
                "LEFT_HEEL": Keypoint(confidence=1.0, part="LEFT_HEEL", position=cam2_left_heel[0:2]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose_1]), PosesForCamera(poses=[pose_2])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertTrue(np.allclose(self.filter.result[0]["LEFT_HEEL"], result, atol=5e-06))

    def test_with_distortion(self):
        """
        Same test as the one above. But in this case, there is distortion.

        Test if keypoint is present in both camera, filter.result is set
        by filter.triangulate_points
        """

        def add_distortion(no_dist_vec: np.array, k1: float = 0.0) -> np.array:
            r2 = sum([x * x for x in no_dist_vec])
            L_r = 1. + k1 * r2
            distorted = [L_r * x for x in no_dist_vec]
            return distorted

        # In Blender, both cameras have these settings:
        # horizontal fov 39.6 degrees,
        # focal length 50.0 mm
        # sensor width is 36.0 mm
        # resolution 1920x1080
        # with these values, we can infer the camera matrix:
        camera_matrix = np.array([
            [2666.6666, 0.0, 960.0],
            [0.0, 2666.6666, 540.0],
            [0.0, 0.0, 1.0]
        ])

        # Calibrate first camera
        cam1_keypoints_3d = [
            [-1.2627, 3.7373, 0.71429],
            [-1.0102, 3.9898, 0],
            [-1.2627, 3.7373, -0.71429],
            [-0.75761, 4.2424, 0.71429],
            [-0.50508, 4.4949, 0.35714],
            [-0.75761, 4.2424, -0.71429]
        ]

        # In OpenCV, the distortion coefficients are represented as:
        # dist_coeff = [k1 k2 p1 p2 k3]
        # For us, it will be: [k1 0 0 0 0]
        # For cam1, let k1 = 0.1
        cam1_dist_coeffs = np.array([0.1, 0.0, 0.0, 0.0, 0.0])
        cam1_keypoints_2d = [
            add_distortion([60.0, 0.0], 0.1),
            add_distortion([285.0, 508.0], 0.1),
            add_distortion([60.0, 1014.0], 0.1),
            add_distortion([483.0, 60.0], 0.1),
            add_distortion([659.0, 289.0], 0.1),
            add_distortion([483.0, 954.0], 0.1)
        ]

        (success, Rvec, tvec) = cv2.solvePnP(
            objectPoints=np.array([cam1_keypoints_3d]),
            imagePoints=np.array([cam1_keypoints_2d]),
            cameraMatrix=camera_matrix,
            distCoeffs=cam1_dist_coeffs,
            flags=cv2.SOLVEPNP_ITERATIVE
        )
        (rotation_mat, jacobian) = cv2.Rodrigues(Rvec)
        cam1_extrinsics = np.c_[rotation_mat, tvec]

        # Calibrate second camera
        cam2_keypoints_3d = [
            [-1.5152, 3.4848, 1.0714],
            [2.0, 5.0, 1.0],
            [0.0, 5.0, 0.0],
            [2.1429, 4.2424, -0.75761],
            [-0.71429, 3.9898, -1.0102],
            [-1.5152, 3.4848, -0.35714]
        ]

        # For cam2, let k1 = 0.07
        cam2_dist_coeffs = np.array([0.07, 0.0, 0.0, 0.0, 0.0])
        cam2_keypoints_2d = [
            add_distortion([108.0, 81.0], 0.07),
            add_distortion([1833.0, 20.0], 0.07),
            add_distortion([1020.0, 539.0], 0.07),
            add_distortion([1740.0, 995.0], 0.07),
            add_distortion([501.0, 981.0], 0.07),
            add_distortion([108.0, 693.0], 0.07)
        ]

        (success, Rvec, tvec) = cv2.solvePnP(
            objectPoints=np.array([cam2_keypoints_3d]),
            imagePoints=np.array([cam2_keypoints_2d]),
            cameraMatrix=camera_matrix,
            distCoeffs=cam2_dist_coeffs,
            flags=cv2.SOLVEPNP_ITERATIVE
        )
        (rotation_mat, jacobian) = cv2.Rodrigues(Rvec)
        cam2_extrinsics = np.c_[rotation_mat, tvec]

        Kmats = {
            "first_camera": camera_matrix,
            "second_camera": camera_matrix
        }
        Rtmats = {
            "first_camera": cam1_extrinsics,
            "second_camera": cam2_extrinsics
        }
        dist_coeffs = {
            "first_camera": cam1_dist_coeffs,
            "second_camera": cam2_dist_coeffs
        }
        self.filter = TriangulateFilter(
            Kmats=Kmats,
            Rtmats=Rtmats,
            dist_coeffs=dist_coeffs,
            res=[1920, 1080]
        )
        self.filter.init()

        cam1_left_heel = np.array(add_distortion([63.0, 542.0], 0.1) + [1.0])
        cam2_left_heel = np.array(add_distortion([264.0, 540.0], 0.07) + [1.0])

        result = self.filter.triangulate_points(
            cam1_left_heel.reshape((-1, 3)),
            cam2_left_heel.reshape((-1, 3))
        )

        pose_1 = Pose(
            confidence=0.0,
            id=0,
            keypoints={
                "LEFT_HEEL": Keypoint(confidence=1.0, part="LEFT_HEEL", position=cam1_left_heel[0:2]),
            },
            keypoints_definitions=Body25Keypoints
        )

        pose_2 = Pose(
            confidence=0.0,
            id=0,
            keypoints={
                "LEFT_HEEL": Keypoint(confidence=1.0, part="LEFT_HEEL", position=cam2_left_heel[0:2]),
            },
            keypoints_definitions=Body25Keypoints
        )

        flow = Flow()
        flow.add_stream(name="pose_backend", type=Stream.Type.POSE_BACKEND)
        flow.set_stream_frame(
            name="pose_backend",
            frame=[Channel(
                name="dummy",
                data=[PosesForCamera(poses=[pose_1]), PosesForCamera(poses=[pose_2])],
                type=Channel.Type.POSE_2D,
                metadata={}
            )]
        )

        self.filter.step(
            flow=flow,
            now=0.0,
            dt=0.0
        )

        self.assertTrue(np.allclose(self.filter.result[0]["LEFT_HEEL"], result, atol=5e-06))
