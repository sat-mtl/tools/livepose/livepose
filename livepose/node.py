import argparse

from abc import ABCMeta, abstractmethod

from typing import Any

import sys

# https://stackoverflow.com/questions/55324449/how-to-specify-a-minimum-or-maximum-float-value-with-argparse/71112312#71112312


def ranged_type(value_type, min_value, max_value):
    """
    Return function handle of an argument type function for ArgumentParser checking a range:
        min_value <= arg <= max_value

    Parameters
    ----------
    value_type  - value-type to convert arg to
    min_value   - minimum acceptable argument
    max_value   - maximum acceptable argument

    Returns
    -------
    function handle of an argument type function for ArgumentParser


    Usage
    -----
        ranged_type(float, 0.0, 1.0)

    """

    def range_checker(arg: str):
        try:
            f = value_type(arg)
        except ValueError:
            raise argparse.ArgumentTypeError(f'must be a valid {value_type}')
        if f < min_value or f > max_value:
            raise argparse.ArgumentTypeError(f'must be within [{min_value}, {min_value}]')
        return f

    # Return function handle to checking function
    return range_checker


class Node:
    """
    Base class for nodes
    """

    def __init__(self, *args: Any, **kwargs: Any):
        self._parser = argparse.ArgumentParser()
        self._args: Any = {}

        self.add_parameters()
        self.parse_parameters(*args, **kwargs)

    @abstractmethod
    def add_parameters(self) -> None:
        """Add Node parameters."""
        pass

    @abstractmethod
    def init(self) -> None:
        """Init Node objects."""
        pass

    def parse_parameters(self, *args: Any, **kwargs: Any) -> None:
        """Parse Node parameters."""
        # Parse known args passed from commandline
        sys_args = self._parser.parse_known_args(sys.argv[1:])[0]
        # Generate config file parameters from parser arguments
        self._args = argparse.Namespace()
        actions = self._parser._option_string_actions
        for action in list(actions):
            argument = actions[action]
            arg = argument.dest
            arg_default = argument.default
            arg_value = argument.default
            kwarg = kwargs.get(
                arg,
                arg_default
            )
            # Apply config file arguments values
            if kwarg != '' and kwarg != None:
                arg_value = kwarg
            # Apply command line arguments values
            sys_arg = ''
            if arg in vars(sys_args):
                sys_arg = vars(sys_args)[arg]
                if sys_arg != '' and sys_arg != None and sys_arg != arg_default:
                    arg_value = sys_arg
            setattr(self._args, arg, arg_value)
