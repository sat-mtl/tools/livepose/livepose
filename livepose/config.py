from livepose.dimmap import DimMap
from livepose.output import Output
from livepose.filter import Filter
from livepose.pose_backend import PoseBackend
from livepose.camera import Camera
import logging
from importlib import __import__, reload
import numpy as np
import os
import json
import sys

from typing import Any, Dict, List, Optional

if sys.version_info >= (3, 9):
    from importlib.resources import open_text


logging.basicConfig(level=logging.ERROR)
logger = logging.getLogger(__name__)

POSE_BACKEND_SECTION = "pose_backends"
CAMERAS_SECTION = "cameras"
FILTERS_SECTION = "filters"
INPUTS_SECTION = "inputs"
OUTPUTS_SECTION = "outputs"
DIMMAPS_SECTION = "dimmaps"

nodes = [
    POSE_BACKEND_SECTION,
    CAMERAS_SECTION,
    FILTERS_SECTION,
    INPUTS_SECTION,
    OUTPUTS_SECTION,
    DIMMAPS_SECTION
]

ROOT_DIR = os.path.realpath(os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    ".."
))


class Config:
    """
    Utility class taking care of loading the configuration parameters
    """

    def __init__(self, path: str) -> None:
        self._path: str = path
        self._cam_paths: List[str] = []
        self._kmat_dict: Dict[str, np.array] = {}
        self._dist_coeffs_dict: Dict[str, np.array] = {}
        self._extrinsics_dict: Dict[str, np.array] = {}
        self._pose_backends: List[Dict[str, Any]] = []
        self._filters: Dict[str, Dict[str, Any]] = {}
        self._inputs: Dict[str, Dict[str, Any]] = {}
        self._outputs: Dict[str, Dict[str, Any]] = {}
        self._dimmaps: Dict[str, Dict[str, Any]] = {}
        self._resize_frames = False
        self.load_config()

    def list_params(self) -> None:
        """
        List params
        """
        params: Dict[str, Any] = {}

        package_path = os.path.dirname(os.path.realpath(__file__))

        for node in nodes:

            node_path = node
            if not node.endswith("s"):
                node_path = node + "s"

            params[node] = {}

            # Parse all backends:
            backends = os.listdir(os.path.join(package_path, node_path))
            # Test with individual backends only:
            # backends = ["backend.py"]

            node_class = node.replace("_", " ")
            if node_class.endswith("s"):
                node_class = node_class.strip("s")
            node_class_words = node_class.split(" ")
            node_class = ''.join(i.capitalize() for i in node_class_words)
            if node == "dimmaps":
                node_class = "DimMap"

            for backend in backends:
                if not backend.startswith("__") and backend.endswith(".py"):
                    backend = backend.split(".py")[0]

                    params[node][backend] = []

                    backend_path = f"livepose.{node_path}.{backend}"
                    try:
                        backend_import = reload(__import__(backend_path))
                    except ImportError as e:
                        logger.error(
                            f"Could not import {backend} from {backend_path}: {e}")
                        sys.exit(1)

                    backend_module = eval(node_class).create_from_name(
                        name=backend
                    )
                    actions = backend_module._parser._option_string_actions
                    for action in list(actions):
                        arg = actions[action]
                        d = arg.default
                        h = arg.help
                        if h is not None:
                            h = h.replace(ROOT_DIR+"/", "")
                            if isinstance(d, str):
                                d = d.replace(ROOT_DIR+"/", "")
                        backend_params: Dict[str, Any] = {}
                        backend_params["name"] = arg.dest
                        backend_params["type"] = type(arg.default).__name__
                        if arg.type is not None and arg.type.__name__ == "range_checker":
                            backend_params["min"] = arg.type.__closure__[0].cell_contents
                            backend_params["max"] = arg.type.__closure__[1].cell_contents
                            backend_params["type"] = arg.type.__closure__[2].cell_contents.__name__
                        if arg.choices is not None:
                            backend_params["choices"] = arg.choices

                        backend_params["default"] = d
                        backend_params["description"] = h
                        if arg.dest != "help":
                            params[node][backend].append(backend_params)

        json_string = json.dumps(params, indent=4)
        print(json_string)

    def load_config(self) -> None:
        """
        Load configuration file
        """
        # Find config file using multi-step search:
        # First check for matching local config files using file path.
        if os.path.exists(self._path):
            with open(self._path) as fp:
                data = json.load(fp)

        # If there's no matching local file, check for matching configs
        # included in the LivePose package data files.
        else:
            if sys.version_info >= (3, 9):
                with open_text("livepose.configs", self._path) as fp:
                    data = json.load(fp)
            else:
                package_path = os.path.dirname(os.path.realpath(__file__))
                with open(os.path.join(package_path, "configs", self._path), "r") as fp:
                    data = json.load(fp)

        # Backend configuration
        if POSE_BACKEND_SECTION in data:
            self._pose_backends = data[POSE_BACKEND_SECTION]

        # Cameras
        if CAMERAS_SECTION in data:
            calibration_data = data[CAMERAS_SECTION]
            self._cam_paths = calibration_data["input_paths"]
            self._cam_params = calibration_data.get("params", {})

            if "intrinsics" in calibration_data:
                for cam, matrix in calibration_data["intrinsics"].items():
                    self._kmat_dict[cam] = np.array([matrix]).reshape((3, 3))
                    self._extrinsics_dict[cam] = np.array([calibration_data["extrinsics"][cam]]).reshape((3, 4))
                    self._dist_coeffs_dict[cam] = np.array([calibration_data["distortion_coeffs"][cam]]).reshape((-1, 1))

            self._flip_cam = calibration_data.get("flip_camera", False)
            self._rotate_cam = calibration_data.get("rotate_camera", 0)

        # DimMaps configuration
        if DIMMAPS_SECTION in data:
            self._dimmaps = data[DIMMAPS_SECTION]

        # Filters configuration
        if FILTERS_SECTION in data:
            self._filters = data[FILTERS_SECTION]

        # Outputs configuration
        if INPUTS_SECTION in data:
            self._inputs = data[INPUTS_SECTION]

        # Outputs configuration
        if OUTPUTS_SECTION in data:
            self._outputs = data[OUTPUTS_SECTION]

    @property
    def pose_backends(self) -> List[Dict[str, Any]]:
        """
        Get a list of the pose backends
        :return: str
        """
        return self._pose_backends

    @property
    def cam_paths(self) -> List[str]:
        """
        Get the camera paths
        :return: List[str]
        """
        return self._cam_paths

    @property
    def cam_params(self) -> Dict[str, Dict[str, Any]]:
        """
        Get a dict by input paths containing a dict
        of parameters for each camera
        :return: Dict[Dict[str, Any]]
        """
        return self._cam_params

    @property
    def filters(self) -> List[str]:
        """
        Get a list of all desired filters
        :return: List[str] - A list of filters name
        """
        return [name for name in self._filters.keys()]

    @property
    def inputs(self) -> List[str]:
        """
        Get a list of all desired inputs
        :return: List[str] - A list of desired inputs
        """
        return [name for name in self._inputs.keys()]

    @property
    def outputs(self) -> List[str]:
        """
        Get a list of all desired outputs
        :return: List[str] - A list of desired outputs
        """
        return [name for name in self._outputs.keys()]

    @property
    def dimmaps(self) -> List[str]:
        """
        Get a list of all desired dimmaps
        :return: List[str] - A list of dimmaps name
        """
        return [name for name in self._dimmaps.keys()]

    @property
    def dist_coeffs_dict(self) -> Dict[str, np.array]:
        """
        Get the distortion coefficients by camera path
        :return: Dict[str, np.array]
        """
        return self._dist_coeffs_dict

    @property
    def extrinsics_dict(self) -> Dict[str, np.array]:
        """
        Get the extrinsic parameters by camera path
        :return: Dict[str, np.array]
        """
        return self._extrinsics_dict

    def set_extrinsics(self, path: str, extrinsics: np.array) -> None:
        """
        Set the extrinsic parameters by camera path
        :param: np.array
        """
        assert (path is None or extrinsics is None or extrinsics.shape == (3, 4))
        self._extrinsics_dict[path] = extrinsics

    @property
    def kmat_dict(self) -> Dict[str, np.array]:
        """
        Get the intrinsic parameters by camera path
        :return: Dict[str, np.array]
        """
        return self._kmat_dict

    def set_kmat(self, path: str, kmat: np.array) -> None:
        """
        Set the intrinsic parameters by camera path
        :param: np.array - K matrix
        """
        assert (path is None or kmat is None or kmat.shape == (3, 3))
        self._kmat_dict[path] = kmat

    def get_filter_params(self, filter_name: str) -> Optional[Dict[str, Any]]:
        """
        Get the parameters for the given filter
        :return: Optional[Dict[str, Any]] - A dict of the parameters,
        or None if the filter is not in the configuration
        """
        if filter_name not in self._filters:
            return None
        return self._filters[filter_name]

    def get_input_params(self, output_name: str) -> Optional[Dict[str, Any]]:
        """
        Get the parameters for the given output
        :return: Optional[Dict[str, Any]] - Return a dict of the parameters,
        or None  if the output is not in the configuration
        """
        if output_name not in self._inputs:
            return None
        return self._inputs[output_name]

    def get_output_params(self, output_name: str) -> Optional[Dict[str, Any]]:
        """
        Get the parameters for the given output
        :return: Optional[Dict[str, Any]] - Return a dict of the parameters,
        or None  if the output is not in the configuration
        """
        if output_name not in self._outputs:
            return None
        return self._outputs[output_name]

    def get_dimmap_params(self, dimmap_name: str) -> Optional[Dict[str, Any]]:
        """
        Get the parameters for the given dimmap
        :return: Optional[Dict[str, Any]] - Return a dict of the parameters,
        or None  if the dimmap is not in the configuration
        """
        if dimmap_name not in self._dimmaps:
            return None
        return self._dimmaps[dimmap_name]
