from livepose.keypoints.body_25 import Body25Keypoints as LivePoseKeypoint
from livepose.pose_backend import PoseBackend, register_pose_backend
from livepose.dataflow import Channel, Flow, Stream
import numpy as np
import cv2
import heapq
import logging
import os
import sys

from typing import Any, Dict, List, Optional, Tuple

from dataclasses import dataclass

from livepose.pose_backend import PosesForCamera, Pose, Keypoint
from livepose.node import ranged_type

logger = logging.getLogger(__name__)

MODEL_DIR = os.path.realpath(os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    "../models/posenet/"
))

DEFAULT_BASE_MODEL: str = "resnet50"

DEFAULT_OUTPUT_STRIDE = 16


def sigmoid(value: float) -> float:
    return 1.0 / (1.0 + np.exp(-value))


@register_pose_backend("posenet")
class PoseNetBackend(PoseBackend):
    """
    Python driver code for running inference on a tfjs PoseNet model,
    using the OpenCV DNN module. By default it will try to run
    on any CUDA device (if OpenCV has been compiled with CUDA support),
    otherwise it will run on the CPU.

    Backend parameters:
    - base_model: Base model used for inference. Mobilenet is faster but less
        accurate, resnet is more accurate but slower
    - device_id: integer, CUDA device ID in the case where CUDA is available.
        If set to -1, the default device will be used
    - draw_bounding_boxes: Whether or not to display bounding boxes overlayed on top of poses.
        Bounding boxes are computed for each pose using all detected keypoints, regardless
        of confidence score
    - draw_skeleton_edges: Whether or not to display edges between keypoints when showing
        results. If set to false, only keypoints are shown
    - input_resolution: Images will be scaled to this size before being passed to the model
        for inference. Larger values will give higher accurace but slower performance
    - min_keypoint_confidence: Minimum confidence score required for detected keypoints
        to be displayed. Note, keypoints with a score below this threshold will not be
        displayed on screen, but will still be included in other output formats such as
        OSC and websocket. Keypoints below the threshold will also not be used as roots
        in the multi pose detection algorithm (see multi pose detection code for more details)
    - min_pose_confidence: Minimum confidence score required for an entire Pose. Poses
        with a score below this threshold will not be displayed on screen AND will not
        be included in any other output formats. These poses will simply be discarded
        during the multi pose detection algorithm. Note, this value is ignored if
        multi_pose_detection is set to false
    - multi_pose_detection: Whether to use the single or multi pose detection algorithm.
        If you know only one person will be in the frame, the single pose algorithm is simpler
        and faster. When in doubt, use the multi pose algorithm if you do not know how many
        people will be in the frame
    - output_stride: Image scaling factor used by the PoseNet algorithm during inference. A
        higher stride (more compressed image) will give faster performance but lower accuracy,
        and vice versa. Note, the "mobilenet" base model only allows for strides 8 and 16;
        "resnet50" only allows 16 and 32.
    """

    LOCAL_MAXIMUM_RADIUS: int = 1

    PART_NAMES = [
        'nose', 'leftEye', 'rightEye', 'leftEar', 'rightEar', 'leftShoulder',
        'rightShoulder', 'leftElbow', 'rightElbow', 'leftWrist', 'rightWrist',
        'leftHip', 'rightHip', 'leftKnee', 'rightKnee', 'leftAnkle', 'rightAnkle'
    ]

    PARENT_CHILDREN_TUPLES: List[Tuple[int, int]] = [
        (0, 1), (1, 3), (0, 2), (2, 4), (0, 5), (5, 7), (7, 9), (5, 11),
        (11, 13), (13, 15), (0, 6), (6, 8), (8, 10), (6, 12), (12, 14), (14, 16)
    ]

    PARENT_TO_CHILD_EDGES: List[int] = [1, 3, 2, 4,
                                        5, 7, 9, 11, 13, 15, 6, 8, 10, 12, 14, 16]
    CHILD_TO_PARENT_EDGES: List[int] = [
        0, 1, 0, 2, 0, 5, 7, 5, 11, 13, 0, 6, 8, 6, 12, 14]

    # Edges only used for drawing the final skeleton for visualization.
    DISPLAY_TUPLES: List[Tuple[int, int]] = [
        (PART_NAMES.index('leftShoulder'), PART_NAMES.index('rightShoulder')),
        (PART_NAMES.index('rightShoulder'), PART_NAMES.index('rightElbow')),
        (PART_NAMES.index('rightElbow'), PART_NAMES.index('rightWrist')),
        (PART_NAMES.index('leftShoulder'), PART_NAMES.index('leftElbow')),
        (PART_NAMES.index('leftElbow'), PART_NAMES.index('leftWrist')),
        (PART_NAMES.index('leftShoulder'), PART_NAMES.index('leftHip')),
        (PART_NAMES.index('rightShoulder'), PART_NAMES.index('rightHip')),
        (PART_NAMES.index('leftHip'), PART_NAMES.index('rightHip')),
        (PART_NAMES.index('rightHip'), PART_NAMES.index('rightKnee')),
        (PART_NAMES.index('rightKnee'), PART_NAMES.index('rightAnkle')),
        (PART_NAMES.index('leftHip'), PART_NAMES.index('leftKnee')),
        (PART_NAMES.index('leftKnee'), PART_NAMES.index('leftAnkle'))
    ]

    # Mapping of indices of PoseNet keypoints to BODY_25 keypoints
    KEYPOINT_MAPPING: Dict[int, int] = {
        0: 0,    # NOSE
        1: 16,   # LEFT_EYE
        2: 15,   # RIGHT_EYE
        3: 18,   # LEFT_EAR
        4: 17,   # RIGHT_EAR
        5: 5,    # LEFT_SHOULDER
        6: 2,    # RIGHT_SHOULDER
        7: 6,    # LEFT_ELBOW
        8: 3,    # RIGHT_ELBOW
        9: 7,    # LEFT_WRIST
        10: 4,   # RIGHT_WRIST
        11: 12,  # LEFT_HIP
        12: 9,   # RIGHT_HIP
        13: 13,  # LEFT_KNEE
        14: 10,  # RIGHT_KNEE
        15: 14,  # LEFT_ANKLE
        16: 11   # RIGHT_ANKLE
    }

    # Number of keypoints used by PoseNet (constant for all posenet models).
    NUM_KEYPOINTS: int = 17

    # COLOR PALETTES
    # NOTE: All colors are in BGR format, to comply with opencv's standard.

    # Skeleton colors based on the detection order
    COLOR_PALETTE: List[Tuple[int, int, int]] = [
        (190, 38, 51),
        (224, 111, 139),
        (73, 60, 43),
        (164, 100, 34),
        (235, 137, 49),
        (247, 226, 107),
        (47, 72, 78),
        (68, 137, 26),
        (163, 206, 39),
        (27, 38, 50),
        (0, 87, 132),
        (49, 162, 242),
        (178, 220, 239),
        (255, 0, 255)
    ]

    # We use a relatively bright yellow color for the bounding boxes, so that
    # it stands out from the keypoint and skeleton colors.
    BOUNDING_BOX_COLOR: Tuple[int, int, int] = (13, 218, 255)

    @dataclass
    class Part:
        heatmap_x: int
        heatmap_y: int
        id: int

        def __lt__(self, other):
            """Implement '<' operator. This is needed when adding `Part` objects
            to the heap in `build_part_with_score_queue()`. The heap is sorted by
            detection score. If two parts have the same score, they are sorted by
            their Part ID. If the ID's are equal, they are sorted by their (x,y)
            coordinates.
            """
            # Use ID as main comparator.
            # If IDs are equal, fall back to x, y coordinates.
            if self.id == other.id:
                if self.heatmap_x == other.heatmap_x:
                    return self.heatmap_y < self.heatmap_y
                else:
                    return self.heatmap_x < other.heatmap_x
            else:
                return self.id < other.id

    @dataclass
    class Keypoint:
        score: float
        position: Tuple[float, float]
        part: str

    @dataclass
    class Pose:
        keypoints: List[Optional[Any]]
        score: float

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

        # Use the base model and output stride to infer the full model path.
        self._model_file_name: Optional[str] = None
        # (there is a separate model file for each base model and output stride combo)
        if self._args.base_model == "mobilenet":
            if self._args.output_stride == 8:
                self._model_file_name = "mobilenet_float_100_v1_stride8_frozen_model.pb"
            elif self._args.output_stride == 16:
                self._model_file_name = "mobilenet_float_100_v1_stride16_frozen_model.pb"
            else:
                logger.error((
                    f"Output stride '{self._args.output_stride}' not supported. "
                    "Must be one of [8, 16] for mobilenet models."
                ))
                sys.exit(1)
        elif self._args.base_model == "resnet50":
            if self._args.output_stride == 16:
                self._model_file_name = "resnet50_float_v1_stride16_frozen_model.pb"
            elif self._args.output_stride == 32:
                self._model_file_name = "resnet50_float_v1_stride32_frozen_model.pb"
            else:
                logger.error((
                    f"Output stride '{self._args.output_stride}' not supported. "
                    "Must be one of [16, 32] for resnet50 models."
                ))
                sys.exit(1)
        else:
            logger.error((
                f"Base model '{self._args.base_model}' not supported. "
                "Must be one of [mobilenet, resnet50]."
            ))
            sys.exit(1)

        # Full absolute model path.
        self._model_path: str = os.path.realpath(os.path.join(
            MODEL_DIR,
            self._model_file_name
        ))

        # File extension for the model
        # `.pb` == Tensorflow frozen model.
        # `.onnx` == Open Neural Network Exchange model.
        self._model_extension: str = os.path.splitext(
            self._model_path
        )[1]

        # Flag to specify which CUDA device ID to use in the case where CUDA is available
        # Defaults to -1 to use the default device.
        self._device_id: int = kwargs.get(
            "device_id", -1
        )

        # Flag to draw the edges that make up the whole skeleton, vs only
        # drawing the keypoints alone.
        # Defaults to True, drawing the whole skeleton.
        self._draw_skeleton_edges: bool = kwargs.get(
            "draw_skeleton_edges", True
        )

        # Flag to display bounding boxes around detected poses.
        # Defaults to False (no bounding boxes shown).
        self._draw_bounding_boxes: bool = kwargs.get(
            "draw_bounding_boxes", False
        )

        # Resolution of input image for the Posenet model.
        # All input images will be re-scaled to this size.
        # Defaults to 257x257
        self._input_resolution: Tuple[Any, ...] = tuple(kwargs.get(
            "input_resolution", (257, 257)
        ))

        # Confidence threshold for keypoints. Any individual keypoint with a
        # confidence score below the threshold will not be displayed. (they
        # may still be used for other purposes and filter outputs).
        self._min_keypoint_confidence: float = kwargs.get(
            "min_keypoint_confidence", 0.1
        )

        # Confidence threshold for a single detected person. Any detected
        # person instance with a whole confidence score below this threshold
        # will have NONE of their keypoints displayed. This overrides any
        # keypoints above the self._min_keypoint_confidence threshold.
        self._min_pose_confidence: float = kwargs.get(
            "min_pose_confidence", 0.15
        )

        # Flag whether to detect multiple people or just one person at most.
        # Defaults to True, detecting all people in image.
        self._multi_pose_detection: bool = kwargs.get(
            "multi_pose_detection", True
        )

    def add_parameters(self) -> None:
        super().add_parameters()

        # Base model used for training and running the full posenet model,
        # one of "mobilenet", "resnet50" (Defaults to mobilenet).
        self._parser.add_argument("--base-model", type=str, default=DEFAULT_BASE_MODEL,
                                  help="Base model used for training and running the full posenet model")

        # Output stride used by the neural net to scale down the image during
        # the feed forward steps. Output stride determines the dimensions of the
        # heatmaps output by the network.
        # Higher output stride --> faster but less accurate.
        self._parser.add_argument("--output-stride", type=int, default=DEFAULT_OUTPUT_STRIDE,
                                  help="Output stride used by the neural net to scale down the image during the feed forward steps")

        # Flag to specify which CUDA device ID to use in the case where CUDA is available
        # Defaults to -1 to use the default device.
        self._parser.add_argument("--device-id", type=int, default=-1,
                                  help="Flag to specify which CUDA device ID to use in the case where CUDA is available")

        # Flag to draw the edges that make up the whole skeleton, vs only
        # drawing the keypoints alone.
        # Defaults to True, drawing the whole skeleton.
        self._parser.add_argument("--draw-skeleton-edges", type=bool, default=True,
                                  help="Flag to draw the edges that make up the whole skeleton, vs only drawing the keypoints alone.")

        # Flag to display bounding boxes around detected poses.
        # Defaults to False (no bounding boxes shown).
        self._parser.add_argument("--draw-bounding-boxes", type=bool, default=False,
                                  help="Flag to display bounding boxes around detected poses.")

        # Resolution of input image for the Posenet model.
        # All input images will be re-scaled to this size.
        # Defaults to 257x257
        self._parser.add_argument("--input-resolution", type=int, nargs=2, default=(257, 257),
                                  help="Resolution of input image for the Posenet model.")

        # Confidence threshold for keypoints. Any individual keypoint with a
        # confidence score below the threshold will not be displayed. (they
        # may still be used for other purposes and filter outputs).
        self._parser.add_argument("--min-keypoint-confidence", type=ranged_type(float, 0.0, 1.0), default=0.1,
                                  help="Confidence threshold for keypoints.")

        # Confidence threshold for a single detected person. Any detected
        # person instance with a whole confidence score below this threshold
        # will have NONE of their keypoints displayed. This overrides any
        # keypoints above the self._args.min_keypoint_confidence threshold.
        self._parser.add_argument("--min-pose-confidence", type=ranged_type(float, 0.0, 1.0), default=0.15,
                                  help="Confidence threshold for a single detected person.")

        # Flag whether to detect multiple people or just one person at most.
        # Defaults to True, detecting all people in image.
        self._parser.add_argument("--multi-pose-detection", type=bool, default=True,
                                  help="Flag whether to detect multiple people or just one person at most.")

    def init(self) -> None:

        self._net = cv2.dnn.readNet(self._model_path)

        # cuda.getCudaEnabledDeviceCount() returns  0 if OpenCV is compiled without
        # CUDA support. If the CUDA driver is not installed, or is incompatible, this
        # function returns -1
        device_count = 0
        try:
            device_count = cv2.cuda.getCudaEnabledDeviceCount()
        except cv2.error as e:
            if e.code == -217:
                # -217: Gpu API call
                logger.error(f"Error {e}")
                if e.err.find("forward compatibility was attempted on non supported HW") != -1 or e.err.find("system has unsupported display driver / cuda driver combination") != -1:
                    logger.error(f"Please reboot your system.")
                sys.exit(1)

        if device_count > 0:
            if self._args.device_id >= 0:
                cv2.cuda.setDevice(self._args.device_id)
            self._net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
            self._net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)
        else:
            logger.warning(
                "Either CUDA drivers are not installed correctly or OpenCV "
                "is compiled without CUDA support. For best performance, "
                "enable CUDA."
            )

    def get_pose_dicts(
        self, poses_by_cam: List[np.array], score_threshold: float = 0.5
    ) -> List[PosesForCamera]:
        """For each camera, generate a dictionary of poses, containing the
        pose_id (if available) and a dictionary of detected keypoint
        coordinates, indexed by keypoint name.
        This is used as the generic keypoint output format across LivePose.

        NOTE: PoseNet ignores pose id for now, since the results are not stable
        enough to track pose IDs between frames. For all poses, `pose_id` is
        automatically set to `None`.
        output = [
            [
                {
                    "pose_id": pose_id,
                    "keypoints": {NOSE: (x, y, conf), .... },
                    "keypoints_definitions": KeypointClass,
                },
                {
                    "pose_id": pose_id,
                    "keypoints": { ... },
                    "keypoints_definitions": ...,
                }
            ],

            [   # One list per camera
                { ... },
                { ... }
            ]
        ]
        """
        output: List[PosesForCamera] = []

        for poses_for_camera in poses_by_cam:
            pose_id: int = 0
            poses: List[Pose] = []
            for detected_pose in poses_for_camera:
                keypoints: Dict[str, Keypoint] = {}
                confidence = 0.0
                for i, keypoint in enumerate(detected_pose.keypoints):
                    if keypoint is None:
                        continue

                    index = PoseNetBackend.KEYPOINT_MAPPING[i]

                    if keypoint.score >= score_threshold:
                        part_name = list(LivePoseKeypoint)[index]
                        confidence += keypoint.score
                        keypoints[part_name] = Keypoint(
                            confidence=keypoint.score,
                            part=part_name,
                            position=[keypoint.position[0], keypoint.position[1]]
                        )
                if len(keypoints) > 0:
                    confidence = confidence / len(keypoints)
                else:
                    confidence = 0

                poses.append(Pose(
                    confidence=confidence,
                    id=pose_id,
                    keypoints=keypoints,
                    keypoints_definitions=LivePoseKeypoint
                ))

                pose_id += 1

            output.append(PosesForCamera(poses=poses))

        return output

    def keypoints_from_posenet(
        self,
        poses_by_cam: List[List["PoseNetBackend.Pose"]],
        score_threshold: float = 0.5
    ) -> List[List[Dict[str, Any]]]:
        """
        Generate generic keypoint representation from PoseNet Results.
        """
        livepose_poses: List[List[Dict[str, np.array]]] = []

        for poses in poses_by_cam:
            livepose_poses_for_cam: List[Dict[str, np.array]] = []

            for pose in poses:
                livepose_pose: Dict[str, np.array] = {}
                for i, keypoint in enumerate(pose.keypoints):
                    if keypoint is None:
                        continue

                    body_25_index = PoseNetBackend.KEYPOINT_MAPPING[i]

                    # Ignore keypoints flagged as low probability / not detected.
                    if keypoint.score >= score_threshold:
                        livepose_pose[list(LivePoseKeypoint)[body_25_index]] = np.array(
                            [*keypoint.position, keypoint.score]
                        )

                livepose_poses_for_cam.append(livepose_pose)

            livepose_poses.append(livepose_poses_for_cam)

        return livepose_poses

    def decode_single_pose(self, heatmaps: np.ndarray, offsets: np.ndarray,
                           output_stride: int, threshold: float = 0.5
                           ) -> np.array:
        """
        Parse PoseNet output tensors to get coordinates of detected keypoints
        for at most one pose. Ignore all keypoints besides the most confident
        candidate for each part, forming the skeleton of one single person.

        Input:
        :param heatmaps: keypoint probability heatmaps for input image.
        3-dimension array with probability-like value of keypoint being
        present in each region of the image.
        :param offsets: offset vectors for an image, 3-dimension array. The
        heatmaps are only on a 3x3 grid, so the offsets are used to
        calculate the exact coordinates of the keypoints w.r.t.
        the input-image space.
        :param output_stride: output stride used by the model during the
        feed-forward step. This is needed to translate the heatmap coordinates
        output by the model to coordinates in the original image space.
        :param threshold: probability threshold for the keypoints. Scalar value.
        Keypoints with probability below this threshold will not be
        displayed in the final result.

        :return: array with coordinates of the keypoints and flags for those that
        have low probability / invalid coordinates.
        """

        num_keypoints = heatmaps.shape[-1]
        # Output keypoint array.
        keypoints: List[Optional[Any]] = []

        for i in range(heatmaps.shape[-1]):
            heatmap = heatmaps[..., i]

            # Max detected probability for keypoint i.
            max_prob = np.max(heatmap)

            # Find heatmap position corresponding to max detection probability.
            max_val_pos = np.squeeze(np.argwhere(heatmap == max_prob))

            # Re-map coordinates from heatmap space to the original image space.
            keypoint_x, keypoint_y = self.get_image_coords(
                max_val_pos[0],
                max_val_pos[1],
                i,
                output_stride,
                offsets
            )

            # Ensure the point lies within the image frame.
            if (keypoint_x >= self._args.input_resolution[0]
                    or keypoint_y >= self._args.input_resolution[1]):
                continue

            keypoint = PoseNetBackend.Keypoint(
                score=max_prob,
                position=(keypoint_x, keypoint_y),
                part=self.PART_NAMES[i]
            )

            keypoints.append(keypoint)

        # Combine keypoints into Pose object.
        pose = PoseNetBackend.Pose(keypoints=keypoints, score=1)

        return pose

    def score_is_maximum_in_local_window(
        self, heatmap_x: int, heatmap_y: int, keypoint_id: int,
        local_maximum_radius: int, scores: np.ndarray
    ) -> bool:
        """
        Check whether the score for the given keypoint is maximum in a given window

        :param keypoint_id: Keypoint ID to consider
        :param heatmap_x: x position of the keypoint in heatmap space
        :param heatmap_y: y position of the keypoint in heatmap space
        :param local_maximum_radius: window size
        :param scores: heatmap scores
        """
        height, width = scores.shape[0:2]
        is_local_maximum = True

        # Score for the given keypoint, which we are comparing against.
        score = scores[heatmap_x, heatmap_y, keypoint_id]

        y_start = max(heatmap_y - local_maximum_radius, 0)
        y_end = min(heatmap_y + local_maximum_radius + 1, height)

        for y in range(y_start, y_end):
            x_start = max(heatmap_x - local_maximum_radius, 0)
            x_end = min(heatmap_x + local_maximum_radius + 1, width)

            for x in range(x_start, x_end):
                if heatmap_y == y and heatmap_x == x:
                    continue
                if scores[x, y, keypoint_id] > score:
                    is_local_maximum = False
                    break

            if not is_local_maximum:
                break

        return is_local_maximum

    def build_part_with_score_queue(
            self, score_threshold: float, local_maximum_radius: int,
            scores: np.ndarray
    ) -> List[Tuple[float, "PoseNetBackend.Part"]]:
        """
        Build a priority queue with part candidate positions. For this we find all
        local maxima in the score maps with score values above a threshold. We create
        a single priority queue across all parts

        :param score_threshold: only include parts that have a threshold greater than this
        :param local_maximum_radius: only consider parts which are maximum in this radius
        :param scores: heatmap scores returned by the posenet network

        :return: A priority queue with part candidate positions and their score
        """
        height, width, num_keypoints = scores.shape
        queue: List[Tuple[float, PoseNetBackend.Part]] = []

        for heatmap_y in range(height):
            for heatmap_x in range(width):
                for keypoint_id in range(num_keypoints):
                    score = scores[heatmap_x, heatmap_y, keypoint_id]

                    if score < score_threshold:
                        continue

                    if self.score_is_maximum_in_local_window(
                            heatmap_x,
                            heatmap_y,
                            keypoint_id,
                            local_maximum_radius,
                            scores
                    ):
                        part = PoseNetBackend.Part(
                            heatmap_x=heatmap_x,
                            heatmap_y=heatmap_y,
                            id=keypoint_id
                        )

                        heapq.heappush(
                            queue,
                            (score, part)
                        )

        return [heapq.heappop(queue) for i in range(len(queue))]

    def get_offset_point(self, heatmap_x: int, heatmap_y: int,
                         keypoint_id: int, offsets: np.ndarray
                         ) -> Tuple[float, float]:
        """
        Get the offset at the given heatmap coordinates, for the given keypoint
        id. The offsets determine the exact position in the original image
        coordinates, as an 'offset' from the scaled down heatmap locations.

        :param heatmap_x: x coordinate in heatmap space
        :param heatmap_y: y coordinate in heatmap space
        :param keypoint_id: keypoint ID
        :param offsets: Offset array for the entire input

        :return: The offset for the given keypoint and location
        """
        # TODO: This should be a constant and a property of the PoseNet class.
        num_keypoints = int(offsets.shape[2] / 2)

        # The last dimension of the offset array has length 2 x NUM_KEYPOINTS.
        # All of the x offsets are listed, followed by all of the y offsets
        # for each keypoint_id at the given location.
        offset_x = offsets[heatmap_x, heatmap_y, keypoint_id]
        offset_y = offsets[heatmap_x, heatmap_y, keypoint_id + num_keypoints]

        return (offset_x, offset_y)

    def get_image_coords(self, heatmap_x: int, heatmap_y: int,
                         keypoint_id: int, output_stride: int,
                         offsets: np.ndarray
                         ) -> Tuple[float, float]:
        """
        Get the coordinates in the original image space for a given heatmap
        location, using offset vectors.

        :param part: Part for which to compute coordinates
        :param output_stride: output stride which was used when feed-forwarding
        through the PoseNet model
        :param offsets: offset vectors for the image

        :return: The 2D coordinates in the image
        """
        offset = self.get_offset_point(
            heatmap_x,
            heatmap_y,
            keypoint_id,
            offsets
        )

        x = heatmap_x * output_stride + offset[0]
        y = heatmap_y * output_stride + offset[1]

        return (x, y)

    def within_nms_radius_of_corresponding_point(
        self, poses: List["PoseNetBackend.Pose"], squared_nms_radius: float,
        coords: Tuple[float, float], keypoint_id: int
    ) -> bool:
        """
        Check whether the given coordinates is within the radius of any points already
        in the pose list

        :param poses: Pose list to compare the coordinates to
        :param squared_nms_radius: Squared non-maximum suppression distance
        :param coords: Point coordinates
        :param keypoint_id: Keypoint ID

        :return: True if the point is within the radius, False otherwise
        """
        for pose in poses:
            keypoint = pose.keypoints[keypoint_id]
            if keypoint is None:
                continue
            keypoint_pos = keypoint.position

            distance = np.linalg.norm(
                np.array(keypoint_pos) - np.array(coords)
            )

            distance_squared = distance ** 2

            if distance_squared <= squared_nms_radius:
                return True

        return False

    def get_stride_index_near_point(
            self, point: Tuple[float, float], output_stride: int, height: int,
            width: int
    ) -> Tuple[int, int]:
        """
        Return the stride index for the given point

        :param point: Point to get stride index of
        :param output_stride: output stride which was used when feed-forwarding
        through the PoseNet model
        :param height: heatmap height
        :param width: heatmap width

        :return: The stride index
        """
        y = max(0, min(round(point[0] / output_stride), height - 1))
        x = max(0, min(round(point[1] / output_stride), width - 1))
        return (int(y), int(x))

    def get_displacement(
            self, edge_id: int, point: Tuple[int, int],
            displacements: np.ndarray
    ) -> Tuple[float, float]:
        """
        Get the displacement at the given point

        :param edge_id: Edge part ID to consider
        :param point: Point coordinates in the displacement field
        :param displacements: Displacements

        :return: Displacements values along both axes
        """
        num_edges = int(displacements.shape[2] / 2)

        displacement = (
            displacements[point[0], point[1], edge_id],
            displacements[point[0], point[1], edge_id + num_edges]
        )

        return displacement

    def traverse_to_target_keypoint(
        self, edge_id: int, source_keypoint: "PoseNetBackend.Keypoint", target_keypoint_id: int,
        scores: np.ndarray, offsets: np.ndarray, output_stride: int,
        displacements: np.ndarray, offset_refine_step: int = 2
    ) -> "PoseNetBackend.Keypoint":
        """
        We get a new keypoint along the edge_id for the pose instance, assuming
        that the position of the source_keypoint part is already known. For this, we
        follow the displacement vector from the source to target part (stored in
        the i-th channel of the displacement tensor). The displaced keypoint
        vector is refined using the offset vector by `offsetRefineStep` times.
        :param edge_id: Edge part ID to follow
        :param source_keypoint: Source keypoint
        :param target_keypoint_id: Target keypoint ID
        :param scores: keypoint probability heatmaps for input image.
        3-dimension array with probability-like value of keypoint being
        present in each region of the image.
        :param offsets: offset vectors for an image, 3-dimension array. The
        heatmaps are only on a 3x3 grid, so the offsets are used to
        calculate the exact coordinates of the keypoints w.r.t.
        the input-image space.
        :param output_stride: output stride which was used when feed-forwarding
        through the PoseNet model
        :param displacements: displacement between consecutive parts from
        the root towards the leaves
        :param offset_refine_step: Refine steps for the offset computation

        :return: The target keypoint
        """
        height, width = scores.shape[0:2]

        source_keypoint_indices = self.get_stride_index_near_point(
            source_keypoint.position,
            output_stride,
            height,
            width
        )

        displacement = self.get_displacement(
            edge_id,
            source_keypoint_indices,
            displacements
        )

        target_keypoint = (
            source_keypoint.position[0] + displacement[0],
            source_keypoint.position[1] + displacement[1]
        )

        for i in range(offset_refine_step):
            target_keypoint_indices = self.get_stride_index_near_point(
                target_keypoint,
                output_stride,
                height,
                width
            )

            offset_point = self.get_offset_point(
                target_keypoint_indices[0],
                target_keypoint_indices[1],
                target_keypoint_id,
                offsets
            )

            target_keypoint = (
                target_keypoint_indices[0] * output_stride + offset_point[0],
                target_keypoint_indices[1] * output_stride + offset_point[1]
            )

        target_keypoint_indices = self.get_stride_index_near_point(
            target_keypoint,
            output_stride,
            height,
            width
        )

        score = scores[
            target_keypoint_indices[0],
            target_keypoint_indices[1],
            target_keypoint_id
        ]

        keypoint = PoseNetBackend.Keypoint(
            position=target_keypoint,
            part=PoseNetBackend.PART_NAMES[target_keypoint_id],
            score=score
        )

        return keypoint

    def decode_pose(
        self, root: Tuple[float, "PoseNetBackend.Part"], scores: np.ndarray, offsets: np.ndarray,
        output_stride: int, fwd: np.ndarray, bwd: np.ndarray
    ) -> List[Optional[Any]]:
        """
        Follows the displacement fields to decode the full pose of the object
        instance given the position of a part that acts as root

        :param root: Root part to start from
        :param scores: keypoint probability heatmaps for input image.
        3-dimension array with probability-like value of keypoint being
        present in each region of the image.
        :param offsets: offset vectors for an image, 3-dimension array. The
        heatmaps are only on a 3x3 grid, so the offsets are used to
        calculate the exact coordinates of the keypoints w.r.t.
        the input-image space.
        :param output_stride: output stride which was used when feed-forwarding
        through the PoseNet model
        :param fwd: forward displacement between consecutive parts from
        the root towards the leaves
        :param bwd: backward displacement between consecutive parts from
        the root towards the leaves

        :return: The pose of the object
        """
        num_parts = scores.shape[2]
        num_edges = len(PoseNetBackend.PARENT_TO_CHILD_EDGES)

        instance_keypoints: List[Optional[Any]] = [
            None for i in range(num_parts)]

        # Start a new detection instance at the position of the root
        root_score = root[0]
        root_part = root[1]
        root_point = self.get_image_coords(
            root_part.heatmap_x,
            root_part.heatmap_y,
            root_part.id,
            output_stride,
            offsets
        )

        instance_keypoints[root_part.id] = PoseNetBackend.Keypoint(
            score=root_score, position=root_point, part=self.PART_NAMES[root_part.id])

        # Decode the part positions upwards in the tree, following the backward displacements
        for edge in reversed(range(num_edges)):
            source_keypoint_id = PoseNetBackend.PARENT_TO_CHILD_EDGES[edge]
            target_keypoint_id = PoseNetBackend.CHILD_TO_PARENT_EDGES[edge]

            source_keypoint = instance_keypoints[source_keypoint_id]
            if source_keypoint is not None and instance_keypoints[target_keypoint_id] is None:
                instance_keypoints[target_keypoint_id] = self.traverse_to_target_keypoint(
                    edge_id=edge,
                    source_keypoint=source_keypoint,
                    target_keypoint_id=target_keypoint_id,
                    scores=scores,
                    offsets=offsets,
                    output_stride=output_stride,
                    displacements=bwd
                )

        # Decode the part positions upwards in the tree, following the forward displacements
        for edge in range(num_edges):
            source_keypoint_id = PoseNetBackend.CHILD_TO_PARENT_EDGES[edge]
            target_keypoint_id = PoseNetBackend.PARENT_TO_CHILD_EDGES[edge]

            source_keypoint = instance_keypoints[source_keypoint_id]
            if source_keypoint is not None and instance_keypoints[target_keypoint_id] is None:
                instance_keypoints[target_keypoint_id] = self.traverse_to_target_keypoint(
                    edge_id=edge,
                    source_keypoint=source_keypoint,
                    target_keypoint_id=target_keypoint_id,
                    scores=scores,
                    offsets=offsets,
                    output_stride=output_stride,
                    displacements=fwd
                )

        return instance_keypoints

    def get_instance_score(
        self, existing_poses: List["PoseNetBackend.Pose"], squared_nms_radius: float, instance_keypoints: List[Optional[Any]]
    ) -> float:
        """
        Score the newly proposed object instance without taking into account
        the scores of the parts that overlap with any previously detected instance

        :param existing_poses: Previously detected instances
        :param squared_nms_radius: Squared non-maximum suppression distance
        :param instance_keypoints: Newly proposed keypoints

        :return: The score
        """
        not_overlapped_keypoint_scores: float = 0.0
        for keypoint_id, keypoint in enumerate(instance_keypoints):
            if keypoint is None:
                continue
            if not self.within_nms_radius_of_corresponding_point(
                existing_poses,
                squared_nms_radius,
                keypoint.position,
                keypoint_id
            ):
                not_overlapped_keypoint_scores += keypoint.score

        return not_overlapped_keypoint_scores / len(instance_keypoints)

    def decode_multiple_poses(
        self, heatmaps: np.ndarray, offsets: np.ndarray, fwd: np.ndarray,
        bwd: np.ndarray, output_stride: int = 8, max_pose_detection: int = 8,
        nms_radius: int = 20
    ) -> List["PoseNetBackend.Pose"]:
        """
        Parse PoseNet output tensors to get coordinates of detected persons.

        :param heatmaps: keypoint probability heatmaps for input image.
        3-dimension array with probability-like value of keypoint being
        present in each region of the image.
        :param offsets: offset vectors for an image, 3-dimension array. The
        heatmaps are only on a 3x3 grid, so the offsets are used to
        calculate the exact coordinates of the keypoints w.r.t.
        the input-image space.
        :param fwd: forward displacement between consecutive parts from
        the root towards the leaves
        :param bwd: backward displacement between consecutive parts from
        the root towards the leaves
        :param output_stride: output stride which was used when feed-forwarding
        through the PoseNet model
        :param max_pose_detection: maximum numbber of instance detections
        :param nms_radius: Non-maximum suppression part distance. It needs to be
        strictly positive. Two parts suppress each other ifi they aree less
        than nms_radius pixels away.

        :return: An array of poses and their scores, each containing keypoints and
        the corresponding keypoint scores
        """
        poses: List[PoseNetBackend.Pose] = []

        # NOTE: Only return instance detections that have a root keypoint score
        # greater than or equal to self._args.min_keypoint_confidence
        queue = self.build_part_with_score_queue(
            score_threshold=self._args.min_keypoint_confidence,
            local_maximum_radius=PoseNetBackend.LOCAL_MAXIMUM_RADIUS,
            scores=heatmaps
        )

        squared_nms_radius = nms_radius * nms_radius

        while len(poses) < max_pose_detection and len(queue) != 0:
            root: Tuple[float, PoseNetBackend.Part] = queue.pop()

            root_image_coords: Tuple[float, float] = self.get_image_coords(
                root[1].heatmap_x,
                root[1].heatmap_y,
                root[1].id,
                output_stride,
                offsets
            )

            if self.within_nms_radius_of_corresponding_point(
                    poses, squared_nms_radius, root_image_coords, root[1].id
            ):
                continue

            keypoints = self.decode_pose(
                root=root,
                scores=heatmaps,
                offsets=offsets,
                output_stride=output_stride,
                fwd=fwd,
                bwd=bwd
            )

            score = self.get_instance_score(
                poses, squared_nms_radius, keypoints)

            # Skip poses with too low of an overall score.
            if score < self._args.min_pose_confidence:
                continue

            poses.append(PoseNetBackend.Pose(keypoints=keypoints, score=score))

        return poses

    def draw_keypoints(self, image: np.ndarray, poses: List["PoseNetBackend.Pose"]):
        """
        Draw the keypoints as colored circles onto the original input image.
        Note: this method draws only the keypoint joints, not the edges
        connecting them to form the full skeleton.
        Inputs:

        :param image: the original input image as a numpy array

        :param poses: List of detected poses. Each Pose object represents the
        skeleton for one detected person, as a list of keypoints and a
        confidence score for the whole skeleton.

        :return: image with keypoints drawn on
        """
        for index, pose in enumerate(poses):
            keypoints = [k for k in pose.keypoints if k is not None]

            for keypoint in keypoints:
                # Exclude keypoints with low confidence.
                if keypoint.score < self._args.min_keypoint_confidence:
                    continue

                image = cv2.circle(
                    image,
                    (int(keypoint.position[0]), int(keypoint.position[1])),
                    radius=4,
                    color=(255, 255, 0),
                    # Fill in circle.
                    thickness=-1
                )

        return image

    def draw_skeletons(self, image: np.ndarray, poses: List["PoseNetBackend.Pose"]):
        """
        Draw the full detected skeletons as edges onto the original input image.
        Inputs:
        :param image: the original input image as a numpy array
        :param poses: List of detected poses. Each Pose object represents the
        skeleton for one detected person, as a list of keypoints and a
        confidence score for the whole skeleton.
        :return: image with keypoints drawn on
        """
        for index, pose in enumerate(poses):
            keypoints = [k for k in pose.keypoints if k is not None]

            num_keypoints = len(keypoints)

            # Only use display edges for drawing the skeleton.
            for line_points in PoseNetBackend.DISPLAY_TUPLES:
                if line_points[0] >= num_keypoints or line_points[1] >= num_keypoints:
                    continue

                source_keypoint = keypoints[line_points[0]]
                target_keypoint = keypoints[line_points[1]]

                if source_keypoint is None or target_keypoint is None:
                    continue

                # If either keypoint is below confidence threshold, do not draw
                # the edge.
                if (source_keypoint.score < self._args.min_keypoint_confidence
                        or target_keypoint.score < self._args.min_keypoint_confidence):
                    continue

                image = cv2.line(
                    image,
                    (int(source_keypoint.position[0]), int(
                        source_keypoint.position[1])),
                    (int(target_keypoint.position[0]), int(
                        target_keypoint.position[1])),
                    color=PoseNetBackend.COLOR_PALETTE[index % len(
                        PoseNetBackend.COLOR_PALETTE)],
                    thickness=2
                )

        return image

    def draw_bounding_boxes(self, image: np.ndarray,
                            bounding_boxes: np.ndarray) -> np.ndarray:
        """
        Draw bounding boxes as coloured lines onto original input image.

        :param image: the original input image as a numpy array
        :param bounding_boxes: numpy array of bounding boxes. Has shape (x,5),
        where x is the number of bounding boxes

        :return np.ndarray: image with bounding boxes overlayed.
        """
        for index, bbox in enumerate(bounding_boxes):
            x1, y1, x2, y2 = [int(val) for val in bbox[0:4]]

            # pairs of coordinates forming edges of bounding box.
            edges = [
                # LEFT
                [(x1, y1), (x1, y2)],
                # RIGHT
                [(x2, y1), (x2, y2)],
                # TOP
                [(x1, y1), (x2, y1)],
                # BOTTOM
                [(x1, y2), (x2, y2)]
            ]

            for p1, p2 in edges:
                image = cv2.line(
                    image,
                    p1,
                    p2,
                    color=self.BOUNDING_BOX_COLOR,
                    thickness=2
                )

        return image

    def step_pose_detection(self, flow: Flow, now: float, dt: float) -> bool:
        """
        Process the given image(s) with the DL Backend model
        :param flow: Flow - Data flow to read from and write to
        :param now: float - current time
        :param dt: float - time since last call
        :return: bool - success
        """
        input_streams = flow.get_streams_by_type(Stream.Type.INPUT)

        if len(input_streams) == 0:
            return False

        poses_by_cam: List[List[PoseNetBackend.Pose]] = []
        tracked_images = []

        for stream in input_streams.values():
            if Channel.Type.COLOR not in stream.channel_types:
                return False

            image: np.array = stream.get_channels_by_type(Channel.Type.COLOR)[
                0].data

            resized_image = cv2.resize(image, self._args.input_resolution)

            # For mobilenet, the input pixel values must be in [-1, 1] range.
            if self._args.base_model == "mobilenet":
                resized_image = (np.float32(resized_image) - 127.5) / 127.5

            # Set input.
            net_input = cv2.dnn.blobFromImage(resized_image)
            self._net.setInput(net_input)

            # Inference and retrieve outputs
            # Note that we need to invert dispFwd and dispBwd compared to the
            # original implementation, due to some inversion that cv2.dnn
            # seems to make regarding outputs
            output_names = self._net.getUnconnectedOutLayersNames()[0:4]

            outputs = self._net.forward(output_names)
            # Remove batch dimensions.
            outputs = [x.squeeze() for x in outputs]
            # Dimensions are also mixed up with OpenCV DNN compared to TFlite.
            outputs = [np.transpose(x, (1, 2, 0)) for x in outputs]

            # The order of the output arrays is different for different models.
            # Use heatmap dimensions to determine output order. There is a single
            # 2d heatmap for each keypoint, and so only heatmaps has the 3rd
            # dimension exactly equal to the number of keypoints.
            if outputs[0].shape[2] == self.NUM_KEYPOINTS:
                heatmaps, offsets, dispBwd, dispFwd = outputs
            elif outputs[2].shape[2] == self.NUM_KEYPOINTS:
                dispBwd, dispFwd, heatmaps, offsets = outputs
            else:
                logger.error((
                    f"Deep learning model '{self._model_file_name}' not "
                    "supported: incorrect output format."
                ))
                sys.exit(1)

            # Apply sigmoid function on the heatmap
            heatmaps = sigmoid(heatmaps)

            # Single vs Multi Pose Detection.
            if not self._args.multi_pose_detection:
                poses = [self.decode_single_pose(
                    heatmaps=heatmaps,
                    offsets=offsets,
                    output_stride=self._args.output_stride
                )]
            else:
                # Coordinates of detected keypoints in the input image.
                poses = self.decode_multiple_poses(
                    heatmaps=heatmaps,
                    offsets=offsets,
                    fwd=dispFwd,
                    bwd=dispBwd,
                    output_stride=self._args.output_stride,
                    nms_radius=40
                )

            # Convert keypoints back to the coordinates of the orignal image
            for pose in poses:
                for keypoint in pose.keypoints:
                    if keypoint is None:
                        continue

                    image_x = (
                        keypoint.position[1] / self._args.input_resolution[0]
                        * image.shape[1]
                    )
                    image_y = (
                        keypoint.position[0] / self._args.input_resolution[1]
                        * image.shape[0]
                    )
                    keypoint.position = (
                        image_x,
                        image_y
                    )

            poses_by_cam.append(poses)
            self._poses_for_cameras = self.get_pose_dicts(poses_by_cam)

            # Draw keypoints and edges.
            keypoint_overlay = self.draw_keypoints(image, poses).squeeze()

            # Draw edges of the skeleton only if specified.
            if self._args.draw_skeleton_edges:
                keypoint_overlay = self.draw_skeletons(
                    keypoint_overlay,
                    poses
                ).squeeze()

            tracked_images.append(keypoint_overlay)

        self._tracked_images = tracked_images

        return True

    def get_output(self) -> List[Channel]:
        channels: List[Channel] = [
            Channel(
                type=Channel.Type.POSE_2D,
                name="keypoints",
                data=self._poses_for_cameras,
                metadata={}
            )
        ]

        if self._tracked_images:
            for index, tracked_image in enumerate(self._tracked_images):
                channels.append(Channel(
                    type=Channel.Type.COLOR,
                    name=f"pose_image_{index}",
                    data=tracked_image,
                    metadata={
                        "resolution": [tracked_image.shape[1], tracked_image.shape[0]]
                    }
                ))

        if self._bounding_boxes is not None:
            channels.append(Channel(
                type=Channel.Type.BB_2D,
                name="bbox",
                data=self._bounding_boxes,
                metadata={}
            ))

        return channels

    def is_gpu_acceleration_active(self) -> bool:
        """Check if GPU acceleration is active."""
        cv2_cuda_enabled_device_count = 0
        try:
            cv2_cuda_enabled_device_count = cv2.cuda.getCudaEnabledDeviceCount()
        except cv2.error as e:
            logger.error(f"{e.msg}")
            if e.msg.find("forward compatibility was attempted on non supported HW") > -1:
                logger.error(
                    f"This error may be due to a recent upgrade of Nvidia drivers, if so you may need to reboot your computer.")
            return False
        if cv2_cuda_enabled_device_count < 1:
            logger.warning(f"opencv: did not detect any CUDA-enabled GPU")
            return False
        else:
            logger.info(f"opencv: CUDA-enabled GPU detected")
        if len(cv2.dnn.getAvailableTargets(cv2.dnn.DNN_BACKEND_CUDA)) == 0:
            logger.warning(
                f"opencv DNN module: not compiled with CUDA backend")
            return False
        else:
            logger.info(f"opencv DNN module: compiled with CUDA backend")
        return True

    def is_gpu_acceleration_needed(self) -> bool:
        """Check if GPU acceleration is needed."""
        return False
