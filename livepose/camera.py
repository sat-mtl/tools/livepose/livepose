import cv2
import logging
import numpy as np
import argparse

from abc import ABCMeta, abstractmethod
from typing import Any, Callable, Dict, List, Optional, Tuple, Type

from livepose.dataflow import Channel
from livepose.node import Node

logger = logging.getLogger(__name__)


def register_camera(name: str) -> Callable:
    """
    Decorator for registering cameras
    :param name: name - Name of the camera
    """

    def deco(cls: Type['Camera']) -> Callable:
        Camera.register(camera_type=cls, name=name)
        return cls

    return deco


class Camera(Node, metaclass=ABCMeta):
    """
    Generic class embedding a camera, its parameters and buffers
    """

    registered_cameras: Dict[str, Type['Camera']] = dict()

    def __init__(self, **kwargs: Any) -> None:
        """Init Camera class."""
        super().__init__(**kwargs)

    @abstractmethod
    def add_parameters(self) -> None:
        """Add Camera parameters."""
        self._parser.add_argument("--path", type=str, default="", help="Camera path")
        # self._parser.add_argument("--api", type=str, default="", help="Camera api")

        self._parser.add_argument("--flip", type=bool, default=False, help="Flip stream")
        self._parser.add_argument("--rotate", type=int, default=0, choices=[0, 90, 180, 270], help="Rotate stream")

        self._frame: Optional[np.array] = None

        self._dist_coeffs: Optional[np.array] = None
        self._kmat: Optional[np.array] = None
        self._extrinsics: Optional[np.array] = None

    @abstractmethod
    def init(self) -> None:
        """Init Camera devices and objects."""
        pass

    @classmethod
    def register(cls, camera_type: Type['Camera'], name: str) -> None:
        if name not in cls.registered_cameras:
            cls.registered_cameras[name] = camera_type
        else:
            raise Exception(
                f"A camera type {name} has already been registered")

    @classmethod
    def create_from_name(cls, name: str, **kwargs: Any) -> Optional['Camera']:
        if name not in Camera.registered_cameras:
            return None
        else:
            return Camera.registered_cameras[name](**kwargs)

    @property
    def path(self) -> str:
        """
        Get the path of the video file or the capturing
        device
        :return: str - Path to the file or device
        """
        return self._args.path

    @property
    def api(self) -> str:
        """
        Get the API of the video file or the capturing
        device
        :return: str - API to the file or device
        """
        return self._args.api

    @property
    def frame(self) -> Optional[np.array]:
        """
        Get the current frame held by the camera
        :return: np.array - Current decoded frame
        """
        return self._frame

    @frame.setter
    def frame(self, frame: np.array) -> None:
        """
        Set frame
        :param: np.array - WxH matrix
        """
        self._frame = frame

    @property
    def depth_frame(self) -> Optional[Any]:
        """
        Get the current depth frame held by the camera
        :return: rs.depth_frame - Current depth frame
        """
        pass

    @property
    def kmat(self) -> Optional[np.array]:
        """
        Get the intrinsics parameters of the device
        :return: np.array - K matrix
        """
        return self._kmat

    @kmat.setter
    def kmat(self, kmat: np.array) -> None:
        """
        Set the K matrix of the device. It is the camera matrix
        representing the intrinsic parameters of the camera.
        It has 9 elements
        :param: np.array - K matrix
        """
        assert(kmat is None or kmat.shape == (3, 3))
        self._kmat = kmat

    @property
    def dist_coeffs(self) -> Optional[np.array]:
        """
        Get the distortion parameters of the device
        :return: np.array - K matrix
        """
        return self._dist_coeffs

    @dist_coeffs.setter
    def dist_coeffs(self, dist_coeffs: np.array) -> None:
        """
        Set the distortion parameters of the device
        :param: np.array - distortion parameters
        """
        assert(dist_coeffs is None or dist_coeffs.shape == (4, 1)
               or dist_coeffs.shape == (5, 1))
        self._dist_coeffs = dist_coeffs

    @property
    def extrinsics(self) -> Optional[np.array]:
        """
        Get the extrinsics parameters of the device
        :return: np.array - extrinsics parameters
        """
        return self._extrinsics

    @extrinsics.setter
    def extrinsics(self, extrinsics: np.array) -> None:
        """
        Set the extrinsics parameters of the devices, the R|t matrix.
        It is the rotation matrix concatenated with the translation vector
        :param: np.array - extrinsics parameters
        """
        assert(extrinsics is None or extrinsics.shape == (3, 4))
        self._extrinsics = extrinsics

    @property  # type: ignore
    @abstractmethod
    def fps(self) -> float:
        """
        Get the grab framerate for the camera
        :return: float - Framerate as frames per second
        """
        pass

    @fps.setter  # type: ignore
    @abstractmethod
    def fps(self, framerate: float) -> None:
        """
        Set the grab framerate, in frames per seconde
        :param: float - Framerate
        """
        pass

    @property  # type: ignore
    @abstractmethod
    def resolution(self) -> Tuple[int, int]:
        """
        Get the resolution of the device
        :return: Tuple[int, int]
        """
        pass

    @resolution.setter  # type: ignore
    @abstractmethod
    def resolution(self, res: Tuple[int, int]) -> None:
        """
        Set the resolution of the device
        :param: Tuple[int, int]
        """
        pass

    @abstractmethod
    def is_open(self) -> bool:
        """
        Check if a video capturing device has been initialized already
        :return: bool
        """
        pass

    @staticmethod
    def grab_all(camera: 'Camera') -> None:
        camera.grab()  # type: ignore

    @abstractmethod
    def grab(self) -> bool:
        """
        Grabs the next frame from video file or capturing device
        :return: bool - in the case of success
        """
        pass

    @abstractmethod
    def retrieve(self) -> bool:
        """
        Decodes and set the grabbed video frame
        grab() should be called beforehand
        :return: bool - true in the case of success
        """
        pass

    def get_channels(self) -> List[Channel]:
        """
        Get the channels grabbed for this camera
        :return: List[Channel] - List of channels
        """
        raw_channels = self.get_raw_channels()

        for channel in raw_channels:
            if channel.type not in [Channel.Type.COLOR, Channel.Type.DEPTH]:
                continue

            if self._args.flip:
                channel.data = cv2.flip(channel.data, 1)

            angle = self._args.rotate
            if angle == 90:
                channel.data = cv2.rotate(
                    channel.data, cv2.ROTATE_90_CLOCKWISE)
            elif angle == 180:
                channel.data = cv2.rotate(channel.data, cv2.ROTATE_180)
            elif angle == 270:
                channel.data = cv2.rotate(
                    channel.data, cv2.ROTATE_90_COUNTERCLOCKWISE)

            channel.metadata["flip"] = self._args.flip
            channel.metadata["rotate"] = self._args.rotate
            channel.metadata["resolution"] = self.resolution

        return raw_channels

    @abstractmethod
    def get_raw_channels(self) -> List[Channel]:
        """
        Get the raw channels from the camera, without any
        transformation (flip, rotate, etc) applied
        :return: List[Channel] - List of raw channels
        """
        pass

    @abstractmethod
    def open(self) -> bool:
        """
        Open video file or device
        :return: bool - true if successful
        """
        pass

    @abstractmethod
    def close(self) -> None:
        """
        Closes video file or capturing device
        """
        pass

    @property  # type: ignore
    def flip(self) -> bool:
        """
        Get the status of flipping (vertical)
        :return: bool - flippping
        """
        return self._args.flip

    @flip.setter  # type: ignore
    def flip(self, flip: bool) -> None:
        """
        Set camera flipping (vertical)
        :param: bool - flipping
        """
        self._args.flip = flip

    @property  # type: ignore
    def rotate(self) -> int:
        """
        Get the status of rotation (clockwise)
        :return: int - rotation
        """
        pass

    @rotate.setter  # type: ignore
    def rotate(self, rotate: int) -> None:
        """
        Set camera rotation (clockwise)
        :param: int - rotation
        """
        pass
