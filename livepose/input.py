from abc import abstractmethod
from copy import deepcopy

from typing import Any, Callable, Dict, List, Optional, Type

from livepose.dataflow import Channel, Flow, Stream
from livepose.node import Node


def register_input(name: str) -> Callable:
    """
    Decorator  for registering inputs
    :param name: name - Name of the input
    """

    def deco(cls: Type['Input']) -> Callable:
        Input.register(input_type=cls, name=name)
        return cls

    return deco


class Input(Node):
    """
    Base class for inputs
    An input gets to receive data from the outside
    """

    registered_inputs: Dict[str, Type['Input']] = dict()

    def __init__(self, input_name: str, *args: Any, **kwargs: Any):
        """Init Input class."""
        super().__init__(**kwargs)
        self._input_name: str = input_name

    @abstractmethod
    def add_parameters(self) -> None:
        """Add Input parameters."""
        pass

    @abstractmethod
    def init(self) -> None:
        """Init Input objects."""
        pass

    @abstractmethod
    def latest_data(self) -> Optional[Any]:
        return None

    @classmethod
    def register(cls, input_type: Type['Input'], name: str) -> None:
        if name not in cls.registered_inputs:
            cls.registered_inputs[name] = input_type
        else:
            raise Exception(f"An input type {name} has already been registered")

    @classmethod
    def create_from_name(cls, name: str, **kwargs: Any) -> Optional['Input']:
        if name not in Input.registered_inputs:
            return None
        else:
            return Input.registered_inputs[name](**kwargs)

    def retrieve_flows(self, flow: Flow, now: float, dt: float) -> None:
        """
        Retrieve the flow from the input
        """
        flow_to_add: Dict[str, Dict[any]] = deepcopy(self.latest_data())  # type: ignore
        for host, streams in flow_to_add.items():
            for stream_name, stream_data in streams.items():
                new_stream_name = host + "_" + stream_name
                if not flow.has_stream(new_stream_name):
                    flow.add_stream(new_stream_name, Stream.Type[stream_data["type"]])
                flow.get_stream_by_name(new_stream_name).timestamp = stream_data["timestamp"]
                new_channels: List[Channel] = []
                for channel_data in stream_data["channels"]:
                    if channel_data:
                        new_channels.append(
                            Channel(channel_data["name"], Channel.Type[channel_data["type"]] , channel_data["data"], channel_data["metadata"]))
                flow.set_stream_frame(new_stream_name, new_channels, update_timestamp=False)

    def stop(self) -> None:
        """
        Stop input
        """
        pass
