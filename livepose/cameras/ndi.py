import cv2

import time
from livepose.dataflow import Channel
from livepose.camera import register_camera, Camera
from typing import Any, List, Optional, Tuple
import cyndilib
import logging
from time import sleep
logger = logging.getLogger(__name__)


@register_camera("ndi")
class NDICamera(Camera):
    """
    Utility class embedding a camera with pyrealsense2, its parameters and buffers
    """
    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.source = None
        self.receiver = cyndilib.receiver.Receiver(color_format=cyndilib.wrapper.ndi_recv.RecvColorFormat.RGBX_RGBA, bandwidth=cyndilib.RecvBandwidth.highest)
        self.finder = cyndilib.Finder()
        # We force the pixel format to RGBX_RGBA. This may introduce a little conversion latency but simplifies the code we have to write in order to support other format. If you feel like NDI latency becomes a problem feel free to improve this.
        self.video_frame_sync = cyndilib.video_frame.VideoFrameSync()
        self.frame_sync = self.receiver.frame_sync
        self.frame_sync.set_video_frame(self.video_frame_sync)
        def on_finder_change():
            if self.finder is None:
                return
            if self.source is not None:
                return
            for s in self.finder:
                logger.info(f"found source {s.name}")
                if s.name == self.path or s.stream_name == self.path:
                    logger.info(f"source {s.name} matches desired source")
                    self.source = s
                    self.receiver.set_source(self.source)
                    break

        self.finder.set_change_callback(on_finder_change)
        self.finder.open()
        # ugly hack, livepose is not patient enough for the ndi callback
        for i in range(100):
            if self.source is not None:
                break
            sleep(0.2)

    def add_parameters(self) -> None:
        super().add_parameters()

    def init(self) -> None:
        pass

    @property
    def fps(self) -> float:
        """
        Get the grab framerate for the camera
        :return: float - Framerate as frames per second
        """
        return self.video_frame_sync.get_frame_rate()

    @fps.setter
    def fps(self, framerate: float) -> None:
        logger.warn("can't set fps for ndi")

    @property
    def resolution(self):
        self.video_frame_sync.get_resolution()


    @resolution.setter
    def resolution(self):
        logger.warn("can't set resolution for ndi")

    def is_open(self) -> bool:
        """
        Check if a video capturing device has been initialized already
        :return: bool
        """
        return self.receiver.is_connected()

    def grab(self) -> bool:
        """
        Grabs the next frame from video file or capturing device
        :return: bool - in the case of success
        """
        if not self.receiver.is_connected():
            return False

        self.receiver.frame_sync.capture_video()
        xres, yres = self.video_frame_sync.get_resolution()

        # if we got an empty frame, skip up to 15 empty frames because cv2 doesn't like empty video
        empty_frames_skip = 0
        while not min(xres, yres) and empty_frames_skip < 15:
            self.receiver.frame_sync.capture_video()
            xres, yres = self.video_frame_sync.get_resolution()
            empty_frames_skip += 1
            # estimate a 30fps frame rate since we dont know the framerate yet
            sleep(1/30)

        if empty_frames_skip == 15:
            return False

        # if our line stride (number of bytes corresponding to a line in our linear bytes array)
        # is greater than xres*4, (RGBA for every pixel in a line, even if we have no explicit alpha channel) it means there
        # was some padding added and that we need to remove the last (line_stride - xres/4) bytes from the array to get an image
        # of the right format.
        bytes_array = self.video_frame_sync.get_array()
        line_stride = self.video_frame_sync.get_line_stride()
        if line_stride > xres*4:
            # first reshape the array as a 3d array with lines wide as the line stride
            bytes_array = bytes_array.reshape(yres, int(line_stride/4), 4)
            # then drop the extra bytes
            bytes_array = bytes_array[:, :xres, :]
        else:
            bytes_array = bytes_array.reshape(yres, xres, 4)
        # opencv actually expects BGR ...
        self._frame = cv2.cvtColor(bytes_array, cv2.COLOR_RGB2BGR)
        return True

    def retrieve(self) -> bool:
        """
        Decodes and set the grabbed video frame
        grab() should be called beforehand
        :return: bool - true in the case of success
        """
        return True

    def get_raw_channels(self) -> List[Channel]:
        """
        Get the raw channels from the camera, without any
        transformation (flip, rotate, etc) applied
        :return: List[Channel] - List of raw channels
        """
        channels: List[Channel] = []

        channels.append(Channel(
            type=Channel.Type.COLOR,
            name=self._args.path,
            data=self._frame,
            metadata={}
        ))
        return channels

    def open(self) -> bool:
        """
        Open video file or device
        :return: bool - true if successful
        """
        return True

    def close(self) -> None:
        """
        Closes video file or capturing device
        """
        return

    @property  # type: ignore
    def rotate(self) -> int:
        """
        Get the status of rotation (clockwise)
        :return: int - rotation
        """
        return self._args.rotate

    @rotate.setter  # type: ignore
    def rotate(self, rotate: int) -> None:
        """
        Set camera rotation (clockwise)
        :param: int - rotation
        """
        self._args.rotate = rotate
