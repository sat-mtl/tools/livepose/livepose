import argparse

from abc import ABCMeta, abstractmethod

from typing import Any, Callable, Dict, List, Optional, Type, Union

from livepose.dataflow import Flow
from livepose.node import Node


def register_filter(name: str) -> Callable:
    """
    Decorator for registering filters
    :param name: name - Name of the filter
    """

    def deco(cls: Type['Filter']) -> Callable:
        Filter.register(filter_type=cls, name=name)
        return cls

    return deco


class Filter(Node):
    """
    Base class for filters.
    A filter gets the skeleton data as input by camera, and outputs a dict with the target path and the value
    """

    Result = Dict[Union[str, int], Any]

    registered_filters: Dict[str, Type['Filter']] = dict()

    def __init__(self, filter_name: str, *args: Any, **kwargs: Any):
        """Init Filter class."""
        super().__init__(**kwargs)
        self._result: Filter.Result = {}
        self._filter_name: str = filter_name

    @abstractmethod
    def add_parameters(self) -> None:
        """Add Filter parameters."""
        pass

    @abstractmethod
    def init(self) -> None:
        """Init Filter objects."""
        pass

    @classmethod
    def register(cls, filter_type: Type['Filter'], name: str) -> None:
        if name not in cls.registered_filters:
            cls.registered_filters[name] = filter_type
        else:
            raise Exception(f"A filter typed {name} has already been registered")

    @classmethod
    def create_from_name(cls, name: str, **kwargs: Any) -> Optional['Filter']:
        if name not in Filter.registered_filters:
            return None
        else:
            return Filter.registered_filters[name](**kwargs)

    @property
    def filter_name(self) -> str:
        return self._filter_name

    @property
    def result(self) -> Result:
        return self._result

    def step(self, flow: Flow, now: float, dt: float) -> None:
        """
        Process the given image(s) with the DL Backend model
        :param flow: Flow - Data flow to read from and write to
        :param now: float - current time
        :param dt: float - time since last call
        :return: bool - success
        """
        self._result.clear()
